(Ta)sk-space (Co)ntrol Library
=====================================

A hierarchical task-space control library. Contains an operational space controller with tasks for position, orientation, center of mass, joint limits, and joint control. Includes examples for creating a controller, using the prewritten controllers, running a gazebo simulation, and running teleoperation with ROS.

_Keywords: hierarchical task-space control, operational space control, force control, hierarchical quadratic programming, gazebo simulation_

This code is developed at the [Stanford Robotics Lab], Stanford University. <br>
*Developers*:  
Brian Soe <sup>1</sup>,  
Mikael Jorda,  
Keegan Go

[Website and doxygen documentation] <br>
[Git repository] <br>
[Augmented Reality Teleoperation Library] <br>

![Overview of control architecture](documentation/Overview.svg)


Example of Use
--------------

```cpp
// create a controller, with end-effector orientation as the highest priority,
// followed by end-effector position control, then joint space damping.
taco::RBDLDynamics dynamics;
taco::fixed::TaskController controller;
taco::fixed::OrientationTask ori_task;
taco::fixed::PositionTask pos_task;
taco::fixed::JointsTask joint_task;

// Import robot model to create kinematic/dynamic model of robot
dynamics.importRobot(taco::model::FILEPATH+"/iiwa/iiwa14.urdf", true);

// Set the joint positions (q) and velocities (dq) in the dynamics engine
dynamics.q_ << 0,M_PI/6,0,-M_PI/6,0,M_PI/6,0;
dynamics.dq_ << 0,0,0,0,0,0,0;
dynamics.updateDynamics();

// Add the dynamics engine and tasks to the controller
controller.setDynamics(&dynamics);
controller.addTask(&ori_task,0);
controller.addTask(&pos_task,1);
controller.addTask(&joint_task,2);

// set operational points, with goal at initial position
pos_task.setOperationalPoint("link_7", Eigen::Vector3d(0,0,0.2));
ori_task.setOperationalBody("link_7");
pos_task.desired_pos_ = pos_task.position(true);
ori_task.desired_pos_ = ori_task.position(true);

controller.gravity_compensation_ = true;
controller.setEnabled(true);

controller.computeTorque();
```

See also the [summary of examples]


Installation
------------

Control Library Dependencies:

* [Eigen3]  - linear algebra
* [Boost]   - stl extensions
* [RBDL]    - rigid body dynamics
* [urdfdom] - urdf parser (required by rdbl)

Simulator Dependencies:

* [ROS Jade] - interprocess messaging (desktop-full)
* [Gazebo]   - graphics and physics simulator (included with ROS Jade)
* [Chai3d]   - for haptic teleoperation example
* [ncurses]  - for keyboard teleoperation example

[Ubuntu installation guide]

[OS X installation guide]


Acquiring Robot Models
----------------------

Robot models are kept in a seperate repository. Use the bash script top pull the models, before running the examples.

     cd taco  
     sh pull_models.sh


Building Examples
-----------------------

See the [summary of examples]. These examples are built by build_all.sh for convenience.

     cd taco/examples
     sh build_examples.sh


--------

<sup>1</sup> bsoe at stanford edu


[Stanford Robotics Lab]:http://cs.stanford.edu/groups/manips/
[Website and doxygen documentation]:http://libtaco.bitbucket.io
[Git repository]:https://bitbucket.org/libtaco/taco.git
[Augmented Reality Teleoperation Library]:https://bitbucket.org/libtaco/art.git
[Ubuntu installation guide]:http://libtaco.bitbucket.io/md_documentation_INSTALL_ON_UBUNTU.html
[OS X installation guide]:http://libtaco.bitbucket.io/md_documentation_INSTALL_ON_OSX.html
[summary of examples]:http://libtaco.bitbucket.io/md_examples_EXAMPLES.html

[Eigen3]:http://eigen.tuxfamily.org/index.php?title=Main_Page
[RBDL]:http://rbdl.bitbucket.org/
[RBDL Readme]:https://bitbucket.org/rbdl/rbdl
[Boost]:http://www.boost.org/
[urdfdom]:https://github.com/ros/urdfdom

[ROS Jade]:http://www.ros.org/
[Gazebo]:http://gazebosim.org/
[ncurses]:https://www.gnu.org/software/ncurses/
[Chai3d]:http://www.chai3d.org/


