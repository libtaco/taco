/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "LinkForceSensorShmPlugin.h"
#include <taco/comm/topics.h>

#include <gazebo/gazebo_config.h>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <memory>

using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_SENSOR_PLUGIN(LinkForceSensorShmPlugin)

LinkForceSensorShmPlugin::LinkForceSensorShmPlugin() : SensorPlugin()
{
    force_filter_.setDimension(3);
    force_filter_.setWindowSize(3);

    torque_filter_.setDimension(3);
    torque_filter_.setWindowSize(3);
}

LinkForceSensorShmPlugin::~LinkForceSensorShmPlugin()
{
    TACO_SHM_SCOPED_LOCK(force_sensor_->sync.mutex);
    force_sensor_->sync.removed = true;

    stopWriteToFile();
}

/////////////////////////////////////////////////
void LinkForceSensorShmPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr /*_sdf*/)
{
  // Get the parent sensor.
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
    this->parentSensor = boost::dynamic_pointer_cast<gazebo::sensors::ContactSensor>(_sensor);
#elif GAZEBO_MAJOR_VERSION==7
    this->parentSensor = std::dynamic_pointer_cast<gazebo::sensors::ContactSensor>(_sensor);
#else
  throw std::runtime_error("ForceSensorRosPlugin. Gazebo version was not defined.");
#endif

  // Make sure the parent sensor is valid.
  if (!this->parentSensor)
  {
    gzerr << "LinkForceSensorShmPlugin requires a ContactSensor.\n";
    return;
  }

  // Connect to the sensor update event.
  this->updateConnection = this->parentSensor->ConnectUpdated(
      boost::bind(&LinkForceSensorShmPlugin::shmUpdate, this));

  // Make sure the parent sensor is active.
  this->parentSensor->SetActive(true);

  // get collision name
  collision_scoped_name_ = this->parentSensor->GetCollisionName(0);
  std::cout << "LinkForceSensorShmPlugin. Using collision [" << collision_scoped_name_ << "]\n";

  // get model name
  std::vector<std::string> tokens;
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
  std::string scoped_name = _sensor->GetScopedName();
#elif GAZEBO_MAJOR_VERSION==7
  std::string scoped_name = _sensor->ScopedName();
#endif
  boost::split(tokens, scoped_name,boost::is_any_of("::"),boost::token_compress_on);

  // get sensor name
  std::string model_name = tokens[1];
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
  std::string sensor_name = _sensor->GetName();
#elif GAZEBO_MAJOR_VERSION==7
  std::string sensor_name = _sensor->Name();
#endif

  // create name for shared memory
  std::string shm_name = model_name+":"+taco::topic::FORCE_SENSOR+":"+sensor_name;
  std::cout << "LinkForceSensorShmPlugin. Using shared memory name [" << shm_name << "].\n";

  // Initialize the shared memory for the joint states
  force_sensor_ = shm_force_sensor_.create(shm_name);

  // Create default file path name
  default_file_path_ = model_name+"_"+taco::topic::FORCE_SENSOR+"_"+sensor_name+".txt";
  if (write_to_file_ && !output_file_.is_open()){
      output_file_.open(default_file_path_);
      std::cout << "Writing force sensor data to [" << default_file_path_ << "].\n";
  }
}

void LinkForceSensorShmPlugin::shmUpdate()
{
  //https://github.com/arpg/Gazebo/blob/master/test/integration/contact_sensor.cc
  //https://bitbucket.org/osrf/gazebo/src/master/gazebo/sensors/ForceTorqueSensor.cc

  contact_bins_.clear();

  // Uncoment the following lines for debug output
  // Get all the contacts.
  msgs::Contacts contacts;
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
  contacts = this->parentSensor->GetContacts();
#elif GAZEBO_MAJOR_VERSION==7
  contacts = this->parentSensor->Contacts();
#endif

  for (int i = 0; i < contacts.contact_size(); ++i)
  {      
    bool body1 = true;
    std::string collision1 = contacts.contact(i).collision1();
    std::string collision2 = contacts.contact(i).collision2();
    if (collision1 == collision_scoped_name_) {
        body1 = true;
        contact_bins_[collision2].count += 1;
    } else if (collision2 == collision_scoped_name_) {
        body1 = false;
        contact_bins_[collision1].count += 1;
    } else {
        printWarning("Neither contact name matches body.");
    }


    for (unsigned int j = 0; j < contacts.contact(i).position_size(); ++j)
    {
        if (body1){
            contact_bins_[collision2].force += math::Vector3(
                        contacts.contact(i).wrench(j).body_1_wrench().force().x(),
                        contacts.contact(i).wrench(j).body_1_wrench().force().y(),
                        contacts.contact(i).wrench(j).body_1_wrench().force().z());
            contact_bins_[collision2].torque += math::Vector3(
                        contacts.contact(i).wrench(j).body_1_wrench().torque().x(),
                        contacts.contact(i).wrench(j).body_1_wrench().torque().y(),
                        contacts.contact(i).wrench(j).body_1_wrench().torque().z());
        } else {
            contact_bins_[collision1].force += math::Vector3(
                        contacts.contact(i).wrench(j).body_2_wrench().force().x(),
                        contacts.contact(i).wrench(j).body_2_wrench().force().y(),
                        contacts.contact(i).wrench(j).body_2_wrench().force().z());
            contact_bins_[collision1].torque += math::Vector3(
                        contacts.contact(i).wrench(j).body_2_wrench().torque().x(),
                        contacts.contact(i).wrench(j).body_2_wrench().torque().y(),
                        contacts.contact(i).wrench(j).body_2_wrench().torque().z());
        }
    }
  }

  force_ = math::Vector3::Zero;
  torque_ = math::Vector3::Zero;
  for (const auto& contact : contact_bins_)
  {
    force_ += (contact.second.force   / (double)contact.second.count);
    torque_ += (contact.second.torque / (double)contact.second.count);
  }

  if (use_force_filter_){
      Eigen::Vector3d temp(force_[0],force_[1],force_[2]);
      temp = force_filter_.update(temp);
      filtered_force_ = math::Vector3(temp(0),temp(1),temp(2));
  }

  if (use_torque_filter_){
      Eigen::Vector3d temp(torque_[0],torque_[1],torque_[2]);
      temp = torque_filter_.update(temp);
      filtered_torque_ = math::Vector3(temp(0),temp(1),temp(2));
  }

#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
  time_ = this->parentSensor->GetLastMeasurementTime();
#elif GAZEBO_MAJOR_VERSION==7
  time_ = this->parentSensor->LastMeasurementTime();
#endif
  {
      TACO_SHM_SCOPED_LOCK(force_sensor_->sync.mutex);
      force_sensor_->header.seq++;
      force_sensor_->header.stamp.sec = time_.sec;
      force_sensor_->header.stamp.nsec = time_.nsec;
      if (use_force_filter_){
          force_sensor_->force[0] = filtered_force_[0];
          force_sensor_->force[1] = filtered_force_[1];
          force_sensor_->force[2] = filtered_force_[2];
      } else {
          force_sensor_->force[0] = force_[0];
          force_sensor_->force[1] = force_[1];
          force_sensor_->force[2] = force_[2];
      }
      if (use_torque_filter_){
          force_sensor_->moment[0] = filtered_torque_[0];
          force_sensor_->moment[1] = filtered_torque_[1];
          force_sensor_->moment[2] = filtered_torque_[2];
      } else {
          force_sensor_->moment[0] = torque_[0];
          force_sensor_->moment[1] = torque_[1];
          force_sensor_->moment[2] = torque_[2];
      }
      force_sensor_->sync.initialized = true;
  }


  if (write_to_file_){
      double t = time_.sec + 0.000000001*time_.nsec;
      output_file_ << t << '\t'
                   << force_[0] << '\t'
                   << force_[1] << '\t'
                   << force_[2] << '\t'
                   << torque_[0] << '\t'
                   << torque_[1] << '\t'
                   << torque_[2] << '\t'
                   << filtered_force_[0] << '\t'
                   << filtered_force_[1] << '\t'
                   << filtered_force_[2] << '\t'
                   << filtered_torque_[0] << '\t'
                   << filtered_torque_[1] << '\t'
                   << filtered_torque_[2] << '\n';
  }

  userCallback();
}

void LinkForceSensorShmPlugin::PrintContacts()
{
  msgs::Contacts contacts;
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
  contacts = this->parentSensor->GetContacts();
#elif GAZEBO_MAJOR_VERSION==7
  contacts = this->parentSensor->Contacts();
#endif

  std::cout << "Time   : " << (double)time_.sec + 0.000000001*time_.nsec << '\n';
  std::cout << "Force  : " << force_ << '\n';
  std::cout << "Torque : " << torque_ << '\n';
  std::cout << "Num contacts [" << contacts.contact_size() << "]\n";

  for (int i = 0; i < contacts.contact_size(); ++i)
  {
    std::cout <<  i << "  Contact between["
              << contacts.contact(i).collision1() << "] and ["
              << contacts.contact(i).collision2() << "]\n";

    bool body1;
    if (contacts.contact(i).collision1() == collision_scoped_name_) {
        body1 = true;
    } else if (contacts.contact(i).collision2() == collision_scoped_name_) {
        body1 = false;
    }
    math::Vector3 force_contact = math::Vector3::Zero;
    math::Vector3 torque_contact = math::Vector3::Zero;

    for (int j = 0; j < contacts.contact(i).position_size(); ++j)
    {
      std::cout << "   " << j << "  Position:"
                << contacts.contact(i).position(j).x() << " "
                << contacts.contact(i).position(j).y() << " "
                << contacts.contact(i).position(j).z() << "\n";
      std::cout << "      Normal: "
                << contacts.contact(i).normal(j).x() << " "
                << contacts.contact(i).normal(j).y() << " "
                << contacts.contact(i).normal(j).z() << "\n";
      std::cout << "      Depth : "
                << contacts.contact(i).depth(j) << "\n";

      math::Vector3 force, torque;
      if (body1){
          force = math::Vector3(    contacts.contact(i).wrench(j).body_1_wrench().force().x(),
                                    contacts.contact(i).wrench(j).body_1_wrench().force().y(),
                                    contacts.contact(i).wrench(j).body_1_wrench().force().z());
          torque = math::Vector3(   contacts.contact(i).wrench(j).body_1_wrench().torque().x(),
                                    contacts.contact(i).wrench(j).body_1_wrench().torque().y(),
                                    contacts.contact(i).wrench(j).body_1_wrench().torque().z());
      } else {
          force = math::Vector3(    contacts.contact(i).wrench(j).body_2_wrench().force().x(),
                                    contacts.contact(i).wrench(j).body_2_wrench().force().y(),
                                    contacts.contact(i).wrench(j).body_2_wrench().force().z());
          torque = math::Vector3(   contacts.contact(i).wrench(j).body_2_wrench().torque().x(),
                                    contacts.contact(i).wrench(j).body_2_wrench().torque().y(),
                                    contacts.contact(i).wrench(j).body_2_wrench().torque().z());
      }

      force_contact  += force;
      torque_contact += torque;
      std::cout << "      Force  : " << force << '\n';
      std::cout << "      Torque : " << torque << '\n';
    }
    std::cout << "   Force  : " << force_contact << '\n';
    std::cout << "   Torque : " << torque_contact << '\n';
  }
}

void LinkForceSensorShmPlugin::startWriteToFile(const std::string& file_path)
{
    if (file_path.empty()){
        if (!default_file_path_.empty()){
            output_file_.open(default_file_path_);
            std::cout << "Writing force sensor data to [" << default_file_path_ << "].\n";
        }
        write_to_file_ = true;
    } else {
        output_file_.open(file_path);
        std::cout << "Writing force sensor data to [" << file_path << "].\n";
        write_to_file_ = true;
    }
}

void LinkForceSensorShmPlugin::stopWriteToFile()
{
    write_to_file_ = false;
    if (output_file_.is_open()){
        output_file_.close();
    }
}

void LinkForceSensorShmPlugin::printWarning(const std::string& message)
{
    std::cout << "WARNING. LinkForceSensorShmPlugin. " << message << std::endl;
}




