Gazebo Plugins
==============

Here you can find a list of all Gazebo plugins for the Task-space Control Library. All plugins create a shared memory interface that allows interprocess communication (IPC). Shared memory was chosen for it's low latency. ROS and other UDP/TCP methods like Redis experience larget intermittent latency issues, due to some unknown conflict with the Gazebo's own TCP infrastructure.

Before using a plugin, you must export the plugin folder for Gazebo to find.

     export GAZEBO_PLUGIN_PATH=${GAZEBO_PLUGIN_PATH}:my-path-to-taco/gazebo/lib


| Plugin                                 | Type         | Description                                             |
| -------------------------------------- |--------------| ------------------------------------------------------- |
| ForceControlShmXPlugin.so <sup>1</sup> | Model        | Control robot. IPC via shared memory.                   |
| MirrorPoseShmXPlugin.so  <sup>1</sup>  | Model        | Mirror robot (nondynamically). IPC via shared memory.   |
| LinkForceSensorShmPlugin.so            | Link Sensor  | Link is a force sensor. IPC via shared memory.          |
| JointForceSensorShmPlugin.so           | Joint Sensor | Joint is a force sensor. IPC via shared memory.         |

--------

1. X is the degrees of freedom of the robot.
