/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef _TACO_GAZEBO_MIRRORPOSESHMPLUGIN_H_
#define _TACO_GAZEBO_MIRRORPOSESHMPLUGIN_H_

#include <taco/comm/shm/SharedMemory.h>
#include <taco/comm/shm/JointState.h>
#include <taco/comm/copy.h>
#include <taco/comm/topics.h>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

#include <chrono>

namespace gazebo{

/** \brief Force control plugin for gazebo.
 * Communicate with Gazebo simulation via shared memory, without synchronization.
 *
 * Reads joint commands from shared memory, at name "robot_name/joint_command".
 * Write joint states to shared memory, at name "robot_name/joint_state".
 *
 * \ingroup gazebo
 */
template <size_t DOF>
class MirrorPoseShmPlugin : public ModelPlugin
{
public:

    MirrorPoseShmPlugin(){}

    virtual ~MirrorPoseShmPlugin()
    {
        TACO_SHM_SCOPED_LOCK(joint_state_->sync.mutex);
        joint_state_->sync.removed = true;
    }

protected: 

    /** \brief initialization */
    virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
        model_ = _parent;
        model_name_ = _parent->GetName();
        std::cout << "MirrorPoseShmPlugin. Using shared memory name [" << model_name_+":"+taco::topic::JOINT_STATE  << "].\n";

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection_ = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&MirrorPoseShmPlugin::callback, this, _1));

        std::cout << "Waiting for robot to create joint state shared memory" << std::endl;
    }

    /** \brief Update the joint states and commands using shared memory.
     * Called each gazebo time step. */
    void callback(const common::UpdateInfo & update_info)
    {
        // open shared memory
        if (joint_state_==NULL){
            joint_state_ = shm_joint_state_.open(model_name_+":"+taco::topic::JOINT_STATE);
            if (joint_state_==NULL){ return; }
            std::cout << "Opened joint state shared memory\n";
            if (joint_state_->dof != DOF){
                throw std::runtime_error("Joint state shared memory DOF mismatch");
            }
        }

        // check if shared memory initialized or has closed
        if (!joint_state_->sync.initialized){return;}
        if (joint_state_->sync.removed){
            std::cout << "Joint state shared memory has closed.\n";
            shm_joint_state_.remove();
            joint_state_ = NULL;
            return;
        }

        for  (int i=0; i<DOF; ++i){
            model_->GetJoint(std::string(joint_state_->joint_names[i]))
                  ->SetPosition(0, joint_state_->position[i]);
        }
    }


    void printWarning(const std::string& message)
    {
        std::cout << "WARNING. MirrorPoseShmPlugin. " << message << std::endl;
    }

    /// \brief Pointer to the model
    physics::ModelPtr model_;

    /// \brief Pointer to the update event connection
    event::ConnectionPtr updateConnection_;

    /// \brief Model name
    std::string model_name_;

    /** \brief Shared memory for joint state */
    taco::shm::SharedMemory<taco::shm::JointState  <DOF> >shm_joint_state_;

    /** Joint state shared memory. Read/write. Lock before writing. */
    taco::shm::JointState  <DOF>* joint_state_ = NULL;
};

}

#endif
