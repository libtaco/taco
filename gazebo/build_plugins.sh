#!/bin/bash

TACO_GAZEBO_PATH=$(dirname $(readlink -f $0))


# gazebo force control shared memory plugin
cd $TACO_GAZEBO_PATH/force_control_shm_plugin
mkdir -p build
cd build
cmake ..
make

# gazebo force sensor shared memory plugin
cd $TACO_GAZEBO_PATH/link_force_sensor_shm_plugin
mkdir -p build
cd build
cmake ..
make

# gazebo joint force sensor shared memory plugin
cd $TACO_GAZEBO_PATH/joint_force_sensor_shm_plugin
mkdir -p build
cd build
cmake ..
make

# gazebo mirror pose shared memory plugin
cd $TACO_GAZEBO_PATH/mirror_pose_shm_plugin
mkdir -p build
cd build
cmake ..
make










