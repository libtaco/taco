#!/bin/bash

TACO_GAZEBO_PATH=$(dirname $(readlink -f $0))

rm -rf $TACO_GAZEBO_PATH/force_control_shm_plugin/build
rm -rf $TACO_GAZEBO_PATH/joint_force_sensor_shm_plugin/build
rm -rf $TACO_GAZEBO_PATH/link_force_sensor_shm_plugin/build
rm -rf $TACO_GAZEBO_PATH/mirror_pose_shm_plugin/build
rm -rf $TACO_GAZEBO_PATH/lib

