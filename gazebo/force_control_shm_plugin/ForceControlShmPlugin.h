/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef _TACO_GAZEBO_FORCECONTROLSHMPLUGIN_H_
#define _TACO_GAZEBO_FORCECONTROLSHMPLUGIN_H_

#include <taco/driver/ShmDriver.h>
#include <taco/comm/copy.h>
#include <taco/comm/topics.h>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

#include <chrono>

namespace gazebo{

/** \brief Force control plugin for gazebo.
 * Communicate with Gazebo simulation via shared memory, without synchronization.
 *
 * Reads joint commands from shared memory, at name "robot_name/joint_command".
 * Write joint states to shared memory, at name "robot_name/joint_state".
 *
 * \ingroup gazebo
 */
template <size_t DOF>
class ForceControlShmPlugin : public ModelPlugin
{
public:

    ForceControlShmPlugin(){}
    virtual ~ForceControlShmPlugin(){}

protected: 

    /** \brief initialization */
    virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
        model_ = _parent;
        model_name_ = _parent->GetName();
        std::cout << "ForceControlShmPlugin. Using shared memory name [" << model_name_+":"+taco::topic::JOINT_STATE  << "].\n";

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection_ = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&ForceControlShmPlugin::shmUpdate, this, _1));

        // Check DOF and get joint names
        unsigned int dof = model_->GetJointCount();
        const auto joints = model_->GetJoints();
        std::vector<std::string> joint_names;
        for (auto joint : joints){
            if (isFixed(joint)){
                std::cout << "ForceControlShmPlugin. Joint [" << joint->GetName() << "] is fixed and ignored.\n";
                --dof;
            } else {
                joint_names.push_back(joint->GetName());
            }
        }

        if (dof!=DOF){throw std::runtime_error(
              "ForceControlShmPlugin. DOF does not match model.");}

        // Set the initial position
        int joint_index = 0;
        for (auto joint : joints){
            if (isFixed(joint)){continue;}
            joint->SetPosition(0, initial_joint_positions_[joint_index]);
            ++joint_index;
        }

        // Initialize the shared memory driver
        shm_driver_.initializeSharedMemory(model_name_, joint_names);

        // Set the Kp since this plugin does not do gravity compensation
        for (int i=0; i<DOF; ++i){
            shm_driver_.timeout_Kp_[i] = 100;
            shm_driver_.timeout_Kd_[i] = 2*sqrt(100);
        }
    }

    /** \brief Update the joint states and commands using shared memory.
     * Called each gazebo time step. */
    void shmUpdate(const common::UpdateInfo & update_info)
    {
        const auto joints = model_->GetJoints();

        // copy joint state from gazebo to shared memory
        {
            TACO_SHM_SCOPED_LOCK(shm_driver_.joint_state_->sync.mutex);
            shm_driver_.joint_state_->header.stamp.sec = update_info.simTime.sec;
            shm_driver_.joint_state_->header.stamp.nsec = update_info.simTime.nsec;
            shm_driver_.joint_state_->header.seq++;

            int joint_index = 0;
            for (auto joint : joints){
                if (isFixed(joint)){continue;}
                shm_driver_.joint_state_->position[joint_index]          = joint->GetAngle(0).Radian();
                shm_driver_.joint_state_->velocity[joint_index]          = joint->GetVelocity(0);
                //shm_driver_.joint_state_->acceleration[joint_index]    = joint->GetAcceleration(0);  // not given by gazebo
                shm_driver_.joint_state_->force_commanded[joint_index]   = joint->GetForce(0);
                //shm_driver_.joint_state_->force_measured[joint_index]  = joint->GetForceMeasured(0); // not given by gazebo
                ++joint_index;
            }
            shm_driver_.joint_state_->sync.initialized = true;
        }

        // copy joint command from shared memory to gazebo
        if (shm_driver_.commandTimeout())
        {
            shm_driver_.updateTimeoutTorque();
            for (int i=0; i<DOF; ++i){
                std::string name;
                taco::copy(shm_driver_.joint_state_->joint_names[i], name);
                model_->GetJoint(name)->SetForce(0, shm_driver_.timeoutTorque(i));
            }
        }
        else {
            TACO_SHM_SCOPED_LOCK(shm_driver_.joint_command_->sync.mutex);
            switch (shm_driver_.joint_command_->control_type){
            case taco::shm::JointCommand<DOF>::CONTROL_NONE:
                break;
            case taco::shm::JointCommand<DOF>::CONTROL_FORCE:
                for (int i=0; i<DOF; ++i){
                    std::string name;
                    taco::copy(shm_driver_.joint_command_->joint_names[i], name);
                    model_->GetJoint(name)->SetForce(0, shm_driver_.joint_command_->desired_force[i]);
                }
                break;
            case taco::shm::JointCommand<DOF>::CONTROL_POSITION:
                printWarning("Not implemented. Try ros_control package for position control.\n");
                break;
            case taco::shm::JointCommand<DOF>::CONTROL_VELOCITY:
                printWarning("Not implemented. Try ros_control package for velocity control.\n");
                break;
            case taco::shm::JointCommand<DOF>::CONTROL_ACCELERATION:
                printWarning("Not implemented. Try ros_control package for acceleration control.\n");
                break;
            default:
                printWarning("Unrecognized joint command ["+std::to_string(shm_driver_.joint_command_->control_type)+"].\n");
                break;
            }
        }
    }

    /** \brief Check if a link is fixed to the ground
     * \return True is link is fixed to the ground
     */
    bool isFixed(const gazebo::physics::JointPtr& joint)
    {
        if (joint->GetType() & joint->FIXED_JOINT){
            return true;
        }
        if (joint->GetLowerLimit(0) == joint->GetUpperLimit(0)){
            return true;
        }
        return false;
    }

    void printWarning(const std::string& message)
    {
        std::cout << "WARNING. ForceControlShmPlugin. " << message << std::endl;
    }

    /// \brief Pointer to the model
    physics::ModelPtr model_;

    /// \brief Pointer to the update event connection
    event::ConnectionPtr updateConnection_;

    /// \brief Model name
    std::string model_name_;

    /// \brief The initial position
    double initial_joint_positions_[DOF] = {0};

    /// \brief Shared memory for joint state
    taco::ShmDriver<DOF> shm_driver_;
};

}

#endif
