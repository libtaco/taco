/**
 * This file is part of Task-Space Control Library.
 *
 * TaCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TaCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
 */

#include "ForceControlShmPlugin.h"

namespace gazebo
{
  class ForceControlShm34Plugin : public ForceControlShmPlugin<34>
  {
  public:
    ForceControlShm34Plugin(){}
    virtual ~ForceControlShm34Plugin(){}
  };

  GZ_REGISTER_MODEL_PLUGIN(ForceControlShm34Plugin)
}
