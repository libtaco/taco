/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "JointForceSensorShmPlugin.h"
#include <taco/comm/topics.h>

#include <gazebo/gazebo_config.h>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <memory>

using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_SENSOR_PLUGIN(JointForceSensorShmPlugin)

JointForceSensorShmPlugin::JointForceSensorShmPlugin() : SensorPlugin()
{
}

JointForceSensorShmPlugin::~JointForceSensorShmPlugin()
{
    TACO_SHM_SCOPED_LOCK(force_sensor_->sync.mutex);
    force_sensor_->sync.removed = true;

    stopWriteToFile();
}

/////////////////////////////////////////////////
void JointForceSensorShmPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr /*_sdf*/)
{
  // Get the parent sensor.
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
    this->parentSensor = boost::dynamic_pointer_cast<gazebo::sensors::ForceTorqueSensor>(_sensor);
#elif GAZEBO_MAJOR_VERSION==7
    this->parentSensor = std::dynamic_pointer_cast<gazebo::sensors::ForceTorqueSensor>(_sensor);
#else
  throw std::runtime_error("ForceSensorRosPlugin. Gazebo version was not defined.");
#endif

  // Make sure the parent sensor is valid.
  if (!this->parentSensor)
  {
    gzerr << "JointForceSensorShmPlugin requires a ForceTorqueSensor.\n";
    return;
  }

  // Connect to the sensor update event.
  this->updateConnection = this->parentSensor->ConnectUpdated(
      boost::bind(&JointForceSensorShmPlugin::shmUpdate, this));

  // Make sure the parent sensor is active.
  this->parentSensor->SetActive(true);

  // get model name
  std::vector<std::string> tokens;
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
  std::string scoped_name = _sensor->GetScopedName();
#elif GAZEBO_MAJOR_VERSION==7
  std::string scoped_name = _sensor->ScopedName();
#endif
  boost::split(tokens, scoped_name,boost::is_any_of("::"),boost::token_compress_on);
  std::string model_name = tokens[1];  

  // get sensor name
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
  std::string sensor_name = _sensor->GetName();
#elif GAZEBO_MAJOR_VERSION==7
  std::string sensor_name = _sensor->Name();
#endif

  // create shared memory name
  std::string shm_name = model_name+":"+taco::topic::FORCE_SENSOR+":"+sensor_name;
  std::cout << "JointForceSensorShmPlugin. Using shared memory name [" << shm_name << "].\n";

  // Initialize the shared memory for the joint states
  force_sensor_ = shm_force_sensor_.create(shm_name);

  // Create default file path name
  default_file_path_ = model_name+"_"+taco::topic::FORCE_SENSOR+"_"+sensor_name+".txt";
  if (write_to_file_ && !output_file_.is_open()){
      output_file_.open(default_file_path_);
      std::cout << "Writing force sensor data to [" << default_file_path_ << "].\n";
  }
}

void JointForceSensorShmPlugin::shmUpdate()
{ 
#if ((GAZEBO_MAJOR_VERSION==5) || (GAZEBO_MAJOR_VERSION==6))
  force_ = parentSensor->GetForce();
  torque_ = parentSensor->GetTorque();
  time_ = this->parentSensor->GetLastMeasurementTime();
#elif GAZEBO_MAJOR_VERSION==7
  force_ = parentSensor->Force();
  torque_ = parentSensor->Torque();
  time_ = this->parentSensor->LastMeasurementTime();
#endif
  {
      TACO_SHM_SCOPED_LOCK(force_sensor_->sync.mutex);
      force_sensor_->header.seq++;
      force_sensor_->header.stamp.sec = time_.sec;
      force_sensor_->header.stamp.nsec = time_.nsec;
      force_sensor_->force[0] = force_[0];
      force_sensor_->force[1] = force_[1];
      force_sensor_->force[2] = force_[2];
      force_sensor_->moment[0] = torque_[0];
      force_sensor_->moment[1] = torque_[1];
      force_sensor_->moment[2] = torque_[2];
      force_sensor_->sync.initialized = true;
  }



  if (write_to_file_){
      double t = time_.sec + 0.000000001*time_.nsec;
      output_file_ << t << '\t'
                   << force_[0] << '\t'
                   << force_[1] << '\t'
                   << force_[2] << '\t'
                   << torque_[0] << '\t'
                   << torque_[1] << '\t'
                   << torque_[2] << '\n';
    }

  userCallback();
}

void JointForceSensorShmPlugin::PrintContacts()
{
    std::cout << "Force  : " << force_ << '\n';
    std::cout << "Torque : " << torque_ << '\n';
}

void JointForceSensorShmPlugin::startWriteToFile(const std::string& file_path)
{
    if (file_path.empty()){
        if (!default_file_path_.empty()){
            output_file_.open(default_file_path_);
            std::cout << "Writing force sensor data to [" << default_file_path_ << "].\n";
        }
        write_to_file_ = true;
    } else {
        output_file_.open(file_path);
        std::cout << "Writing force sensor data to [" << file_path << "].\n";
        write_to_file_ = true;
    }
}

void JointForceSensorShmPlugin::stopWriteToFile()
{
    write_to_file_ = false;
    if (output_file_.is_open()){
        output_file_.close();
    }
}

void JointForceSensorShmPlugin::printWarning(const std::string& message)
{
    std::cout << "WARNING. JointForceSensorShmPlugin. " << message << std::endl;
}




