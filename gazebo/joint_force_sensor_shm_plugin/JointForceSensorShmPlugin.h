/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef _TACO_GAZEBO_JOINTFORCESENSORSHMPLUGIN_H_
#define _TACO_GAZEBO_JOINTFORCESENSORSHMPLUGIN_H_

#include <taco/comm/shm/SharedMemory.h>
#include <taco/comm/shm/ForceSensor.h>
#include <taco/comm/copy.h>
#include <taco/comm/topics.h>

#include <gazebo/common/Plugin.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/util/system.hh>

#include <fstream>

namespace gazebo
{
  /** \brief A plugin for a contact sensor. Publishes the forces to shared memory.
   *
   * Write force data to shared memory, at name "robot_name/sensor_name", where sensor_name is given by the SDF
   *
   * \ingroup gazebo
   */
  class GAZEBO_VISIBLE JointForceSensorShmPlugin : public SensorPlugin
  {

public:

    JointForceSensorShmPlugin();

    virtual ~JointForceSensorShmPlugin();

    /** \brief Load the sensor plugin.
     * \param sensor Sensor that loaded this plugin.
     * \param sdf SDF elements for  plugin.
     */
    virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);

    /// \brief Print all contact information
    virtual void PrintContacts();

    /// \brief Start writing force sensor data to file
    /// \param file_path File to write data to.
    void startWriteToFile(const std::string& file_path = "");

    /// \brief Stop writing force sensor data to file
    void stopWriteToFile();

    /// \brief User callback
    virtual void userCallback(){}

protected:
    /// \brief Callback that recieves the contact sensor's update signal.
    /// Override this this function to get callbacks when the contact sensor
    /// is updated with new data.
     virtual void shmUpdate();

    /// \brief print a warning
    void printWarning(const std::string& message);

    /// \brief Pointer to the contact sensor
    sensors::ForceTorqueSensorPtr parentSensor;

    /// \brief Connection that maintains a link between the contact sensor's
    /// updated signal and the OnUpdate callback.
    event::ConnectionPtr updateConnection;

    /// \brief Sum of all contact forces, in global frame (i think)
    math::Vector3 force_ = math::Vector3(0,0,0);

    /// \brief Moment resulting from all contact forces, in global frame (i think)
    math::Vector3 torque_ = math::Vector3(0,0,0);

    /// \brief Shared memory for joint command
    taco::shm::SharedMemory<taco::shm::ForceSensor> shm_force_sensor_;

    /// \brief joint state object in shared memory
    taco::shm::ForceSensor* force_sensor_ = NULL;

    /// \brief write force data to file
    bool write_to_file_ = false;

    /// \brief output data file
    std::ofstream output_file_;

    /// \brief default file path set in load to be mode name etc
    std::string default_file_path_;

    /// \brief time of last update
    gazebo::common::Time time_;



  };
}
#endif
