Installing the Task-space Control Library on OS X 
=================================================

TaCo has not been tested on OS X, and will likely not work out of the box. However, the below steps might be helpful.


Pull Repository
---------------

If you intend to use ros (required for teleoperation examples), clone the repository into *catkin_ws/src/*. If you do not currently have ROS, see the below section.

     cd catkin_ws/src
     git clone https://bitbucket.org/libtaco/taco.git


Installing Dependencies
-----------------------

Install brew.

    xcode-select --install
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew update

1. [Gazebo] and [ROS] - recommended

   TaCo uses Gazebo for the simulation examples, and ROS for the teleoperation examples. We recommend one the following combinations:

   1. ROS Jade / Gazebo 5 (included with ROS).
   2. ROS Kinetic / Gazebo 7 (included with ROS).

 The ROS wiki has instructions for [ROS installation] and [ROS workspace configuration]. 

2. [Eigen3] and [RBDL] - required
 
   TaCo uses Eigen for linear algebra, and RBDL for dynamics/kinematics. RBDL requires [Boost], [Eigen3], and [urdfdom]
 
        brew install boost eigen
        brew install boost
        cd taco/3rdparty/
        git clone https://github.com/ros/urdfdom.git
        cd urdfdom
        mkdir -p build && cd build
        cmake ..
        make && make install
        cd ../..
        hg clone https://bitbucket.org/rbdl/rbdl

3. Edit rbdl/CMakeLists.txt, and set the urdfreader option to ON.

        OPTION (RBDL_BUILD_ADDON_URDFREADER "Build the (experimental) urdf reader" ON)

4. Compile RBDL as shown in the [RBDL Readme].

   @note If you encounter an error "ros/ros.h: No such file or directory", comment out "#include \"ros/ros.h\"" in "rbdl/addons/urdfreader/urdfreader.cc"

5. [ncurses] - recommended

   TaCo uses ncurses for the keyboard controlled teleoperation example

        brew install libncurses-dev


Building TaCo
-----------------------

TaCo provides a bash script to build the library, write an absolute path to the robot models, link to ros messages, build the gazebo plugins, and build the motion primitives. By default, it uses ROS which is required by the teleoperation examples.

If you are using ros, build with catkin first, then run the bash script:

     cd ~/catkin_ws && catkin_make
     cd src/taco
     sh build_all.sh 

If you do not want to use ros, peform the following steps:
 
1. in "build_all.sh", set TACO_USE_ROS=false 
2. in "examples/build_examples.sh", set TACO_USE_ROS=false

Now run the build bash script:

     cd taco
     sh build_all.sh


@ref index "Back to main page"

------------------------------------



[Stanford Robotics Lab]:http://cs.stanford.edu/groups/manips/
[Eigen3]:http://eigen.tuxfamily.org/index.php?title=Main_Page
[RBDL]:http://rbdl.bitbucket.org/
[RBDL Readme]:https://bitbucket.org/rbdl/rbdl
[Boost]:http://www.boost.org/
[urdfdom]:https://github.com/ros/urdfdom
[Gazebo]:http://gazebosim.org/
[ROS]:http://www.ros.org/
[ROS installation]:http://wiki.ros.org/jade/Installation/OSX/Homebrew/Source
[ROS workspace configuration]:http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment
[ncurses]:https://www.gnu.org/software/ncurses/

