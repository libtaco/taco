Common ROS Commands
===================

Using ROS
-------------

Launch roscore in seperate terminal, and then run the program

     roscore

Peak into the ros messages

     rosnode list
     rostopic list
     rostopic echo -n 1 /kuka_iiwa/joint_state

Setting the ROS Host IP
-----------------------

The export lines can be added to ~/.bashrc  
so they are automatically run.

To local computer

     export ROS_HOSTNAME=localhost
     export ROS_MASTER_URI=http://localhost:11311
     roscore

To another computer

     export ROS_MASTER_URI=http://<master-ip-address>:11311
     export ROS_IP=<my-ip-address>
     roscore

Setting up a catkin workspace
-----------------------------

Setup the catkin workspace

     mkdir catkin_ws/src
     cd catkin_ws/src
     catkin_init_workspace
     cd ros_ws
     if grep -Fxq "source $(pwd)/devel/setup.sh" ~/.bashrc
     then
         echo "ros workspace already sourced"
     else
         echo "source $(pwd)/devel/setup.sh" >> ~/.bashrc
     fi

Compiling using catkin
----------------------

     catkin_make

