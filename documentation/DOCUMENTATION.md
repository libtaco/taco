See the generated doxygen documentation: http://libtaco.bitbucket.org/

Generate the documentation yourself:

     cd taco
     sudo apt-get install libjs-mathjax
     doxygen Doxyfile
