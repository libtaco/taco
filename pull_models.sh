#!/bin/bash

TACO_PATH=$(dirname $(readlink -f $0))
git submodule init
git submodule update --remote
cd $TACO_PATH/models
sh update_gazebo_models.sh
cd $TACO_PATH
