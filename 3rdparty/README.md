Some programs used in 3rd party may be under GPL. They are not used in the taco library. However, they may be used in certain examples, and these examples are marked GPL in the header comment.
