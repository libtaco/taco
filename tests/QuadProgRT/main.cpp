/*
 * run a simple qp
 */

#include <taco/controller/math.h>
#include <taco/optimizer/QuadProgRT.h>
#include <taco/optimizer/QPRegularization.h>
#include <iostream>
#include <chrono>

int main()
{
    // ===================================================================
    // problem 1   
    // taken from http://doc.cgal.org/latest/QP_solver/index.html
    std::cout << "================================================\n";
    std::cout <<"solve a simple qp with inequality constraints and no equality constraints\n";
    std::cout << "================================================\n";
    {
    int n = 2;
    int num_econst = 0;
    int num_iconst = 5;

    Eigen::MatrixXd A(n,n);
    Eigen::VectorXd B(n);
    Eigen::MatrixXd E(num_econst,n);
    Eigen::VectorXd E0(num_econst);
    Eigen::MatrixXd I(num_iconst,n);
    Eigen::VectorXd I0(num_iconst);
    Eigen::VectorXd x(n);
    Eigen::VectorXd x_x0(n);
    Eigen::VectorXd x0(n);

    A << 1,0,0,4;
    B << 0,-32;
    I << 1,1,-1,2, -1,0, 0,-1, 0,1;
    I0 << 7,4,0,0,4;

    double cost, cost_x0;
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> time, time_x0;

    {
        taco::QuadProgRT qp;
        qp.setObjective(A,B);
        qp.setEqualities(E,E0);
        qp.setInequalities(I,I0);
        qp.checkProblem();

        std::cout << "solving simple inequality qp without initial conditions\n";

        start = std::chrono::system_clock::now();
        cost = qp.solve(x);
        end = std::chrono::system_clock::now();
        time = end-start;
    }

    {
        taco::QuadProgRT qp;
        qp.setObjective(A,B);
        qp.setEqualities(E,E0);
        qp.setInequalities(I,I0);
        qp.checkProblem();

        x0 = x + 0.1*Eigen::VectorXd::Random(n);

        std::cout << "solving simple inequality qp with initial conditions\n";

        start = std::chrono::system_clock::now();
        cost_x0 = qp.solve(x_x0, x0);
        end = std::chrono::system_clock::now();
        time_x0 = end-start;
    }
    std::cout << "\n";
    std::cout << "correct solution/cost : " << "(2,3) / -56" << "\n";
    std::cout << "solution for qp w/o initial guess: " << x.transpose() << "\n";
    std::cout << "solution for qp w/  initial guess: " << x_x0.transpose() << "\n";
    std::cout << "cost     for qp w/o initial guess : " << cost << "\n";
    std::cout << "cost     for qp w/  initial guess : " << cost_x0 << "\n";
    std::cout << "error    for qp w/o initial guess : " << (A*x-B).transpose() << "\n";
    std::cout << "error    for qp w/  initial guess : " << (A*x_x0-B).transpose() << "\n";
    std::cout << "time     for qp w/o initial guess : " << time.count() << "\n";
    std::cout << "time     for qp w/  initial guess : " << time_x0.count() << "\n";


    }

    // ===================================================================
    // problem 2
    std::cout << "================================================\n";
    std::cout <<"solve a least squares problem with equality constraints\n";
    std::cout << "================================================\n";

    {
    int rows = 3;
    int cols = 7;
    int zero = 0;

    Eigen::MatrixXd AA = Eigen::MatrixXd::Random(rows,cols) + 3*Eigen::MatrixXd::Identity(rows,cols);
    Eigen::VectorXd bb = Eigen::VectorXd::Random(rows);

    Eigen::VectorXd x_ls = Eigen::VectorXd::Zero(cols);
    Eigen::VectorXd x_qp = Eigen::VectorXd::Zero(cols);
    Eigen::VectorXd x_rqp = Eigen::VectorXd::Zero(cols);
    Eigen::VectorXd x_rg = Eigen::VectorXd::Zero(cols);
    Eigen::VectorXd x_rg0 = Eigen::VectorXd::Zero(cols);

    {
        std::cout << "solving least squares" << std::endl;
        x_ls = taco::pseudoInv(AA)*bb;
    }

    {
        Eigen::MatrixXd A = Eigen::MatrixXd::Identity(cols,cols);
        Eigen::VectorXd B = Eigen::VectorXd::Zero(cols);
        Eigen::MatrixXd E = AA;
        Eigen::VectorXd E0 = bb;
        Eigen::MatrixXd I(zero,cols);
        Eigen::VectorXd I0(zero);

        taco::QuadProgRT qp;
        qp.setObjective(A,B);
        qp.setEqualities(E,E0);
        qp.setInequalities(I,I0);
        qp.checkProblem();

        std::cout << "solving inequality qp without initial conditions" << std::endl;
        qp.solve(x_qp);
    }

    {
        Eigen::MatrixXd A = Eigen::MatrixXd::Zero(rows,cols);
        Eigen::VectorXd B = Eigen::VectorXd::Zero(rows);
        Eigen::MatrixXd W = Eigen::MatrixXd::Identity(cols,cols);
        Eigen::MatrixXd E = AA;
        Eigen::VectorXd E0 = bb;
        Eigen::MatrixXd I(zero,cols);
        Eigen::VectorXd I0(zero);

        taco::QPRegularization<taco::QuadProgRT> regularization;
        regularization.setObjective(A,B,W);
        regularization.setEqualities(E,E0);
        regularization.setInequalities(I,I0);
        regularization.qp()->checkProblem();

        std::cout << "solving qp with regularization without initial conditions" << std::endl;
        regularization.solve(x_rqp);
    }

    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> time, time_x0;

    {
        Eigen::MatrixXd A = AA;
        Eigen::VectorXd B = bb;
        Eigen::MatrixXd W = 0.1*Eigen::MatrixXd::Identity(cols,cols);
        Eigen::MatrixXd E = Eigen::MatrixXd::Zero(zero,cols);
        Eigen::VectorXd E0 = Eigen::VectorXd::Zero(zero);
        Eigen::MatrixXd I(zero,cols);
        Eigen::VectorXd I0(zero);

        taco::QPRegularization<taco::QuadProgRT> regularization;
        regularization.setObjective(A,B,W);
        regularization.setEqualities(E,E0);
        regularization.setInequalities(I,I0);
        regularization.qp()->checkProblem();

        std::cout << "solving regularization approximation without initial conditions" << std::endl;
        start = std::chrono::system_clock::now();
        regularization.solve(x_rg);
        end = std::chrono::system_clock::now();
        time = end-start;
    }

    {
        Eigen::MatrixXd A = AA;
        Eigen::VectorXd B = bb;
        Eigen::MatrixXd W = 0.1*Eigen::MatrixXd::Identity(cols,cols);
        Eigen::MatrixXd E = Eigen::MatrixXd::Zero(zero,cols);
        Eigen::VectorXd E0 = Eigen::VectorXd::Zero(zero);
        Eigen::MatrixXd I(zero,cols);
        Eigen::VectorXd I0(zero);

        taco::QPRegularization<taco::QuadProgRT> regularization;
        regularization.setObjective(A,B,W);
        regularization.setEqualities(E,E0);
        regularization.setInequalities(I,I0);
        regularization.qp()->checkProblem();

        std::cout << "solving regularization approximation with initial conditions" << std::endl;
        start = std::chrono::system_clock::now();
        regularization.solve(x_rg0, x_rg + 0.005*Eigen::VectorXd::Random(cols));
        end = std::chrono::system_clock::now();
        time_x0 = end-start;
    }

    std::cout << "\n";
    std::cout << "least squares\n" << x_ls.transpose() << "\n";
    std::cout << "quadratic program\n" << x_qp.transpose() << "\n";
    std::cout << "qp via regularization\n" << x_rqp.transpose() << "\n";
    std::cout << "regularization approx\n" << x_rg.transpose() << "\n";
    std::cout << "regularization approx w/ initial guess\n" << x_rg0.transpose() << "\n";

    std::cout << "\nerror for:\n";
    std::cout << "least squares                : " << (AA*x_ls-bb).transpose() << "\n";
    std::cout << "quadratic program            : " << (AA*x_qp-bb).transpose() << "\n";
    std::cout << "qp via regularization        : " << (AA*x_rqp-bb).transpose() << "\n";
    std::cout << "regularization approx        : " << (AA*x_rg-bb).transpose() << "\n";
    std::cout << "regularization w/ init guess : " << (AA*x_rg0-bb).transpose() << "\n";

    std::cout << "\nsolve time for\n";
    std::cout << "regularization w/o init guess : " << time.count() << "\n";
    std::cout << "regularization w/  init guess : " << time_x0.count() << "\n";
    }

    // ===================================================================
    // problem 3
    std::cout << "================================================\n";
    std::cout <<"solve a least squares problem with equality and inequaity constraints\n";
    std::cout << "================================================\n";

    {
    int rows = 3;
    int cols = 7;
    int zero = 0;

    Eigen::MatrixXd AA = Eigen::MatrixXd::Random(rows,cols) + 3*Eigen::MatrixXd::Identity(rows,cols);
    Eigen::VectorXd bb = Eigen::VectorXd::Random(rows);
    Eigen::VectorXd x_max =  0.25*Eigen::VectorXd::Ones(cols);
    Eigen::VectorXd x_min = -0.20*Eigen::VectorXd::Ones(cols);

    Eigen::VectorXd x_ls  = Eigen::VectorXd::Zero(cols);
    Eigen::VectorXd x_qp  = Eigen::VectorXd::Zero(cols);
    Eigen::VectorXd x_rg  = Eigen::VectorXd::Zero(cols);

    Eigen::MatrixXd Identity = Eigen::MatrixXd::Identity(cols,cols);

    {
        std::cout << "solving least squares" << std::endl;
        x_ls = taco::pseudoInv(AA)*bb;
    }

    {
        Eigen::MatrixXd A = Eigen::MatrixXd::Identity(cols,cols);
        Eigen::VectorXd B = Eigen::VectorXd::Zero(cols);
        Eigen::MatrixXd E = AA;
        Eigen::VectorXd E0 = bb;
        Eigen::MatrixXd I(2*cols,cols); I <<  Identity,
                                             -Identity;
        Eigen::VectorXd I0(2*cols); I0 << x_max, -x_min;

        taco::QuadProgRT qp;
        qp.setObjective(A,B);
        qp.setEqualities(E,E0);
        qp.setInequalities(I,I0);
        qp.checkProblem();

        std::cout << "solving inequality qp without initial conditions" << std::endl;
        qp.solve(x_qp);
    }


    {
        Eigen::MatrixXd A = AA;
        Eigen::VectorXd B = bb;
        Eigen::MatrixXd W = 0.1*Eigen::MatrixXd::Identity(cols,cols);
        Eigen::MatrixXd E = Eigen::MatrixXd::Zero(zero,cols);
        Eigen::VectorXd E0 = Eigen::VectorXd::Zero(zero);
        Eigen::MatrixXd I(2*cols,cols); I <<  Identity,
                                             -Identity;
        Eigen::VectorXd I0(2*cols); I0 << x_max, -x_min;

        taco::QPRegularization<taco::QuadProgRT> regularization;
        regularization.setObjective(A,B,W);
        regularization.setEqualities(E,E0);
        regularization.setInequalities(I,I0);
        regularization.qp()->checkProblem();

        std::cout << "solving regularization approximation without initial conditions" << std::endl;
        regularization.solve(x_rg);
    }

    std::cout << "\n";
    std::cout << "least squares\n" << x_ls.transpose() << "\n";
    std::cout << "quadratic program\n" << x_qp.transpose() << "\n";
    std::cout << "regularization approx\n" << x_rg.transpose() << "\n";

    std::cout << "\nerror for:\n";
    std::cout << "least squares                : " << (AA*x_ls-bb).transpose() << "\n";
    std::cout << "quadratic program            : " << (AA*x_qp-bb).transpose() << "\n";
    std::cout << "regularization approx        : " << (AA*x_rg-bb).transpose() << "\n";

    }

	return 0;
}
