clc; clear; close all;

%% import data
data_file = 'build/data.txt';
data = importdata(data_file);

Ts = 0.001;

col_time = 1;
col_in = 2:4;
col_out = 5:7;

time = data(:,col_time);
in   = data(:,col_in);
out  = data(:,col_out);
N    = length(time);

i = 1;

%% plot time history
figure
plot(time,in(:,i),time,out(:,i))
xlabel('time (s)')
ylabel('position')

%% frequency response
Gest = etfe( iddata(out(:,1),in(:,1),Ts), [], N );

figure;
bode(Gest); %,{2*pi*fmin,2*pi*fmax})
hold on;

%% ideal frequency response
wn = 2*pi*20;
zeta = 0.7071;

s = tf('s');
G = wn^2/(s^2+2*zeta*wn*s+wn^2);
bode(G);



