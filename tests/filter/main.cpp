/****************************************************************************/

#include <fstream>
#include <iostream>
#include <math.h>

#include <taco/filter/LowPassFilter.h>


/****************************************************************************/

int main(int argc, char **argv)
{
    std::ofstream myfile;
    myfile.open ("data.txt");

    // chirp parameters
    const double F_MIN = 0.1;
    const double F_MAX = 100;
    const double AMPLITUDE = 1;
    const double T_RISE = 0;
    const double T_FALL = 0;

    // simulation parameters
    const double Ts = 0.001;
    const double T_END = 10;

    // low pass filter parameters
    double wn = 2*M_PI*20;
    double zeta = 0.7071;
    taco::LowPassFilter filter;
    filter.setDimension(3);
    filter.setParameters(wn,zeta,Ts);

    for (double t = 0; t < T_END; t+=Ts){
        
        // sweeping sine wave (chirp)
        // linear increasing frequency    
        double f_diff    = F_MAX - F_MIN;
        double frequency = F_MIN + f_diff*t/T_END;
        double phi       = F_MIN*t + 0.5*f_diff/T_END*t*t;

        // rise and fall time to smooth spectral ripple
        double amplitude = 0;
        if (t > T_END) {
            amplitude = 0;
        } else if (t < T_RISE){
            amplitude = AMPLITUDE*t/T_RISE;
        } else if (t > (T_END-T_FALL)){
            amplitude = AMPLITUDE*(t-(T_END-T_FALL));
        } else {
            amplitude = AMPLITUDE;
        }

        Eigen::VectorXd in(3);
        in.setConstant( amplitude*sin(2*M_PI*phi) );

        Eigen::VectorXd out = filter.update(in);
        myfile << t << ' ' << in.transpose() << ' ' << out.transpose() << '\n';
    }

    myfile.close();

    return 0;
}

/****************************************************************************/
