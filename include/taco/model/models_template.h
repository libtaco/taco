/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_MODEL_MODELS_H_
#define TACO_MODEL_MODELS_H_

#include <iostream>

namespace taco {
namespace model {
    const std::string FILEPATH  = ???
    const std::string WAM       = "wam/wam";
    const std::string JACO      = "jaco/jaco";
    const std::string MICO      = "mico/mico";
    const std::string IIWA7     = "iiwa/iiwa7";
    const std::string IIWA14    = "iiwa/iiwa14";
    const std::string LWR       = "lwr/lwr";
    const std::string PUMA560   = "puma560/puma560";
    const std::string UR3       = "ur3/ur3";
    const std::string UR5       = "ur5/ur5";
    const std::string UR10      = "ur10/ur10";
}
}

#endif
