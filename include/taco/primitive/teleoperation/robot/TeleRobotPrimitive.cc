/**
 * TaCo - Task-Space Control Library.
 * Copyright (c) 2015-2016
 *
 * TaCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TaCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU Lesser General Public License
 * along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
 */


#include "TeleRobotPrimitive.h"
#include <taco/comm/topics.h>

namespace taco {


TeleRobotPrimitive::TeleRobotPrimitive(const std::string& ros_name, const std::string& tool_name) :
    RobotPrimitive("TeleRobot"),
    tool_name_(tool_name)
{


    // initialize ros, if it has not already bee initialized.
    if (!ros::isInitialized()){
      int argc = 0;
      char **argv = NULL;
      ros::init(argc, argv, "TeleRobotPrimitive", ros::init_options::NoSigintHandler);
    }
    
    // subscribe and publish ros topics
    ros_node_.reset(new ::ros::NodeHandle(ros_name));
    publisher_  = ros_node_->advertise<taco_msgs::ToolState>(taco::topic::TOOL_STATE+"/"+tool_name,1);
    subscriber_ = ros_node_->subscribe(taco::topic::TOOL_COMMAND+"/"+tool_name, 1,
                                       &TeleRobotPrimitive::toolCommandCallback, this,
                                       ros::TransportHints().unreliable().tcpNoDelay());
}

TeleRobotPrimitive::~TeleRobotPrimitive(){}

void TeleRobotPrimitive::sendToolState()
{
    publisher_.publish(tool_state_);
}

bool TeleRobotPrimitive::toolCommandUpdated()
{
    bool temp = tool_command_updated_;
    tool_command_updated_ = false;
    return temp;
}

void TeleRobotPrimitive::toolCommandCallback(const taco_msgs::ToolCommand& msg)
{
    tool_command_ = msg;
    tool_command_updated_ = true;
}
   
}

