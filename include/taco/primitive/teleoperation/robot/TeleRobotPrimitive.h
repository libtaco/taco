/**
 * TaCo - Task-Space Control Library.
 * Copyright (c) 2015-2016
 *
 * TaCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TaCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU Lesser General Public License
 * along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
 *
 * \file
 * \ingroup TaCo
 */


#ifndef TACO_PRIMITIVE_TELEROBOTPRIMITIVE_H_
#define TACO_PRIMITIVE_TELEROBOTPRIMITIVE_H_

#include <taco/primitive/RobotPrimitive.h>
#include <taco/comm/ros/ToolState.h>
#include <taco/comm/ros/ToolCommand.h>

#include <ros/ros.h>

#include <Eigen/Dense>

#include <memory>
#include <string>
#include <iostream>


namespace taco {


/** \brief Teleoperation primitive, from the robot side.
 * Sends the tool state via ros.
 * Receives the new tool/end-effector command via ros.
 *
 * \ingroup primitive
 */
class TeleRobotPrimitive : public RobotPrimitive {

public:

    TeleRobotPrimitive(const std::string& ros_name, const std::string& tool_name);
    
    virtual ~TeleRobotPrimitive();

    /** \brief Check for update to tool command
     * \return True if a new tool command was received since the last time the function was called.
     */
    virtual bool toolCommandUpdated();

    /** \brief Send the tool state via ros */
    virtual void sendToolState();

    taco_msgs::ToolState tool_state_;
    taco_msgs::ToolCommand tool_command_;

protected:

    virtual void toolCommandCallback(const taco_msgs::ToolCommand& msg);

    bool tool_command_updated_ = false;

    std::unique_ptr<ros::NodeHandle> ros_node_;
    ros::Publisher  publisher_;
    ros::Subscriber subscriber_;

    std::string tool_name_;
  
};


} 
#endif 
