# ====== FIND AND LINK LIBRARIES ======

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

# PRIMITIVE
set(TACO_INCLUDE_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../..)
set(TeleRobotPrimitive_INCLUDE_DIR ${CMAKE_CURRENT_LIST_DIR})
if(NOT TARGET TeleRobotPrimitive)
    find_library( TeleRobotPrimitive_LIBRARY NAMES TeleRobotPrimitive 
                  PATHS ${CMAKE_CURRENT_LIST_DIR} )
endif()

# EIGEN LIBARARY
find_package (Eigen3 REQUIRED)

# ROS
find_package(           roscpp REQUIRED)
find_package(           std_msgs REQUIRED)

# create headers list
set(TeleRobotPrimitive_INCLUDE_DIRS ${TeleRobotPrimitive_INCLUDE_DIR}
                                    ${TACO_INCLUDE_DIR}
                                    ${EIGEN3_INCLUDE_DIR}
                                    ${roscpp_INCLUDE_DIRS}
                                    ${std_msgs_INCLUDE_DIRS}
)

# create libraries list
set(TeleRobotPrimitive_LIBRARIES    ${TeleRobotPrimitive_LIBRARY}
                                    ${roscpp_LIBRARIES}
)
