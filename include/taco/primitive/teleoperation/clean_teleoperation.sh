#!/bin/bash
TELEOPERATION_PATH=$(dirname $(readlink -f $0))

rm -rf $TELEOPERATION_PATH/robot/build
rm -rf $TELEOPERATION_PATH/robot/libTeleRobotPrimitive.so
rm -rf $TELEOPERATION_PATH/user/build
rm -rf $TELEOPERATION_PATH/user/libTeleUserPrimitive.so
