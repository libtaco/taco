#!/bin/bash
TELEOPERATION_PATH=$(dirname $(readlink -f $0))

# build the robot primitive
cd $TELEOPERATION_PATH/robot
mkdir -p build
cd build
cmake ..
make

# build the user primitive
cd $TELEOPERATION_PATH/user
mkdir -p build
cd build
cmake ..
make

