/**
 * TaCo - Task-Space Control Library.
 * Copyright (c) 2015-2016
 *
 * TaCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TaCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU Lesser General Public License
 * along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
 */


#include "TeleUserPrimitive.h"
#include <taco/comm/topics.h>

namespace taco {


TeleUserPrimitive::TeleUserPrimitive(const std::string& ros_name, const std::string& tool_name) :
    UserPrimitive("TeleUser"),
    tool_name_(tool_name)
{
    // initialize ros, if it has not already bee initialized.
    if (!::ros::isInitialized()){
      int argc = 0;
      char **argv = NULL;
      ros::init(argc, argv, "TeleUserPrimitive", ::ros::init_options::NoSigintHandler);
    }
    
    // subscribe and publish ros topics
    ros_node_.reset(new ros::NodeHandle(ros_name));
    publisher_  = ros_node_->advertise<taco_msgs::ToolCommand>(taco::topic::TOOL_COMMAND+"/"+tool_name,1);
    subscriber_ = ros_node_->subscribe(taco::topic::TOOL_STATE+"/"+tool_name, 1,
                                       &TeleUserPrimitive::toolStateCallback, this,
                                       ros::TransportHints().unreliable().tcpNoDelay());
}

TeleUserPrimitive::~TeleUserPrimitive(){}

bool TeleUserPrimitive::toolStateUpdated()
{
    bool temp = tool_state_updated_;
    tool_state_updated_ = false;
    return temp;
}

void TeleUserPrimitive::sendToolCommand()
{
    publisher_.publish(tool_command_);
}

void TeleUserPrimitive::toolStateCallback(const taco_msgs::ToolState& msg)
{
    tool_state_ = msg;
    tool_state_updated_ = true;
}
   
}

