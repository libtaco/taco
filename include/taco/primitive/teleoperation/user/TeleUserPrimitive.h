/**
 * TaCo - Task-Space Control Library.
 * Copyright (c) 2015-2016
 *
 * TaCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TaCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU Lesser General Public License
 * along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
 *
 * \file
 * \ingroup TaCo
 */


#ifndef TACO_PRIMITIVE_TELEUSERPRIMITIVE_H_
#define TACO_PRIMITIVE_TELEUSERPRIMITIVE_H_

#include <taco/primitive/UserPrimitive.h>
#include <taco/comm/ros/ToolState.h>
#include <taco/comm/ros/ToolCommand.h>

#include <ros/ros.h>

#include <Eigen/Core>

#include <memory>
#include <string>
#include <iostream>


namespace taco {


/** \brief Teleoperation primitive, from the user side.
 * Receives the tool state via ros.
 * Sends the new tool/end-effector command via ros.
 *
 * \ingroup primitive
 */
class TeleUserPrimitive : public UserPrimitive {

public:

    TeleUserPrimitive(const std::string& ros_name, const std::string& tool_name);
    
    virtual ~TeleUserPrimitive();

    /** \brief Check for update to tool state
     * \return True if a new tool state was received since the last time the function was called.
     */
    virtual bool toolStateUpdated();

    /** \brief Send the tool command via ros */
    virtual void sendToolCommand();

    taco_msgs::ToolState tool_state_;
    taco_msgs::ToolCommand tool_command_;

protected:

    virtual void toolStateCallback(const taco_msgs::ToolState& msg);

    bool tool_state_updated_ = false;

    std::unique_ptr<ros::NodeHandle> ros_node_;
    ros::Publisher  publisher_;
    ros::Subscriber subscriber_;

    std::string tool_name_;
  
};


} 
#endif 
