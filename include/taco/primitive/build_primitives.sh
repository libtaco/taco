#!/bin/bash

PRIMITIVE_PATH=$(dirname $(readlink -f $0))

if [ "$TACO_USE_ROS" = true ] ; then

sh $PRIMITIVE_PATH/teleoperation/build_teleoperation.sh

fi # TACO_USE_ROS
