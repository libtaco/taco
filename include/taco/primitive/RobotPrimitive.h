/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_PRIMITIVE_ROBOTPRIMITIVE_H_
#define TACO_PRIMITIVE_ROBOTPRIMITIVE_H_

#include <Eigen/Core>

#include <string>
#include <iostream>

namespace taco {

/** \brief Base class for a primitive that interacts with the human operator
 *
 * This class is currently empty. The definition of a robot primitive is not yet clearly defined in this framework.
 *
 * \ingroup primitive
 */
class RobotPrimitive {

public:

    RobotPrimitive(const std::string& name) : name_(name) {}
    
    virtual ~RobotPrimitive(){}

protected:

    std::string name_;

    virtual void printWarning(const std::string& message) {
        std::cout << "WARNING. RobotPrimitive [" << name_ << "]. " << message << std::endl;
    }
    
};


} /* namespace taco */
#endif /* RobotPrimitive_H_ */
