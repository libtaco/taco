Primitives
===========

| Primitive    | Reference                | Description                                          |
| ------------ | ------------------------ | ---------------------------------------------------- |
| Teleoperaion | N/A                      | Send tool commands and receive tool state via ROS    |


