/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_COMM_COPY_H_
#define TACO_COMM_COPY_H_

#include <string>

#include <eigen3/Eigen/Dense>
#include <boost/array.hpp>


namespace taco {

/** \brief Write a string to a char array */
static inline void copy(const std::string& in_string, char* out_buffer){
    std::size_t length = in_string.copy(out_buffer,in_string.length());
    out_buffer[length]='\0';
}

/** \brief Write a char array to a string */
static inline void copy(const char* out_buffer, std::string& in_string){
    in_string = std::string(out_buffer);
}

/** \brief Write a double array to an Eigen VectorXd */
static inline void copy(const double* array, Eigen::VectorXd& vector, size_t elements){
    vector.resize(elements);
    for (size_t i=0; i<elements; ++i){
        vector[i] = *(array+i);
    }
}

/** \brief Write a Eigen VectorXd to a double array */
static inline void copy(const Eigen::VectorXd& vector, double* array){
    for (int i=0; i<vector.size(); ++i){
        array[i] = vector[i];
    }
}

/** \brief Copy a ROS array[3] to an Eigen Vector3d */
static inline void copy(const boost::array<double, 3>& in_vec3d, Eigen::Vector3d& out_vec3d){
    out_vec3d(0) = in_vec3d[0];
    out_vec3d(1) = in_vec3d[1];
    out_vec3d(2) = in_vec3d[2];
}


/** \brief Copy an Eigen Vector3d to a ROS array[3] */
static inline void copy(const Eigen::Vector3d& in_vec3d, boost::array<double, 3>& out_vec3d){
    out_vec3d[0] = in_vec3d(0);
    out_vec3d[1] = in_vec3d(1);
    out_vec3d[2] = in_vec3d(2);
}

/** \brief Copy a ROS array[4] to an Eigen Matrix3d */
static inline void copy(const boost::array<double, 4>& in_quaterion, Eigen::Matrix3d& out_rot3d){
    Eigen::Quaternion<double> quat( in_quaterion[0],
                                    in_quaterion[1],
                                    in_quaterion[2],
                                    in_quaterion[3]);
    out_rot3d = quat.matrix();
}

/** \brief Copy an Eigen Matrix3d to a ROS array[4] */
static inline void copy(const Eigen::Matrix3d& in_rot3d, boost::array<double, 4>& out_quaterion){
    Eigen::Quaternion<double> quat(in_rot3d);
    out_quaterion[0] = quat.w();
    out_quaterion[1] = quat.x();
    out_quaterion[2] = quat.y();
    out_quaterion[3] = quat.z();
}

}

#endif
