/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_COMM_SHM_SHAREDMEMORY_H_
#define TACO_COMM_SHM_SHAREDMEMORY_H_

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

namespace taco {
namespace shm {

/** \brief Manages shared memory. 
 * One process should create the shared memory. All other processes should open the shared memory.
 *
 * \param T is the shared memory struct. See ExampleShm.h for an example.
 *
 * \ingroup shared_memory
 */
template <class T>
class SharedMemory
{
public:
    SharedMemory(){}

    virtual ~SharedMemory()
    {
        if (remove_on_close_){
            this->remove();
        }
    }

    /** \brief Creates the shared memory.
     * \param  name The name of the shared memory
     * \param  remove_on_close If true, will try to remove the shared memory on destruction
     * \return The pointer to the shared memory structure
     */
    T* create(std::string name, bool remove_on_close = true)
    {
        name_ = name;
        remove_on_close_ = remove_on_close;

        //Remove shared memory
        boost::interprocess::shared_memory_object::remove(name_.c_str());

        //Create a shared memory object.
        shm_ = boost::interprocess::shared_memory_object
            (boost::interprocess::create_only   //only create
            ,name_.c_str()                      //name
            ,boost::interprocess::read_write    //read-write mode
            );

        //Set size
        shm_.truncate(sizeof(T));

        //Map the whole shared memory in this process
        region_ = boost::interprocess::mapped_region
            (shm_                               //What to map
            ,boost::interprocess::read_write    //Map it as read-write
            );

        //Get the address of the mapped region
        addr_ = region_.get_address();

        //Construct the shared structure in memory
        data_ = new (addr_) T;
        return data_;
    }

    /** \brief Open the shared memory
     * \param  name The name of the shared memory
     * \param  remove_on_close If true, will try to remove the shared memory on destruction
     * \return The pointer to the shared memory structure
     */
    T* open(std::string name, bool remove_on_close = true)
    {
        name_ = name;
        remove_on_close_ = remove_on_close;

        //Open the shared memory object.    
        try {
            shm_ = boost::interprocess::shared_memory_object
                (boost::interprocess::open_only     //only create
                ,name_.c_str()                      //name
                ,boost::interprocess::read_write    //read-write mode
                );
        } catch(const boost::interprocess::interprocess_exception& e) {
            data_ = NULL;
            return NULL;
        }

        //Map the whole shared memory in this process
        region_ = boost::interprocess::mapped_region
            (shm_                               //What to map
            ,boost::interprocess::read_write    //Map it as read-write
            );

        //Get the address of the mapped region
        addr_ = region_.get_address();

        //Construct the shared structure in memory
        data_ = static_cast<T*>(addr_);
        return data_;
    }

    /** \brief Close shared memory, without removing it. */
    void close()
    {
        shm_ = boost::interprocess::shared_memory_object();
        region_ = boost::interprocess::mapped_region();
        addr_ = NULL;
        data_ = NULL;
    }

    /** \brief Close and remove shared memory. Will not remove if other process have it open.
     * \return True if successfully removed. Returns false otherwise. */
    bool remove()
    {
        close();
        return boost::interprocess::shared_memory_object::remove(name_.c_str());
    }

    /** \brief Object pointer, after shared memory has been created or opened.
     * \return The pointer to the shared memory structure
     */
    T* object(){
        return data_;
    }

private:
    std::string name_;
    boost::interprocess::shared_memory_object shm_;
    boost::interprocess::mapped_region region_;
    void * addr_ = NULL;
    T* data_ = NULL;
    bool remove_on_close_ = true;
};

}
}

#endif // SHAREDMEMORY_H

