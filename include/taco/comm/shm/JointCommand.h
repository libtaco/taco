/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_COMM_SHM_JOINTCOMMAND_H_
#define TACO_COMM_SHM_JOINTCOMMAND_H_

#include <taco/comm/shm/Header.h>
#include <taco/comm/shm/Sync.h>
#include <iostream>

namespace taco {
namespace shm {

/** \brief Joint commmand data that exists in shared memory
 *
 * Allows interprocess real-time data sharing.
 * Use TACO_SHM_SCOPED_LOCK(sync.mutex) before reading or writing data.
 * Use shared memory name "robot_name/taco::shm::names::JointCommand"
 *
 * \param DOF The degrees of freedom of the robot, known at compile time.
 *
 * \ingroup shared_memory
 */
template <int DOF>
struct JointCommand
{    
    enum ControlType {
        CONTROL_NONE=0,
        CONTROL_POSITION=1,
        CONTROL_VELOCITY=2,
        CONTROL_ACCELERATION=3,
        CONTROL_FORCE=4
    };

    Header  header;                     /// \brief message info

    ControlType control_type    = CONTROL_NONE;     /// \brief control method
    char    joint_names[DOF][STR_LEN] = {{'\0'}};   /// \brief joint names
    double  desired_pos[DOF]    = {0};  /// \brief desired position of joints including unactuated
    double  desired_vel[DOF]    = {0};  /// \brief desired velocity of joints including unactuated
    double  desired_acc[DOF]    = {0};  /// \brief desired acceleration of joints including unactuated
    double  desired_force[DOF]  = {0};  /// \brief desired force of joints including unactuated
    const unsigned int dof      = DOF;  /// \brief dof of freedom of the robot, for convenience

    Sync sync;                          /// \brief interprocess synchronization
};

}
}

#endif
