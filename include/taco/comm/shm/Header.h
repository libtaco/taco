/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_COMM_SHM_HEADER_H_
#define TACO_COMM_SHM_HEADER_H_

namespace taco {
namespace shm {

/// \brief Maximum length of a string. Save the last character for termination, i.e. '\0'.
const unsigned int STR_LEN = 20;

/** \brief Two-integer timestamp that is expressed since epoch */
struct Time{
    /// \brief Seconds (stamp_secs) since epoch
    long sec;

    /// \brief Nanoseconds since stamp_secs
    long nsec;
};

/** \brief Standard metadata for higher-level flow data types
 *
 * \ingroup shared_memory
 */
struct Header
{
    /// \brief Sequence ID: consecutively increasing ID
    unsigned long seq;      
    
    /// \brief Time stamp
    Time stamp;
    
    /// \brief Frame this data is associated with
    char frame_id[STR_LEN];
};

}
}

#endif




