/**
 * TaCo - Task-Space Control Library.
 * Copyright (c) 2015-2016
 *
 * TaCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TaCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU Lesser General Public License
 * along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
 *
 * \file
 * \ingroup TaCo
 */


#ifndef TACO_COMM_SHM_SYNC_H_
#define TACO_COMM_SHM_SYNC_H_

#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>

/// \brief Boost shared memory mutex, defined for convenience.
#define TACO_SHM_SCOPED_LOCK boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex>

namespace taco {
namespace shm {

/** \brief Synchronization mechanisms for interprocess management of shared memory
 *
 * \ingroup shared_memory
 */
struct Sync
{    
    /// \brief Mutex to protect access to the queue
    boost::interprocess::interprocess_mutex mutex;

    /// \brief Initialized
    bool initialized = false;

    /// \brief Removal flag. If flag is true, everyone should close and remove.
    bool removed = false;
};

}
}

#endif
