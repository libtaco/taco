/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_COMM_SHM_FORCESENSOR_H_
#define TACO_COMM_SHM_FORCESENSOR_H_

#include <taco/comm/shm/Header.h>
#include <taco/comm/shm/Sync.h>

namespace taco {
namespace shm {

/** \brief Force sensor data that exists in shared memory
 *
 * Allows interprocess real-time data sharing.
 * Use TACO_SHM_SCOPED_LOCK(sync.mutex) before reading or writing data.
 * Use shared memory name "robot_name/taco::shm::names::ForceSensor/sensor_name"
 *
 * \ingroup shared_memory
 */
struct ForceSensor
{    
    Header  header;             /// \brief message info

    double  force[3]  = {0};    /// \brief force measurement
    double  moment[3] = {0};    /// \brief moment measurement

    Sync sync;                  /// \brief interprocess synchronization
};

}
}

#endif
