/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_COMM_SHM_JOINTSTATE_H_
#define TACO_COMM_SHM_JOINTSTATE_H_

#include <taco/comm/shm/Header.h>
#include <taco/comm/shm/Sync.h>
#include <string>
#include <iostream>

namespace taco {
namespace shm {

/** \brief Joint state data that exists in shared memory
 *
 * Allows interprocess real-time data sharing.
 * Use TACO_SHM_SCOPED_LOCK(sync.mutex) before reading or writing data.
 * Use shared memory name "robot_name/taco::shm::names::JointState"
 *
 * \param DOF The degrees of freedom of the robot, known at compile time.
 *
 * \ingroup shared_memory
 */
template <int DOF>
struct JointState
{    
    Header  header;                     /// \brief message info

    char    joint_names[DOF][STR_LEN] = {{'\0'}}; /// \brief joint names
    double  position[DOF]       = {0};  /// \brief joint positions
    double  velocity[DOF]       = {0};  /// \brief joint velocities
    double  acceleration[DOF]   = {0};  /// \brief joint accelerations
    double  force_commanded[DOF]= {0};  /// \brief commanded force or torque
    double  force_measured[DOF] = {0};  /// \brief measured force or torque
    const unsigned int dof      = DOF;  /// \brief dof of freedom of the robot, for convenience

    Sync sync;                          /// \brief interprocess synchronization

    /** \brief Print the robot state to terminal */
    void print()
    {
        std::cout <<   "seq  : " << header.seq;
        std::cout << "\ntime : " << header.stamp.sec + 0.000000001*header.stamp.nsec;
        std::streamsize ss = std::cout.precision();
        std::cout.precision(4);
        std::cout << "\nposition         : ";
        for (int i=0; i<DOF; ++i){
            std::cout << position[i] << ' ';
        }
        std::cout << "\nvelocity         : ";
        for (int i=0; i<DOF; ++i){
            std::cout << velocity[i] << ' ';
        }
        std::cout << "\nacceleration     : ";
        for (int i=0; i<DOF; ++i){
            std::cout << acceleration[i] << ' ';
        }
        std::cout << "\ncommanded torque : ";
        for (int i=0; i<DOF; ++i){
            std::cout << force_commanded[i] << ' ';
        }
        std::cout << "\nmeasured torque  : ";
        for (int i=0; i<DOF; ++i){
            std::cout << force_measured[i] << ' ';
        }
        std::cout << "\n";
        std::cout.precision(ss);
    }
};

}
}

#endif
