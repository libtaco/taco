/** \file
 * \page IPC Interprocess Communication
 *
 * TaCo include code for both ROS and shared memory interprocess communication. 
 * The @ref md_examples_EXAMPLES demonstrate the use of both. 
 * We prefer to use shared memory between controller/sensor/simulator due to the low-latency.
 * We prefer to use ROS for teleoperation for inter-computer message passing.
 *
 * ROS Messaging
 * ------------- 
 * ROS uses a pubish/subscribe model, with messages sent via TCP or UDP. 
 * It can be used for interprocess or intercomputer communication.
 *
 * | Message               | Topic                              | Description                                               |
 * | --------------------- | ---------------------------------- | --------------------------------------------------------- |
 * | @ref JointState.msg   | robot_name/JointState              | state of all joints, such as position and velocity        |
 * | @ref JointCommand.msg | robot_name/JointCommand            | control command for all joints, such as position or force |
 * | @ref ForceSensor.msg  | robot_name/ForceSensor/sensor_name | force sensor reading                                      |
 * | @ref ToolState.msg    | robot_name/ToolState               | state of a tool, such as position and velocity            |
 * | @ref ToolCommand.msg  | robot_name/ToolCommand             | control command for a tool, such as a position or force   |
 * 
 * Shared Memory Segments
 * ----------------------
 * Shared memory uses a shared memory segment between processes.
 * Therefore, it is very lowest latency and can be used in real-time applications.
 * However, TaCo's simple implementation allows only c style structs (no std strings or maps).
 * It relies on mutex locks for safe reads and writes.
 *
 * | Shared Memory                                                 | Name                               | Description                                               |
 * | ------------------------------------------------------------- | ---------------------------------- | --------------------------------------------------------- |
 * | <A HREF=JointState_8h_source.html><B>JointState.h</B></A>     | robot_name:JointState              | state of all joints, such as position and velocity        |
 * | <A HREF=JointCommand_8h_source.html><B>JointCommand.h</B></A> | robot_name:JointCommand            | control command for all joints, such as position or force |
 * | <A HREF=ForceSensor_8h_source.html><B>ForceSensor.h</B></A>   | robot_name:ForceSensor:sensor_name |force sensor reading                                       |
 * 
 */

#ifndef TACO_COMM_TOPICS_H_
#define TACO_COMM_TOPICS_H_

namespace taco {
namespace topic {
    const std::string FORCE_SENSOR  = "force_sensor"; 
    const std::string JOINT_COMMAND = "joint_command"; 
    const std::string JOINT_STATE   = "joint_state"; 
    const std::string TOOL_COMMAND  = "tool_command"; 
    const std::string TOOL_STATE    = "tool_state"; 
}
}


#endif
