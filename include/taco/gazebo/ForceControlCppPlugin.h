/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef _TACO_GAZEBO_FORCECONTROLCPPPLUGIN_H_
#define _TACO_GAZEBO_FORCECONTROLCPPPLUGIN_H_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

#include <boost/bind.hpp>
#include <string>

namespace gazebo{

/** \brief Force control plugin for gazebo.
 * Base class for inserting a controller directly in Gazebo simulation time step 
 *
 * User's controller should inherit from this plugin.
 * User should override userLoad with their controller initialization.
 * User should override userUpdate with their control loop iteration.
 *
 * \ingroup gazebo
 */
class ForceControlCppPlugin : public ModelPlugin
{
public:

    ForceControlCppPlugin(){}
    virtual ~ForceControlCppPlugin(){}

protected:

    /** \brief Called when model is loaded */
    virtual void userLoad(){}

    /** \brief Called each gazebo update */
    virtual void userUpdate(const common::UpdateInfo &){}

    /** \brief Set joint force by name */
    void setJointForce(const std::string& joint_name, const double joint_force){
        model_->GetJoint(joint_name)->SetForce(0,joint_force);
    }

    /** \brief Get joint position by name */
    double jointPosition(const std::string& joint_name){
        return model_->GetJoint(joint_name)->GetAngle(0).Radian();
    }

    /** \brief Get joint velocity by name */
    double jointVelocity(const std::string& joint_name){
        return model_->GetJoint(joint_name)->GetVelocity(0);
    }

    /** \brief Obtain the absolute path to the model at gazebo-model-dir/relative_model_path
     * \return The absolute path to the model file, or empty string if not found.
     */
    std::string findModel(const std::string relative_model_path)
    {
        std::string abs_model_path;
        const std::list<std::string> models_paths = gazebo::common::SystemPaths::Instance()->GetModelPaths();
        for (std::string path : models_paths)
        {
            std::string temp_path = path + '/' + relative_model_path;
            std::ifstream fstream(temp_path);
            if (fstream.good()){
                abs_model_path = temp_path;
               break;
            }
        }
        if (abs_model_path.empty()){
            std::cout << "WARNING. Could not find model [" + relative_model_path + "]" << std::endl;
        }
        return abs_model_path;
    }

    /** \brief Connect the world physics update step to the our callback */
    virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
        std::cout << "Initializing ModelPlugin" << std::endl;

        // Store the pointer to the model
        this->model_ = _parent;

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection_ = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&ForceControlCppPlugin::userUpdate, this, _1));

        userLoad();
    }

    /// \brief Pointer to the model
    physics::ModelPtr model_;

    /// \brief Pointer to the update event connection
    event::ConnectionPtr updateConnection_;
};

}

#endif
