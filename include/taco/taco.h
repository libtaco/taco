/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/

#ifndef TACO_TACO_H_
#define TACO_TACO_H_

/**
@namespace taco
@brief Covers all classes with the Task-space control library.
*/

/**
@defgroup dynamics Dynamic engines
*/
#include <taco/dynamics/Dynamics.h>
#include <taco/dynamics/RBDLDynamics.h>

/**
@defgroup controller Task-space controllers
*/
#include <taco/controller/math.h>
#include <taco/controller/Controller.h>
#include <taco/controller/fixed/FixedController.h>
#include <taco/controller/qp/QPController.h>
#include <taco/controller/ControlLoopTimer.h>

/**
@defgroup optimizer Convex optimization solvers.
*/
#include <taco/optimizer/QP.h>
#include <taco/optimizer/QuadProgRT.h>
#include <taco/optimizer/Regularization.h>
#include <taco/optimizer/QPRegularization.h>

/**
@defgroup shared_memory Shared memory
@namespace shm
@brief Covers all structs that exist in shared memory.
*/
#include <taco/comm/topics.h>
#include <taco/comm/copy.h>
#include <taco/comm/shm/SharedMemory.h>

/**
@defgroup filter Discrete signal filters
*/
#include <taco/filter/ButterworthFilter.h>
#include <taco/filter/AdaptPolyFilter.h>

/**
@defgroup primitive Motion primitives for a robot and human operator.
*/
#include <taco/primitive/RobotPrimitive.h>
#include <taco/primitive/UserPrimitive.h>

/**
@defgroup gazebo Plugins for gazebo.
*/

#endif
