/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef LOWPASSFILTER_H_
#define LOWPASSFILTER_H_

#include <taco/filter/DiscreteFilter.h>

namespace taco {

/**
 * \brief 2nd order low pass filter.
 *
 * \f$ \frac{\omega_n^2}{s^2+2 \zeta \omega_n s+\omega_n^2} \f$
 */
class LowPassFilter : public DiscreteFilter<3,3>
{ 
public:

    LowPassFilter(){}
    virtual ~LowPassFilter(){}
    
    /** create butterworth filter. 
     *  zeta = sqrt(2)/2 for maximally flat response.
     * \param wn natural/cutoff frequency (rad/s)
     * \param Ts sampling period (s)
     */
    void setButterworth(double wn, double Ts)
    {
        setParameters(wn,sqrt(2)/2, Ts);
    }

    /** filter = wn^2/(s^2+2*zeta*wn*s+wn^2)
     * \param wn    natural/cutoff frequency (rad/s)
     * \param zeta  damping ratio
     * \param Ts    sampling period (s)
     */
    void setParameters(double wn, double zeta, double Ts)
    {
        wn_ = wn;
        zeta_ = zeta;
        Ts_ = Ts;

        double Ts2 = Ts*Ts;
        double wn2 = wn*wn;

        num_[0] =   Ts2*wn2;
        num_[1] = 2*Ts2*wn2;
        num_[2] =   Ts2*wn2;
        den_[0] =   Ts2*wn2 + 4*zeta*Ts*wn + 4;
        den_[1] = 2*Ts2*wn2 - 8;
        den_[2] =   Ts2*wn2 - 4*zeta*Ts*wn + 4;

        this->intialize();
    }

    /** get the cutoff frequency (rad/s) */
    double getWn(){return wn_;}

    /** get the damping ratio */
    double getZeta(){return zeta_;}

    /** get the sampling frequency (s) */
    double getTs(){return Ts_;}

private:

    double wn_   = 0;
    double zeta_ = 0;
    double Ts_   = 0;

};

}
 
#endif
