/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef ADAPTPOLYFILTER_H_
#define ADAPTPOLYFILTER_H_

#include <eigen3/Eigen/Dense>
#include <vector>
#include <iostream>

namespace taco {

/** \brief Position, velocity, acceration estimates from a polynomial fit of variable window size
 *
 * This class is taken from [AdaptPolyFilter] which includes further explanation and demonstrations.
 *
 * [AdaptPolyFilter]:https://bitbucket.org/bsoe/adaptpolyfilter/src
 *
 * \ingroup filter
*/
template <typename T>
class AdaptPolyFilter
{

public:

AdaptPolyFilter()
{
    p_.Zero();
    setMaxWindowSize(50);
}

virtual ~AdaptPolyFilter(){}

/** update filter with new value */
void update(const T raw_position)
{
    // update x history by shifting
    for (int i=(max_window_size_-1); i>0; --i){
        x_[i] = x_[i-1];
    }
    x_[0] = raw_position;

    // check that 3 time steps passed, because using 2nd order polynomial
    if (t_current_ < 2){
        position_ = 0; velocity_ = 0; acceleration_ = 0;
        ++t_current_;
        return;
    }

    // compute new sums
    sum_x_[0]   = raw_position;
    sum_x2_[0]  = raw_position*raw_position;
    sum_tx_[0]  = 0;
    sum_t2x_[0] = 0;
    for (int i=1; i<max_window_size_; ++i){
        sum_x_[i]   = sum_x_[i-1]   + x_[i];
        sum_x2_[i]  = sum_x2_[i-1]  + x_[i]*x_[i];
        sum_tx_[i]  = sum_tx_[i-1]  + t_[i]*x_[i];
        sum_t2x_[i] = sum_t2x_[i-1] + t2_[i]*x_[i];
    }

    // adaptive window
    p_.Zero();
    Eigen::Vector3d B;
    for (int i=(window_sizes_.size()-1); i>=0; --i)
    {
        window_size_ = window_sizes_[i];
        unsigned int window_index = window_size_-1;

        // check that window is shorter than time elapsed
        if (window_index > t_current_){
            continue;
        }

        B[0] = sum_x_[window_index];
        B[1] = sum_tx_[window_index];
        B[2] = sum_t2x_[window_index];

        // compute least squares and residual
        p_ = A_inv_[i]*B;
        double residual = p_(0)*p_(0)*window_size_
                        + p_(1)*p_(1)*sum_t2_[window_index]
                        + p_(2)*p_(2)*sum_t4_[window_index]
                        + 2*p_(0)*p_(1)*sum_t_[window_index]
                        + 2*p_(0)*p_(2)*sum_t2_[window_index]
                        + 2*p_(1)*p_(2)*sum_t3_[window_index]
                        - 2*p_(0)*sum_x_[window_index]
                        - 2*p_(1)*sum_tx_[window_index]
                        - 2*p_(2)*sum_t2x_[window_index]
                        + sum_x2_[window_index];
        double stddev = sqrt(residual)/sqrt(window_size_);

        // check if residual small
        if (stddev <= noise_stddev_){
            break;
        }
    }

    // anti-windup
    //if (t_current_ < window_size ){
    if ((t_current_+1) < max_window_size_ ){
        p_.Zero();
        window_size_ = 0;
    }

    position_ = p_(0);
    velocity_ = -p_(1)*sampling_freq_;
    acceleration_ = 2*p_(2)*sampling_freq_*sampling_freq_;

    ++t_current_;
}

double position()    { return position_; }
double velocity()    { return velocity_; }
double acceleration(){ return acceleration_; }
double windowSize()  { return window_size_; }

/** maximum noise standard deviation */
double noise_stddev_ = 0.1;

/** sampling time of updates */
double sampling_freq_ = 1;



/** set maximum window size. default is 49.
  * window sizes start at 3 at increase exponentially. */
void setMaxWindowSize(unsigned int max_window_size)
{
    const double growth_factor = 0.1;

    std::vector<unsigned int> window_sizes;
    unsigned int window_size = 3;
    unsigned int i = 0;

    while(window_size < max_window_size)
    {
        window_sizes.push_back(window_size);
        ++i;
        window_size = round(pow(1+growth_factor,i)/growth_factor
                            -1/growth_factor + 3);
    }

    setWindowSizes(window_sizes);


    // print window sizes to terminal
    /*
    std::cout << "window sizes: " << '\n';
    for (int i=0; i<window_sizes_.size(); ++i){
        std::cout << window_sizes_[i] << ", ";
    }
    std::cout << "end" << std::endl;
    */
}

/** set all window sizes.
  * minumum value is 3. need to be in increasing order. */
void setWindowSizes(const std::vector<unsigned int>& window_sizes)
{
    window_sizes_ = window_sizes;
    max_window_size_ = window_sizes_.back();
    const int num_windows = window_sizes_.size();

    t_.resize(max_window_size_,0);
    t2_.resize(max_window_size_,0);
    t3_.resize(max_window_size_,0);
    t4_.resize(max_window_size_,0);

    sum_t_.resize(max_window_size_,0);
    sum_t2_.resize(max_window_size_,0);
    sum_t3_.resize(max_window_size_,0);
    sum_t4_.resize(max_window_size_,0);

    x_.resize(max_window_size_,0);
    sum_x_.resize(max_window_size_,0);
    sum_x2_.resize(max_window_size_,0);
    sum_tx_.resize(max_window_size_,0);
    sum_t2x_.resize(max_window_size_,0);

    A_inv_.resize(num_windows);

    // compute t, t^2, t^3, t^4
    for (int i=0; i<max_window_size_; ++i)
    {
        t_[i]  = i;
        t2_[i] = i*t_[i];
        t3_[i] = i*t2_[i];
        t4_[i] = i*t3_[i];
    }

    // compute sum of t, t^2, t^3, t^4
    for (int i=1; i<max_window_size_; ++i)
    {
        sum_t_[i]  = sum_t_[i-1]  + t_[i];
        sum_t2_[i] = sum_t2_[i-1] + t2_[i];
        sum_t3_[i] = sum_t3_[i-1] + t3_[i];
        sum_t4_[i] = sum_t4_[i-1] + t4_[i];
    }

    // compute least squares matrix
    for (int i=0; i<num_windows; ++i)
    {
        unsigned int window_size = window_sizes_[i];
        unsigned int window_index = window_sizes_[i]-1;

        Eigen::Matrix3d A;

        A << window_size,           sum_t_[window_index],  sum_t2_[window_index],
             sum_t_[window_index],  sum_t2_[window_index], sum_t3_[window_index],
             sum_t2_[window_index], sum_t3_[window_index], sum_t4_[window_index];

        A_inv_[i] = A.inverse();
    }

    // reset time
    t_current_ = 0;
}


/** return maximum window size. */
unsigned int maxWindowSize() {return window_sizes_.back();}

/** return window sizes. */
void maxWindowSizes(std::vector<unsigned int>& window_sizes){
    window_sizes = window_sizes_;
}

private:

// window variables
std::vector<unsigned int> window_sizes_;// all window sizes to try
unsigned int max_window_size_ = 0;      // max window size in window_sizes_
unsigned int window_size_ = 0;          // current window size

// precomputed variables, based upon window size
// history of time, t[0] = newest, t[end] = oldest
// for i = 1 to window size
std::vector<unsigned int> t_;       // t
std::vector<unsigned int> t2_;      // t^2
std::vector<unsigned int> t3_;      // t^3
std::vector<unsigned int> t4_;      // t^4
std::vector<unsigned int> sum_t_;   // sum {t_i}
std::vector<unsigned int> sum_t2_;  // sum {(t_i)^2}
std::vector<unsigned int> sum_t3_;  // sum {(t_i)^3}
std::vector<unsigned int> sum_t4_;  // sum {(t_i)^4}

std::vector<Eigen::Matrix3d> A_inv_;

// history of values, x[0] = newest, x[end] = oldest
std::vector<T> x_;                  // x_i
std::vector<T> sum_x_;              // sum {x_i}
std::vector<T> sum_x2_;             // sum {x_i}
std::vector<T> sum_tx_;             // sum {t_i * x_i}
std::vector<T> sum_t2x_;            // sum {(t_i)^2 * x_i}

// polynomial coefficients
// x_est = p0 + p1*x[i] + p2*x[i]^2;
Eigen::Vector3d p_;

// filtered estimated value
double position_    = 0;    // position estimate
double velocity_    = 0;    // velocity estimate
double acceleration_= 0;    // acceleration estimate

// counter of time
unsigned long long t_current_ = 0;

};

}


#endif // ADAPTPOLYFILTER_H_
