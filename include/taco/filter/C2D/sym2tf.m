function G = sym2tf(g)
g = simplifyFraction(g);
[num,den] = numden(g);
num_n     = sym2poly(num);
den_n     = sym2poly(den);
num_n     = num_n/den_n(1);
den_n     = den_n/den_n(1);
G = tf(num_n,den_n);