clc; clear;

%% s-domain tf - user specified
syms s

syms wn zeta;   
tf_s = wn^2/(s^2+2*zeta*wn*s+wn^2);   

Ts_eval = 0.001;
parameters  = {wn zeta};
values      = {2*pi*20 sqrt(2)/2};

%% bilinear tf
syms z Ts;

tf_z = subs(tf_s, s, 2/Ts*(z-1)/(z+1));
tf_z = collect(simplify(tf_z),z);
pretty(tf_z)

%return

%% numeric values
tf_z = subs(tf_z,[Ts parameters],[Ts_eval values]);
tf_z = sym2tf(tf_z)

%% verification
s = tf('s');
tf_s = subs(tf_s,parameters,values);
tf_s = sym2tf(tf_s);
tf_z = c2d(tf_s,Ts_eval,'tustin')

tf_s