/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_FILTER_ADAPTPOLYNFILTER_H_
#define TACO_FILTER_ADAPTPOLYNFILTER_H_

#include <taco/filter/Filter.h>
#include <taco/filter/AdaptPolyFilter.h>
#include <vector>

namespace taco {

/** \brief Position, velocity, acceration estimates from a polynomial fit of variable window size
 *
 * This class is taken from [AdaptPolyFilter] which includes further explanation and demonstrations.
 * It is a wrapper, that contains N instances of AdaptPolyFilter.
 *
 * [AdaptPolyFilter]:https://bitbucket.org/bsoe/adaptpolyfilter/src
 *
 * \ingroup filter
*/
template <typename T>
class AdaptPolyNFilter : public Filter
{
public:

    AdaptPolyNFilter(){}
    virtual ~AdaptPolyNFilter(){}


    void setDimension(const int dimension)
    {
        dimension_ = dimension;
        filters.resize(dimension);
        position_.resize(dimension);
        velocity_.resize(dimension);
        acceleration_.resize(dimension);

        for (int i=0; i<dimension_; ++i){
            filters_[i].noise_stddev_ = noise_stddev_;
            filters_[i].sampling_freq_ = sampling_freq_;
            filters_[i].setMaxWindowSize(max_window_size_);
        }
    }

    const Eigen::VectorXd& update(const Eigen::VectorXd& x)
    {
        for (int i=0; i<dimension_; ++i)
        {
            filters_[i].update(x(i));
            position_(i)     = filters_[i].position();
            velocity_(i)     = filters_[i].velocity();
            acceleration_(i) = filters_[i].acceleration();
        }
        return position_;
    }

    Eigen::VectorXd position()    { return position_; }
    Eigen::VectorXd velocity()    { return velocity_; }
    Eigen::VectorXd acceleration(){ return acceleration_; }

    /** maximum noise standard deviation */
    setNoiseStdDev(double noise_stddev)
    {
        noise_stddev_ = noise_stddev;
        for (int i=0; i<dimension_; ++i){
            filters_[i].noise_stddev_ = noise_stddev_;
        }
    }

    /** sampling time of updates */
    setSamplingFreq(double freq)
    {
        sampling_freq_ = freq;
        for (int i=0; i<dimension_; ++i){
            filters_[i].sampling_freq_ = sampling_freq_;
        }
    }

    /** set maximum window size. default is 49.
      * window sizes start at 3 at increase exponentially. */
    void setMaxWindowSize(unsigned int max_window_size)
    {
        max_window_size_ = max_window_size;
        for (int i=0; i<dimension_; ++i){
            filters_[i].setMaxWindowSize(max_window_size_);
        }
    }

private:

    unsigned int dimension_ = 1;

    double noise_stddev_ = 0.1;

    double sampling_freq_ = 1;

    unsigned int max_window_size_ = 50;

    std::vector<AdaptPolyFilter<double> > filters_;

    Eigen::VectorXd position_;
    Eigen::VectorXd velocity_;
    Eigen::VectorXd acceleration_;

};

}


#endif // AdaptPolyNFilter_H_
