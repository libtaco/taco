Filters
=======

The following filters are available for convenience. For additional filters, take a look at [DSPFilters].

| Filter                | Description                                                                      |
| --------------------- | -------------------------------------------------------------------------------- |
| [Adaptive Polynomial] | Fits polynomial to position, with variable window size. Estimates vel and accel. |
| Median                | Median value of window                                                           |
| Discrete              | Discrete filter. Can be used for any discrete transfer function.                 |
| LowPass               | 2nd order low pass filter. Includes 2nd order Butterworth filter.                |
| Butterworth           | 2nd order Butterworth filter. DEPRECATED, USE LOWPASS INSTEAD.                   |


[DSPFilters]:https://github.com/vinniefalco/DSPFilters
[Adaptive Polynomial]:https://bitbucket.org/bsoe/adaptpolyfilter







