/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef DISCRETEFILTER_H_
#define DISCRETEFILTER_H_

#include <taco/filter/Filter.h>

namespace taco {

/**
 * \brief Discrete Transfer Function
 * \param N_NUM size of the numerator
 * \param N_NUM size of the denominator
 *
 * See Matlab script "C2D/C2D_symbolic.m" for converting continuous time domain transfer function
 * into discrete time transfer function, to determine the coefficients a,b. <br>
 * https://en.wikipedia.org/wiki/Digital_filter
 *
 * \f$ \frac{a_0*z[t]+a_1*z[t-1]+...+a_N*z[t-N]}{b_0*z[t]+b_1*z[t-1]+...+b_M*z[t-M]} \f$
 *
 * where: <br>
 * N_NUM = N+1 = length(a) <br>
 * N_DEN = M+1 = length(b) <br>
 * num_ = a                <br>
 * den_ = b               
 */
template <size_t N_NUM, size_t N_DEN>
class DiscreteFilter : public Filter
{ 
public:

    DiscreteFilter(){}
    virtual ~DiscreteFilter(){}

    /** \brief Set the dimension of the input state.
     * \param dimension The dimension of the input state */
    virtual void setDimension(const int dimension)
    {
        // dim x N_
        dimension_ = dimension;
        x_.setZero(dimension_, N_NUM);
        y_.setZero(dimension_, N_DEN);
    }

    /** \brief Update the filter
     * \param x State vector. Each element is filtered individually. */
    virtual const Eigen::VectorXd& update(const Eigen::VectorXd& x)
    {
        for (int i=(N_NUM-1); i>0; --i){
            x_.col(i) = x_.col(i-1);        
        }
        x_.col(0) = x;

        for (int i=(N_DEN-1); i>0; --i){
            y_.col(i) = y_.col(i-1);        
        }
        y_.col(0).setZero();

        for (int i=0; i<N_NUM; ++i){
            y_.col(0) += num_[i]*x_.col(i);
        }
        for (int i=1; i<N_DEN; ++i){
            y_.col(0) -= den_[i]*y_.col(i);
        }

        out = y_.col(0);
        return out;
    }

protected:

    virtual void intialize()
    {
        for (int i=0; i<N_NUM; ++i){
            num_[i] /= den_[0]; 
        }
        for (int i=1; i<N_DEN; ++i){
            den_[i] /= den_[0]; 
        }
        den_[0] = 1;

        for (int i=0; i<N_NUM; ++i){
            std::cout << num_[i] << std::endl;
        }
        for (int i=0; i<N_DEN; ++i){
            std::cout << den_[i] << std::endl;
        }
    }

    double num_[N_NUM] = {0};
    double den_[N_DEN] = {0};

    size_t dimension_ = 0;
    Eigen::VectorXd x_;
    Eigen::VectorXd y_;

    Eigen::VectorXd out;
};

}
 
#endif
