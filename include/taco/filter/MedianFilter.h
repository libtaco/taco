/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_FILTER_MEDIANFILTER_H_
#define TACO_FILTER_MEDIANFILTER_H_

#include <taco/filter/Filter.h>
#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>

namespace taco {

/** \brief Returns the median value of the last values
 * \ingroup filter */
class MedianFilter : public Filter
{
public:

    MedianFilter(){}
    ~MedianFilter(){}

    /** \brief Set the dimension of the input state. Resets the filter.
     * \param dimension The dimension of the input state */
    void setDimension(const int dimension)
    {
        dimension_ = dimension;
        history_.resize(dimension);
        sorted_.resize(dimension);
        filtered_x_.resize(dimension);
        setWindowSize(window_size_);
    }

    /** \brief Set the window size. Resets the filter.
     * \param window The number of samples to us for the median. */
    void setWindowSize(unsigned int window)
    {
        window_size_ = window;
        for (auto& v : history_){
            v.resize(window_size_);
            std::fill(v.begin(), v.end(), 0);
        }
        for (auto& v : sorted_){
            v.resize(window_size_);
            std::fill(v.begin(), v.end(), 0);
        }
        index_ = 0;
    }

    /** \brief Update the filter
     * \param x State vector. Each element is filtered individually. */
    const Eigen::VectorXd& update(const Eigen::VectorXd& x)
    {
        for (int i=0; i<dimension_; ++i){
            history_[i][index_] = x(i);
            sorted_[i] = history_[i];
            filtered_x_(i) = median(sorted_[i]);
        }

        ++index_;
        if (index_>=window_size_){ index_=0; }

        return filtered_x_;
    }

private:

    inline double median(std::vector<double> &v)
    {
        size_t n = v.size() / 2;
        nth_element(v.begin(), v.begin()+n, v.end());
        return v[n];
    }

    unsigned int dimension_ = 0;
    unsigned int window_size_ = 5;
    unsigned int index_ = 0;

    std::vector<std::vector<double> > history_;
    std::vector<std::vector<double> > sorted_;

    Eigen::VectorXd filtered_x_;
};

}

#endif
