/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_FILTER_FILTER_H_
#define TACO_FILTER_FILTER_H_

#include <eigen3/Eigen/Dense>

namespace taco {

/** \brief Filter base class
 * \ingroup filter */
class Filter
{
public:

    Filter(){}
    virtual ~Filter(){}

    /** \brief Set the dimension of the input state.
     * \param dimension The dimension of the input state */
    virtual void setDimension(const int dimension) = 0;

    /** \brief Update the filter
     * \param x State vector. Each element is filtered individually. */
    virtual const Eigen::VectorXd& update(const Eigen::VectorXd& x) = 0;

};

}

#endif
