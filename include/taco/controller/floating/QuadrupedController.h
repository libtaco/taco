/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FLOATING_QuadrupedController_H_
#define TACO_CONTROLLER_FLOATING_QuadrupedController_H_

#include <taco/controller/floating/TaskController.h>
#include <taco/controller/floating/PositionTask.h>
#include <taco/controller/floating/OrientationTask.h>
#include <taco/controller/floating/ElbowTask.h>
#include <taco/controller/floating/JointsTask.h>

#include <Eigen/Dense>

#include <string>


namespace taco {
namespace floating {

/** \brief Operational space controller for a robotic arm.
 *
 * \param DynamicsT Dynamics engine to use, of Dynamics derived class
 *
 * Tasks from highest to lowest priority:
 * (1) Orientation of link_name
 * (2) Position of link_name
 * (3) Joint damping
 *
 * \note No assumption is made about DOF. The task hierarchy should handle various kinematic configurations.
 *
 * \ingroup floating
 */
template <class DynamicsT, class FLOATING_T>
class QuadrupedController : public taco::floating::TaskController
{
public:
    
    /** \brief Constructor for arm controller with all required entries.
     *
     * \param robot_file    Path to the urdf description of the robot
     * \param link_name     Name of end effector link to control, from the urdf description
     * \param pos_in_link   Position of the point on the link to control, relative to the link origin
     */
    QuadrupedController(const std::string& robot_file,
                  const std::string& end_effector_link_name,
                  const Eigen::Vector3d& end_effector_pos_in_link,
                  const std::string& elbow_link_name,
                  const Eigen::Vector3d& elbow_pos_in_link);

    /** \brief Destructor
     */
    virtual ~QuadrupedController();

    /// \brief Task controlling end effector position.
    taco::floating::PositionTask position_task;

    /// \brief Task controlling end effector orientation.
    taco::floating::OrientationTask orientation_task;

    /// \brief Task controlling elbow.
    taco::floating::ElbowTask elbow_task;

    /// \brief Task controlling joints.
    taco::floating::JointsTask joints_task;

protected:

    virtual void printWarning(const std::string& message) {
        std::cout << "WARNING. QuadrupedController. " << message << std::endl;
    }

};


}
}

#include <taco/controller/floating/QuadrupedController.cpp>

#endif
