/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/

#include "ElbowTask.h"

#include <iostream>

namespace taco {
namespace floating {


ElbowTask::ElbowTask() : Task("elbow")
{
    pos_.setZero();
    vel_.setZero();
    acc_.setZero();
    applied_acc_.setZero(1);
}

ElbowTask::~ElbowTask(){}


void ElbowTask::setOperationalPoint(const std::string& link_name,
                                       const Eigen::Vector3d& position_in_link)
{
    link_name_ = link_name;
    pos_in_link_ = position_in_link;
    if (dynamics_ && dynamics_->linkID(link_name_)<0){
        throwException("Link ["+link_name_+"] is not a valid link");
    }
}


void ElbowTask::initialize()
{
    if (!link_name_.empty() && dynamics_->linkID(link_name_)<0){
        throwException("Link ["+link_name_+"] is not a valid link");
    }
}


void ElbowTask::computeAcceleration()
{
    dynamics_->transform(T_link_, link_name_);
    pos_ = T_link_ * pos_in_link_;
    vel_ = Jv_*dynamics_->dq_;
    //dynamics_->acceleration(acc_, link_name_, pos_in_link_);

    applied_acc_     = lambda_inv_*desired_force_;
    applied_acc_(0) -= damping_*vel_(2);

    // check if enabled
    if (!enabled_){ applied_acc_.setZero(); }
}

void ElbowTask::computeModel()
{
    dynamics_->Jv(Jv_, link_name_, pos_in_link_);
    J_ = Jv_.row(2);
    computeNullSpace();
}

void ElbowTask::printInfo()
{
    std::cout << "goal        : " << desired_force_ << "\n";
    std::cout << "applied acc : " << applied_acc_.transpose() << "\n";
}

// --------------------------------
// Task-specific functions
// --------------------------------

const Eigen::Vector3d& ElbowTask::position(bool force_update)
{
    if (force_update){
        dynamics_->position(pos_,link_name_,pos_in_link_);
    }
    return pos_;
}

const Eigen::Vector3d& ElbowTask::velocity(bool force_update)
{
    if (force_update){
        dynamics_->velocity(vel_,link_name_,pos_in_link_);
    }
    return vel_;
}

const Eigen::Vector3d& ElbowTask::acceleration(bool force_update)
{
    if (force_update){
        dynamics_->acceleration(acc_, link_name_, pos_in_link_);
    }
    return acc_;
}


}
} /* namespace taco */
