/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FLOATING_POSITIONTASK_H_
#define TACO_CONTROLLER_FLOATING_POSITIONTASK_H_

#include "GoalTask.h"

#include <string>
#include <fstream>
#include <iostream>
#include <limits>


namespace taco {
namespace floating {


/** \brief Control the position of link relative to the ground frame.
 *
 * \ingroup floating
 */
class PositionTask: public GoalTask<Eigen::Vector3d, Eigen::Vector3d> {
public:
    PositionTask();
    virtual ~PositionTask();

    virtual void initialize();
    virtual void computeAcceleration();
    virtual void computeModel();
    virtual void printInfo();

    // --------------------------------
    // Task-specific functions
    // --------------------------------

    /** \brief Set operational point.
     * \param link_name The name of the link that the operational point is defined
     * \param position_in_link The position of operational point in that link
     */
    void setOperationalPoint(const std::string& link_name,
                             const Eigen::Vector3d& position_in_link = Eigen::Vector3d::Zero() );

    virtual const Eigen::Vector3d& position(bool force_update = false);

    virtual const Eigen::Vector3d& velocity(bool force_update = false);

    virtual const Eigen::Vector3d& acceleration(bool force_update = false);


    /// \brief The name of the link that the operational point is defined
    std::string link_name_;

    /// \brief The position of operational point in the link
    Eigen::Vector3d pos_in_link_ = Eigen::Vector3d::Zero();

    /// \brief Position gain
    double Kp_ = 100;

    /// \brief Velocity gain
    double Kv_ =  20;

    /// \brief Set the maximum force applied to the operational point, in the operational space.
    /// If the computed operational space force exceeds this value, it will be normalized to this max.
    double max_acceleration_ = 1000;

private:

    /// \brief Tranformation matrix of the link
    Eigen::Affine3d T_link_;

};

}
} /* namespace taco */
#endif /* PositionTask_H_ */
