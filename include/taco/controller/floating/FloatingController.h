/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FLOATING_FLOATINGCONTROLLER_H_
#define TACO_CONTROLLER_FLOATING_FLOATINGCONTROLLER_H_

/**
@defgroup floating Quadratic program approximation of operational space control with fixed base.
\ingroup controller

\namespace floating
\brief Covers all classes within the floating operation space controller with fixed base.
*/

#include <taco/controller/floating/TaskController.h>
#include <taco/controller/floating/Task.h>
#include <taco/controller/floating/PositionTask.h>
#include <taco/controller/floating/OrientationTask.h>
#include <taco/controller/floating/JointsTask.h>

#endif
