/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FLOATING_TASKCONTROLLER_H_
#define TACO_CONTROLLER_FLOATING_TASKCONTROLLER_H_

#include <taco/controller/Controller.h>
#include <taco/controller/constraint/Constraint.h>
#include <taco/optimizer/QP.h>

#include <Eigen/Core>

#include <string>
#include <map>
#include <vector>
#include <set>


namespace taco {
namespace floating {

class Task;

/** \brief Operational space controller.
 *
 * A hierarchical control formulation, that gives an analytical solution
 * of a mass weighted optimization problem to solve inverse dynamics.
 *
 * Reference:
 * Hutter, Marco, et al.
 * "Quadrupedal locomotion using hierarchical operational space control."
 * The International Journal of Robotics Research (2014).
 *
 * \ingroup floating
 */
class TaskController : public taco::Controller {

    friend class Task;

public:

    TaskController();

    virtual ~TaskController();

    /** \brief Set the QP solver for all tasks. Each task will get it's own copy of this solver.
     * \note Ensure that the QP solver copy method has the desired behavior.
     */
    virtual void setQPSolver(taco::QP* solver);

    /** \brief Cost of the last solution */
    virtual double cost(){return cost_;}

    /** \brief Compute total control force from all tasks.
     * \return The compute joint torques.
     */
    virtual const Eigen::VectorXd& computeTorque();

    /** \brief Set the controller's status to enabled or diabled.
     *
     * Performs any initialization or destruction required when enabling or disabling the controller.
     *
     * \param enabled   the desired status of the controller.
     */
    virtual void setEnabled(bool enabled);

    /** \brief Add a task to the controller
     * \param task An operational space task
     * \param priority The priority of the task, where 0 is the highest priority.
     */
    virtual void addTask( Task* task , unsigned int priority);

    /** \brief Add a contact constraint to the controller
     * \param contraint An operational space constraint
     * \param name The name of the constraint
     */
    virtual void addConstraint( Constraint* task );

    /** \brief The number of control cycles since enabling.
     * \ return The number of calls to computeTorque() since enabling.
     */
    unsigned long long updateCount() {return update_count_;}

    /** \brief print controller and task info to terminal */
    virtual void printInfo();

    /// \brief Tasks, ordered by priority
    std::vector<Task*> tasks_;

    /// \brief Tasks, ordered by name
    std::map<std::string, Task*> task_lookup_;

protected:

    /// \brief Compute the robot model, such as mass matrix
    void computeRobotModel();

    /// \brief compute all task models
    void computeTaskModels();

    /// \brief compute all task torques
    void computeTaskTorques();

    /** \brief Compute joint acceleration from desired operational space acceleration,
     * i.e. compute this->ddq_ from this->ddx_
     *
     *      Let ddx_desired = stacked desired operational space acceleration
     *      Let ddq         = joint acceleration to achieve ddx
     *      Let J           = stacked task jacobians
     *
     *      cost = ||ddx_desired_ - J ddq_|| + ddq^T M ddq
     *           = ddq^T (J^T*J + M) ddq - 2 ddx_desired^T J ddq
     */
    bool computeRegularization();

    /// \brief the number of times computeTorque() must be called
    /// before computeTaskModels() is called again.
    /// Higher values reduce computational cost, but sacrifice accuracy and stability
    const int model_update_period_ = 1;

    /// \brief the number of calls to computeTorque() since enabling
    unsigned long long update_count_ = 0;

    /// \brief joint forces caused by gravity
    Eigen::VectorXd gravity_;

    /// \brief joint forces caused by coriolis terms
    Eigen::VectorXd coriolis_;

public: // SHOULD BE PROTECTED, but derived tasks must access so had to make public >.<

    /// \brief Mass matrix
    Eigen::MatrixXd M_;

    /// \brief Inverse of the mass matrix
    Eigen::MatrixXd M_inv_;

    /// \brief N_[i] is the null space of task[i-1], and range space of task i
    std::vector<Eigen::MatrixXd> N_;

    /// \brief The stacked Jacobians of all the tasks
    Eigen::MatrixXd J_;

    /// \brief The stacked operational space goals (applied_acc_) of all the tasks
    Eigen::VectorXd ddx_;

    /// \brief Joint acceleration command.
    /// Used to calculate joint force, from F_ = M*ddq_
    /// Used as the free variable in the optimization
    Eigen::VectorXd ddq_;

    /// \brief Constraint jacobian
    Eigen::VectorXd Jc_;

    /// \brief Convex optimizer.
    QP* solver_ = NULL;

    /// \brief Cost of last optimization solve
    double cost_ = 0;

    /// \brief weight assigned to the regularization weighting term.
    double regularization_weight_ = 0.01;
};

}
} /* namespace taco */
#endif
