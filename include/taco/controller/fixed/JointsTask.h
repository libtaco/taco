/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_JOINTSTASK_H_
#define TACO_CONTROLLER_FIXED_JOINTSTASK_H_

#include "GoalTask.h"

namespace taco {
namespace fixed {

/** \brief Control all joints indepenently.
 *
 * \ingroup fixed
 */
class JointsTask: public GoalTask<Eigen::VectorXd, Eigen::VectorXd> {
public:
    JointsTask();
    virtual ~JointsTask();

    virtual void initialize();
    virtual void computeTorque();
    virtual void computeModel();
    virtual void printInfo();

    // --------------------------------
    // Task-specific functions
    // --------------------------------

    virtual const Eigen::VectorXd& position(bool force_update = false);

    virtual const Eigen::VectorXd& velocity(bool force_update = false);

    virtual const Eigen::VectorXd& acceleration(bool force_update = false);


    /// \brief Position gain
    Eigen::VectorXd Kp_; // 100;

    /// \brief Velocity gain
    Eigen::VectorXd Kv_; // 20;

    /// \brief Set the maximum force applied to the operational point, in the operational space.
    /// If the computed operational space force exceeds this value, it will be normalized to this max.
    Eigen::VectorXd max_force_; // 1000;

};

}
} /* namespace taco */
#endif /* JointsTask_H_ */
