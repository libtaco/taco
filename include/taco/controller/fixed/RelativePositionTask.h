/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_RELATIVEPOSITIONTASK_H_
#define TACO_CONTROLLER_FIXED_RELATIVEPOSITIONTASK_H_

#include "GoalTask.h"

#include <string>


namespace taco {
namespace fixed {


/** \brief Control the position of a link relative to another link.
 *
 *  \note This task is untested, and may not work properly. Please test before using.
 *
 * \ingroup fixed
 */
class RelativePositionTask: public GoalTask<Eigen::Vector3d, Eigen::Vector3d> {
public:

    RelativePositionTask();
    virtual ~RelativePositionTask();

    virtual void initialize();
    virtual void computeTorque();
    virtual void computeModel();
    virtual void printInfo();

    // --------------------------------
	// Task-specific functions
	// --------------------------------

    /** set operational point. will check if link exists. */
    void setOperationalPoint(const std::string& link_name,
                             const Eigen::Vector3d& position_in_link = Eigen::Vector3d::Zero() );

    /** set reference point. will check if link exists.
     *  default is ground, zero vector (i.e. Position Task). */
    void setReferencePoint(const std::string& link_name,
                             const Eigen::Vector3d& position_in_link = Eigen::Vector3d::Zero() );

    /** set reference frame of operational point.
     *  default is ground (i.e. PositionTask). */
    void setReferenceFrame(const std::string& link_name);

    const Eigen::Vector3d& position(bool force_update = false);

    const Eigen::Vector3d& velocity(bool force_update = false);

    const Eigen::Vector3d& acceleration(bool force_update = false);


    // --------------------------------
    // Task-specific variables
    // --------------------------------
    std::string       op_link_name_;
    Eigen::Vector3d   op_pos_in_link_;
    std::string       ref_link_name_       = "ground";
    Eigen::Vector3d   ref_pos_in_link_     = Eigen::Vector3d::Zero();
    std::string       ref_frame_link_name_ = "ground";

    /// \brief Position gain
    double Kp_ = 100;

    /// \brief Velocity gain
    double Kv_ =  20;

    /// \brief Set the maximum force applied to the operational point, in the operational space.
    /// If the computed operational space force exceeds this value, it will be normalized to this max.
    double max_force_ = 1000;


private:

    /// \brief Reference frame of the relative position
    Eigen::Matrix3d ref_frame_T_;

    /// \brief Tranformation of the operational link
    Eigen::Affine3d T_op_link_;

    /// \brief Transformation of the reference link
    Eigen::Affine3d T_ref_link_;


};

}
} /* namespace taco */
#endif /* RelativePositionTask_H_ */
