/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "ElbowTask.h"

#include <iostream>
#include <stdexcept>

namespace taco {
namespace fixed {


ElbowTask::ElbowTask() : Task("elbow")
{
    pos_ee_.setZero();
    pos_elbow_.setZero();
    vel_elbow_.setZero();

    static_assert(std::numeric_limits<float>::is_iec559, "ElbowTask. IEEE 754 required for infinity");
}

ElbowTask::~ElbowTask(){}


void ElbowTask::initialize()
{
    if (!link_name_ee_.empty() && dynamics_->linkID(link_name_ee_)<0){
        throwException("Link ["+link_name_ee_+"] is not a valid link");
    }
    if (!link_name_elbow_.empty() && dynamics_->linkID(link_name_elbow_)<0){
        throwException("Link ["+link_name_elbow_+"] is not a valid link");
    }
}


void ElbowTask::setEndEffector(const std::string& link_name,
                               const Eigen::Vector3d& position_in_link)
{
    link_name_ee_ = link_name;
    pos_in_link_ee_ = position_in_link;
    if (dynamics_ && dynamics_->linkID(link_name_ee_)<0){
        throwException("Link ["+link_name_ee_+"] is not a valid link");
    }
}


void ElbowTask::setElbow(const std::string& link_name,
                         const Eigen::Vector3d& position_in_link)
{
    link_name_elbow_ = link_name;
    pos_in_link_elbow_ = position_in_link;
    if (dynamics_ && dynamics_->linkID(link_name_elbow_)<0){
        throwException("Link ["+link_name_elbow_+"] is not a valid link");
    }
}


void ElbowTask::computeTorque()
{
    dynamics_->transform(T_link_ee_, link_name_ee_);
    pos_ee_ = T_link_ee_ * pos_in_link_ee_;

    dynamics_->transform(T_link_elbow_, link_name_elbow_);
    pos_elbow_ = T_link_elbow_ * pos_in_link_elbow_;
    vel_elbow_ = J_elbow_*dynamics_->dq_;

    Eigen::VectorXd force_applied;

    if (std::isinf(ceiling_)){
        // only want to raise elbow, so apply upward force
        force_applied.resize(1);
        force_applied(0) = applied_acc_ - damping_*vel_elbow_(2);
    }
    else {
        // calculate distance penetrated into ceiling
        dist_penetrated_ = pos_elbow_(2) - ceiling_;
        if (dist_penetrated_<0){ dist_penetrated_=0; }

        force_applied.resize(2);
        force_applied(0) = - damping_*vel_elbow_(1);
        force_applied(1) = - damping_*vel_elbow_(2) + applied_acc_/(1.0+dist_penetrated_) ;

        if (dist_penetrated_==0){
            lean_ = LEAN_NONE;
        }
        else {
            // penetration so lean the elbow
            switch (lean_){
                case LEAN_NONE:
                {
                    double relative_angle = ee_angle_ - midplane_angle_;
                    if (0 <= relative_angle && relative_angle <= M_PI/2 ){
                        lean_ = LEAN_LEFT;
                    } else if (M_PI/2 <= relative_angle && relative_angle <= M_PI ){
                        lean_ = LEAN_RIGHT;
                    } else if (-M_PI/2 <= relative_angle && relative_angle <= 0 ){
                        lean_ = LEAN_RIGHT;
                    } else if (-M_PI <= relative_angle && relative_angle <= -M_PI/2 ){
                        lean_ = LEAN_LEFT;
                    } else {
                        printWarning("Bad relative angle. This should never happen.");
                    }
                    // don't break, let's now handle the leaning with below cases
                }
                case LEAN_RIGHT:
                {
                    force_applied(0) += repulsive_gain_*dist_penetrated_;
                    force_applied(1) -= repulsive_gain_*dist_penetrated_;
                    break;
                }
                case LEAN_LEFT:
                {
                    force_applied(0) -= repulsive_gain_*dist_penetrated_;
                    force_applied(1) -= repulsive_gain_*dist_penetrated_;
                    break;
                }
            } //swich lean
        } // if not penetrated
    } // if ceiling is not infinity

    applied_force_ = lambda_bounded_*force_applied;

    /*
    // saturate force
    double F_norm = applied_force_.norm();
    if (F_norm > max_force_){
        applied_force_ = (max_force_/F_norm) * applied_force_;
    }
    */

    // compute joint torques
    F_ = JN_T_ * applied_force_;

    // check if enabled
    if (!enabled_){ F_.setZero(); }
}

void ElbowTask::computeModel()
{
    // find angle that the end-effector is pointed towards
    Eigen::Vector3d pos_base;
    dynamics_->position(pos_ee_, link_name_ee_, pos_in_link_ee_);
    dynamics_->position(pos_base, dynamics_->linkName(0), pos_in_link_ee_);
    Eigen::Vector3d dpos = pos_ee_ - pos_base;
    ee_angle_ = atan2(dpos(1),dpos(0));

    // z rotation for that angle
    Eigen::Matrix3d zrot;
    zrot << cos(ee_angle_), -sin(ee_angle_), 0,
            sin(ee_angle_),  cos(ee_angle_), 0,
                   0      ,          0     , 1;

    // rotate jacobian into frame that points along end effector
    dynamics_->Jv(J_elbow_, link_name_elbow_, pos_in_link_elbow_);
    J_elbow_ = zrot.transpose()*J_elbow_;

    if (std::isinf(ceiling_)){
        // jacobian is z direction only
        J_ = J_elbow_.row(2);
    } else {
        // jacobian is y and z direction
        J_ = J_elbow_.bottomRows(2);
    }

    computeNullSpace();
}

void ElbowTask::printInfo()
{
    std::cout << "force       : " << applied_force_.transpose() << "\n";
    std::cout << "torque      : " << F_.transpose() << "\n";
    std::cout << "angle(deg)  : " << ee_angle_*180.0/M_PI << "\n";
    std::cout << "height      : " << pos_elbow_(2) << '\n';
    std::cout << "penetration : " << dist_penetrated_ << '\n';
    std::cout << "lean        : " << lean_ << '\n';
    std::cout << "velocity    : " << vel_elbow_ << "\n";
}

// --------------------------------
// Task-specific functions
// --------------------------------



}
} /* namespace taco */
