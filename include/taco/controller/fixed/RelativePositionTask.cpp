/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "RelativePositionTask.h"


#include <iostream>

namespace taco {
namespace fixed {


RelativePositionTask::RelativePositionTask() : GoalTask("relative_position")
{
    desired_pos_.setZero();
    desired_vel_.setZero();
    desired_acc_.setZero();
    desired_force_.setZero();
    pos_.setZero();
    vel_.setZero();
    acc_.setZero();
    applied_force_.setZero(3);
}

RelativePositionTask::~RelativePositionTask() {}

void RelativePositionTask::initialize()
{
    if (!op_link_name_.empty() && dynamics_->linkID(op_link_name_)<0){
        throwException("Link ["+op_link_name_+"] is not a valid link");
    }
    if (!ref_link_name_.empty() && dynamics_->linkID(ref_link_name_)<0){
        throwException("Link ["+ref_link_name_+"] is not a valid link");
    }
}

void RelativePositionTask::computeTorque()
{
    if (ref_frame_link_name_ == "ground"){
        T_ref_link_.rotate( Eigen::Matrix3d::Identity() );
        T_ref_link_.translate( Eigen::Vector3d::Zero() );
        ref_frame_T_ = T_ref_link_.rotation().transpose();
    } else {
        dynamics_->transform(T_ref_link_, ref_frame_link_name_);
        ref_frame_T_ = T_ref_link_.rotation().transpose();
    }

    dynamics_->transform(T_op_link_, op_link_name_);
    pos_ = ref_frame_T_*(T_op_link_ * op_pos_in_link_ - T_ref_link_ * ref_pos_in_link_);
    vel_ = ref_frame_T_*(J_*dynamics_->dq_);
    Eigen::Vector3d op_acc, ref_acc;
    dynamics_->acceleration(op_acc, op_link_name_,op_pos_in_link_);
    dynamics_->acceleration(ref_acc, ref_link_name_,ref_pos_in_link_);
    acc_ = ref_frame_T_*(op_acc - ref_acc);


    switch (control_type_) {
        case CONTROL_POS:
            applied_force_ = lambda_bounded_ * T_ref_link_.rotation() * ( Kp_ * (desired_pos_ - pos_)
                                                                         +Kv_ * (desired_vel_ - vel_));
            break;
        case CONTROL_VEL:
            applied_force_ = lambda_bounded_ * T_ref_link_.rotation() * Kv_ * (desired_vel_ - vel_);
            break;
        case CONTROL_ACC:
            applied_force_ = lambda_bounded_ * T_ref_link_.rotation() * desired_acc_;
            //applied_force_ = lambda_bounded_ * (desired_acc_ - dJ_*dynamics_->dq_); // correct
            break;
        case CONTROL_FORCE:
            applied_force_ = desired_force_;
            break;
    }

    // saturate force
    double F_norm = applied_force_.norm();
    if (F_norm > max_force_){
        applied_force_ = (max_force_/F_norm) * applied_force_;
    }

    // compute joint torques
    F_ = JN_T_ * applied_force_;

    // check if enabled
    if (!enabled_){ F_.setZero(); }

    printWarning("Untested!");

}

void RelativePositionTask::computeModel()
{
    //task jacobian
    dynamics_->Jv(J_, op_link_name_, op_pos_in_link_);

    // remove all generalized coordinates before the reference link
    int ref_link_id = dynamics_->linkID(ref_link_name_);
    for (int i=0; i<=ref_link_id; ++i){
        J_.col(i) = Eigen::Vector3d(0,0,0);
    }

    computeNullSpace();
}

void RelativePositionTask::printInfo()
{
    std::cout << "goal   : " << desired_pos_.transpose() << "\n";
    std::cout << "pos    : " << pos_.transpose() << "\n";
    std::cout << "vel    : " << vel_.transpose() << "\n";
    std::cout << "force  : " << applied_force_.transpose() << "\n";
    std::cout << "torque : " << F_.transpose() << "\n";
}

// --------------------------------
// Task-specific functions
// --------------------------------

void RelativePositionTask::setOperationalPoint(const std::string& link_name,
                                               const Eigen::Vector3d& position_in_link)
{
    op_link_name_ = link_name;
    op_pos_in_link_ = position_in_link;
    if (dynamics_ && dynamics_->linkID(op_link_name_)<0){
        throwException("Link ["+op_link_name_+"] is not a valid link");
    }
}

void RelativePositionTask::setReferencePoint(const std::string& link_name,
                                       const Eigen::Vector3d& position_in_link)
{
    ref_link_name_ = link_name;
    ref_pos_in_link_ = position_in_link;
    if (dynamics_ && dynamics_->linkID(ref_link_name_)<0){
        throwException("Link ["+ref_link_name_+"] is not a valid link");
    }
}

void RelativePositionTask::setReferenceFrame(const std::string& link_name)
{
    ref_frame_link_name_ = link_name;
}

const Eigen::Vector3d& RelativePositionTask::position(bool force_update)
{
    if (force_update){
        if (ref_frame_link_name_ == "ground"){
            T_ref_link_.rotate( Eigen::Matrix3d::Identity() );
            T_ref_link_.translate( Eigen::Vector3d::Zero() );
            ref_frame_T_ = T_ref_link_.rotation().transpose();
        } else {
            dynamics_->transform(T_ref_link_, ref_frame_link_name_);
            ref_frame_T_ = T_ref_link_.rotation().transpose();
        }
        dynamics_->transform(T_op_link_, op_link_name_);
        pos_ = ref_frame_T_*(T_op_link_ * op_pos_in_link_ - T_ref_link_ * ref_pos_in_link_);
    }
    return pos_;
}

const Eigen::Vector3d& RelativePositionTask::velocity(bool force_update)
{
    if (force_update){
        dynamics_->Jv(J_, op_link_name_, op_pos_in_link_);
        vel_ = ref_frame_T_*(J_*dynamics_->dq_);
    }
    return vel_;
}

const Eigen::Vector3d& RelativePositionTask::acceleration(bool force_update)
{
    if (force_update){
        Eigen::Vector3d op_acc, ref_acc;
        dynamics_->acceleration(op_acc, op_link_name_,op_pos_in_link_);
        dynamics_->acceleration(ref_acc, ref_link_name_,ref_pos_in_link_);
        acc_ = ref_frame_T_*(op_acc - ref_acc);

    }
    return acc_;
}


}
} /* namespace taco */
