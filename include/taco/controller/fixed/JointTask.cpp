/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "JointTask.h"

#include <iostream>

namespace taco {
namespace fixed {

JointTask::JointTask() : GoalTask("joint")
{
    applied_force_.setZero(1);
}

JointTask::~JointTask() {}

void JointTask::initialize()
{
    if (!joint_name_.empty()){
        joint_id_ = dynamics_->jointID(joint_name_);
    } else if (joint_id_>=0){
        joint_name_ = dynamics_->jointName(joint_id_);
    }
}

void JointTask::computeTorque()
{
    pos_ = dynamics_->q_(joint_id_);
    vel_ = dynamics_->dq_(joint_id_);
    acc_ = dynamics_->ddq_(joint_id_);

    switch (control_type_) {
        case CONTROL_POS:
            applied_force_(0) = lambda_(1,1) * (Kp_ * (desired_pos_ - pos_) + Kv_ * (desired_vel_ - vel_));
            break;
        case CONTROL_VEL:
            applied_force_(0) = lambda_(1,1) * Kv_ * (desired_vel_ - vel_);
            break;
        case CONTROL_ACC:
            applied_force_(0) = lambda_(1,1) * desired_acc_;
            //force_ = lambda_ * (desired_acc_ - dJ_*dynamics_->dq_); // correct
            break;
        case CONTROL_FORCE:
            applied_force_(0) = desired_force_;
            break;
    }

    // saturate force
    if (applied_force_(0) > max_force_){
        applied_force_(0) = max_force_;
    }
    else if (applied_force_(0) < -max_force_){
        applied_force_(0) = -max_force_;
    }

    // compute joint torques
    // F_ = JN_T_ * applied_force_ = N_.row(i) * applied_force_
    F_ = (controller_->N_[priority_].transpose()).col(joint_id_) * applied_force_(0);

    // check if enabled
    if (!enabled_){ F_.setZero(); }
}

void JointTask::computeModel()
{
    // We could set J_ and computeNullSpace(),
    // but for computational efficiency, we compute it directly

    //J_ = Eigen::MatrixXd::Zero(1,dynamics_->DOF());
    //J_(joint_id_) = 1;
    //computeNullSpace();

    int dof = dynamics_->DOF();
    N_.setIdentity(dof,dof);
    N_(joint_id_, joint_id_) = 0;

    double mass = controller_->M_(joint_id_, joint_id_);
    lambda_.setConstant(1,1,mass);
}


// --------------------------------
// Task-specific functions
// --------------------------------

bool JointTask::setJoint(const std::string& joint_name)
{
    joint_name_ = joint_name;
    joint_id_ = -1;
    if (dynamics_){
        initialize();
    }
}

void JointTask::setJoint(const int joint_id)
{
    joint_name_.clear();
    joint_id_ = joint_id;
    if (dynamics_){
        initialize();
    }
}

const double& JointTask::position(bool force_update)
{
    if (force_update){
        pos_ = dynamics_->q_(joint_id_);
    }
    return pos_;
}

const double& JointTask::velocity(bool force_update)
{
    if (force_update){
        vel_ = dynamics_->dq_(joint_id_);
    }
    return vel_;
}

const double& JointTask::acceleration(bool force_update)
{
    if (force_update){
        acc_ = dynamics_->ddq_(joint_id_);
    }
    return acc_;
}

}
} /* namespace taco */
