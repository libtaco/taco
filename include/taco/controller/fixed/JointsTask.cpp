/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "JointsTask.h"

#include <limits>
#include <iostream>

namespace taco {
namespace fixed {

JointsTask::JointsTask() : GoalTask("joints") {}

JointsTask::~JointsTask() {}


void JointsTask::initialize()
{
    int dof = dynamics_->DOF();

    // initialize size
    F_.setZero(dof);
    N_.setZero(dof,dof);
    applied_force_.setZero(dof);

    // initialize default values
    std::cout << "inializing joints task vectors" << std::endl;
    if (desired_pos_.size()     ==0){ desired_pos_.setConstant(dof,  0); }
    if (desired_vel_.size()     ==0){ desired_vel_.setConstant(dof,  0); }
    if (desired_acc_.size()     ==0){ desired_acc_.setConstant(dof,  0); }
    if (desired_force_.size()   ==0){ desired_force_.setConstant(dof,  0); }
    if (Kp_.size()              ==0){ Kp_.setConstant(dof, 100); }
    if (Kv_.size()              ==0){ Kv_.setConstant(dof,  20); }
    if (max_force_.size()       ==0){ max_force_.setConstant(dof, 1000); }
}


void JointsTask::computeTorque()
{
    pos_ = dynamics_->q_;
    vel_ = dynamics_->dq_;
    acc_ = dynamics_->ddq_;

    switch (control_type_) {
        case CONTROL_POS:
            applied_force_ = controller_->M_ * ( Kp_.cwiseProduct(desired_pos_ - pos_)
                                                +Kv_.cwiseProduct(desired_vel_ - vel_));
            break;
        case CONTROL_VEL:
            applied_force_ = controller_->M_ * Kv_.cwiseProduct(desired_vel_ - vel_);
            break;
        case CONTROL_ACC:
            applied_force_ = controller_->M_ * desired_acc_;
            break;
        case CONTROL_FORCE:
            applied_force_ = desired_force_;
            break;
    }

    // saturate force
    applied_force_ = applied_force_.cwiseMin(max_force_);
    applied_force_ = applied_force_.cwiseMax(-max_force_);

    // compute joint torques
    // F_ = JN_T_ * applied_force_ = N_T_ * applied_force_
    F_ = (controller_->N_[priority_].transpose()) * applied_force_;

    // check if enabled
    if (!enabled_){ F_.setZero(); }
}

void JointsTask::computeModel()
{
    // We could set J_ and computeNullSpace(),
    // but for computational efficiency, we compute it directly

    //int dof = dynamics_->DOF();
    //J_ = Eigen::MatrixXd::Identity(dof,dof);
    //computeNullSpace();

    // check if enabled
    if (enabled_){
        int dof = dynamics_->DOF();
        N_.setZero(dof,dof);
    } else {
        N_ = controller_->N_[priority_];
    }
}

void JointsTask::printInfo()
{
    std::cout << "goal   : " << desired_pos_.transpose()   << "\n";
    std::cout << "pos    : " << pos_.transpose()           << "\n";
    std::cout << "vel    : " << vel_.transpose()           << "\n";
    std::cout << "force  : " << applied_force_.transpose() << "\n";
    std::cout << "torque : " << F_.transpose()             << "\n";
}

const Eigen::VectorXd& JointsTask::position(bool force_update)
{
    if (force_update){
        pos_ = dynamics_->q_;
    }
    return pos_;
}

const Eigen::VectorXd& JointsTask::velocity(bool force_update)
{
    if (force_update){
        vel_ = dynamics_->dq_;
    }
    return vel_;
}

const Eigen::VectorXd& JointsTask::acceleration(bool force_update)
{
    if (force_update){
        acc_ = dynamics_->ddq_;
    }
    return acc_;
}


}
} /* namespace taco */
