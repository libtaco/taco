/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "JointLimitsTask.h"

#include <iostream>
#include <limits>

namespace taco {
namespace fixed {

JointLimitsTask::JointLimitsTask() : Task("joint_limits") {}

JointLimitsTask::~JointLimitsTask() {}


void JointLimitsTask::initialize()
{
    int dof = dynamics_->DOF();

    // initialize size
    N_.setIdentity(dof,dof);
    F_.setZero(dof);
    applied_force_.setZero(dof);

    F_lower_.setZero(dof);
    F_upper_.setZero(dof);
    F_star_.setZero(dof);

    dist_lower_null_.setZero( dof);
    dist_upper_null_.setZero( dof);
    dist_lower_repel_.setZero(dof);
    dist_upper_repel_.setZero(dof);

    // set default maximum force
    if (F_max_.size()       ==0){ F_max_.setConstant(dof, 100);   }

    // set default limits +-infinity
    if (limit_lower_.size() ==0){ limit_lower_.setConstant(dof, -std::numeric_limits<double>::infinity()); }
    if (limit_upper_.size() ==0){ limit_upper_.setConstant(dof,  std::numeric_limits<double>::infinity()); }

    // set default gain and buffer
    if (Kp_.size()          ==0){ Kp_.setConstant(dof,100); }
    if (Kd_.size()          ==0){ Kd_.setConstant(dof, 20); }
    if (exponent_.size()    ==0){ exponent_.setConstant(dof,2); }
    if (null_buffer_.size() ==0){ null_buffer_.setConstant( dof, 0.131);}
    if (repel_buffer_.size()==0){ repel_buffer_.setConstant(dof, 0.131);}
}


void JointLimitsTask::computeTorque()
{
    int dof = dynamics_->DOF();

    F_lower_.setZero();
    F_upper_.setZero();

    // distance into null space buffer
    dist_lower_null_  = (-dynamics_->q_ + (limit_lower_+repel_buffer_+null_buffer_)).cwiseQuotient(null_buffer_);
    dist_upper_null_  = ( dynamics_->q_ - (limit_upper_-repel_buffer_-null_buffer_)).cwiseQuotient(null_buffer_);

    // distance into repel buffer
    dist_lower_repel_ = (-dynamics_->q_ + (limit_lower_+repel_buffer_)).cwiseQuotient(repel_buffer_);
    dist_upper_repel_ = ( dynamics_->q_ - (limit_upper_-repel_buffer_)).cwiseQuotient(repel_buffer_);

    for (int i=0; i<dof; ++i){

        // repulsive force
        if (dist_lower_repel_(i)>0){
            F_lower_(i) += Kp_(i)*pow(dist_lower_repel_(i), exponent_(i));
            F_lower_(i) -= Kd_(i)*dynamics_->dq_(i);
            //std::cout << "in lower repel buffer: " << dynamics_->linkName(i) << std::endl;
        }
        if (dist_upper_repel_(i)>0){
            F_upper_(i) -= Kp_(i)*pow(dist_upper_repel_(i), exponent_(i));
            F_upper_(i) -= Kd_(i)*dynamics_->dq_(i);
            //std::cout << "in upper repel buffer: " << dynamics_->linkName(i) << std::endl;
        }

        // null space
        // if near joint limit, controller pushing farther into it
        // then scale back controller's ability to control joint
        if (dist_lower_null_(i)>0 && (controller_->F_(i)<0)){
            // near lower limit
            N_(i,i) = 1-dist_lower_null_(i);
            if (N_(i,i)<epsilon_){
                N_(i,i) = epsilon_;
            }
            std::cout << "in lower null buffer: " << dynamics_->linkName(i) << std::endl;
        } else {
            N_(i,i) = 1;
        }
        if ((dist_upper_null_(i)>0) && (controller_->F_(i)>0)){
            // near upper limit
            N_(i,i) = 1-dist_upper_null_(i);
            if (N_(i,i)<epsilon_){
                N_(i,i) = epsilon_;
            }
            std::cout << "in upper null buffer: " << dynamics_->linkName(i) << std::endl;
        } else {
            N_(i,i) = 1;
        }
    }

    // saturate force
    F_star_ = F_lower_ + F_upper_;
    F_star_ = F_star_.cwiseMin( F_max_);
    F_star_ = F_star_.cwiseMax(-F_max_);

    // scale by mass matrix
    F_ = M_diag_.cwiseProduct(F_star_);
}

void JointLimitsTask::computeModel()
{
    M_diag_ = controller_->M_.diagonal();

    //Null space updated every control cycle because highly dependent on position
}

void JointLimitsTask::printInfo()
{
    std::cout << "dist lo null  : " << dist_lower_null_.transpose()    << "\n";
    std::cout << "dist lo repel : " << dist_lower_repel_.transpose()   << "\n";
    std::cout << "dist up null  : " << dist_upper_null_.transpose()    << "\n";
    std::cout << "dist up repel : " << dist_upper_repel_.transpose()   << "\n";

    std::cout << "F_lower_      : " << F_lower_.transpose() << "\n";
    std::cout << "F_upper_      : " << F_upper_.transpose() << "\n";
    std::cout << "F_            : " << F_.transpose()       << "\n";
}



}
} /* namespace taco */
