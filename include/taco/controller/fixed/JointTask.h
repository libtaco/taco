/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_JOINTTASK_H_
#define TACO_CONTROLLER_FIXED_JOINTTASK_H_

#include "GoalTask.h"

namespace taco {
namespace fixed {

/** \brief Control a single joint
 *
 * \ingroup fixed
 */
class JointTask: public GoalTask<double, double>
{
public:

    JointTask();
    virtual ~JointTask();

    virtual void initialize();
    virtual void computeTorque();
    virtual void computeModel();

    // --------------------------------
    // Task-specific variables
    // --------------------------------
    /** \brief set joint (at base of link) to control */
    bool setJoint(const std::string& joint_name);

    /** \brief set the joint (at base of link) to control */
    void setJoint(const int joint_id);

    virtual const double& position(bool force_update = false);

    virtual const double& velocity(bool force_update = false);

    virtual const double& acceleration(bool force_update = false);

    /// \brief Position gain
    double Kp_ = 100;

    /// \brief Velocity gain
    double Kv_ =  20;

    /// \brief Set the maximum force applied to the operational point, in the operational space.
    /// If the computed operational space force exceeds this value, it will be normalized to this max.
    double max_force_ = 1000;

private:

    /// \brief The index of the joint, i.e. the generalized coordinate index
    int joint_id_ = -1;

    /// \brief The joint name
    std::string joint_name_;

};

}
} /* namespace taco */
#endif /* JointTask_H_ */
