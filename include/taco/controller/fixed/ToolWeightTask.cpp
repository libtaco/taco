/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "ToolWeightTask.h"

#include <iostream>

namespace taco {
namespace fixed {


ToolWeightTask::ToolWeightTask() : Task("tool_weight")
{
    pos_.setZero();
    vel_.setZero();
    acc_.setZero();
    applied_force_.setZero(3);
}

ToolWeightTask::~ToolWeightTask(){}


void ToolWeightTask::setTool(const double mass,
                             const std::string& link_name,
                             const Eigen::Vector3d& position_in_link)
{
    mass_ = mass;
    link_name_ = link_name;
    pos_in_link_ = position_in_link;
    if (dynamics_ && dynamics_->linkID(link_name_)<0){
        throwException("setLink. link ["+link_name_+"] is not a valid link");
    }
}


void ToolWeightTask::initialize()
{
    if (!link_name_.empty() && dynamics_->linkID(link_name_)<0){
        throwException("setLink. link ["+link_name_+"] is not a valid link");
    }

    int dof = dynamics_->DOF();;
    N_.setIdentity(dof,dof);
}


void ToolWeightTask::computeTorque()
{
    dynamics_->transform(T_link_, link_name_);
    pos_ = T_link_ * pos_in_link_;
    vel_ = J_*dynamics_->dq_;
    //dynamics_->acceleration(acc_, link_name_, pos_in_link_);

    applied_force_ = mass_*Eigen::Vector3d(0,0,9.81);

    // compute joint torques
    F_ = J_.transpose()*applied_force_;

    // check if enabled
    if (!enabled_){ F_.setZero(); }
}

void ToolWeightTask::computeModel()
{
    dynamics_->Jv(J_, link_name_, pos_in_link_);

    // null space always identity, set in initialize()
}

void ToolWeightTask::printInfo()
{
    std::cout << "pos    : " << pos_.transpose() << "\n";
    std::cout << "vel    : " << vel_.transpose() << "\n";
    std::cout << "force  : " << applied_force_.transpose() << "\n";
    std::cout << "torque : " << F_.transpose() << "\n";
}

// --------------------------------
// Task-specific functions
// --------------------------------

const Eigen::Vector3d& ToolWeightTask::position(bool force_update)
{
    if (force_update){
        dynamics_->position(pos_,link_name_,pos_in_link_);
    }
    return pos_;
}

const Eigen::Vector3d& ToolWeightTask::velocity(bool force_update)
{
    if (force_update){
        dynamics_->velocity(vel_,link_name_,pos_in_link_);
    }
    return vel_;
}

const Eigen::Vector3d& ToolWeightTask::acceleration(bool force_update)
{
    if (force_update){
        dynamics_->acceleration(acc_, link_name_, pos_in_link_);
    }
    return acc_;
}


}
} /* namespace taco */
