/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include <iostream>
#include <stdexcept>

#include "TaskController.h"
#include "Task.h"

namespace taco {
namespace fixed {


TaskController::TaskController(){}


TaskController::~TaskController(){}


const Eigen::VectorXd& TaskController::computeTorque()
{
    if (enabled_)
    {
        if (update_count_ % model_update_period_ == 0) {
            computeRobotModel();
            computeTaskModels();
        }
        computeTaskTorques();

        update_count_++;
    }

    return F_;
}

void TaskController::setEnabled(bool enabled)
{
    int dof = dynamics_->DOF();
    M_.setZero(dof,dof);
    M_inv_.setZero(dof,dof);
    gravity_.setZero(dof);
    coriolis_.setZero(dof);
    F_.setZero(dof);

    update_count_ = 0;
    enabled_ = enabled;
}

void TaskController::addTask( Task* task , unsigned int priority)
{
    if (dynamics_==NULL){
        throw std::runtime_error("TaskController::addTask. Add dynamics before adding any tasks.");
    }
    if (task==NULL){
        throwException("addTask(). task is NULL.");
    }

    // set priority in task object
    task->priority_ = priority;

    // link task object
    task->controller_ = this;
    task->dynamics_ = dynamics_;

    // check that the vector large enough size
    if (tasks_.size() <= priority) {
        tasks_.resize( priority+1, NULL );
    }

    // check that priority not already taken
    if (tasks_[priority] != NULL) {
        std::cout << "WARNING. Controller::addTasks. "
                  << "Task of priority [" << priority << "] already exists. "
                  << "Replacing task [" << tasks_[priority]->name_ << "] "
                  << "with task [" << task->name_ << "]." << std::endl;
    }

    // insert task in map-vector and lookup
    tasks_[priority] = task;
    if (!task->name_.empty()){
        task_lookup_[task->name_] = task;
    }

    // initialize task
    task->initialize();
}


void TaskController::printInfo()
{
    std::cout << "=====CONTROLLER FORCE=====" << "\n";
    std::cout << "torque   :\n" << F_.transpose() << "\n";
    std::cout << "gravity  :\n" << gravity_.transpose() << "\n";
    std::cout << "coriolis :\n" << coriolis_.transpose() << "\n";

    for (Task* task : tasks_){
        std::cout << "=====TASK [" << task->name_ << "]=====\n";
        task->printInfo();
    }
}

//=========================================================================
//Private Helper Functions
//=========================================================================

void TaskController::computeRobotModel()
{
    dynamics_->updateDynamics();
    M_.setZero(); //TODO - this should be uncessary after the first time
    dynamics_->massMatrix(M_);

    M_inv_.setZero();
    M_inv_ = M_.inverse();
}


void TaskController::computeTaskModels()
{
    int dof = dynamics_->DOF();
    int num_tasks = tasks_.size();
    if (N_.size() != num_tasks+1){
        N_.resize(num_tasks+1,
                  Eigen::MatrixXd::Identity(dof,dof));
    }

    for (int i=0; i<num_tasks; ++i){
        Task* task = tasks_[i];

        if (task==NULL){
            // copy N over if missing task
            N_[i+1] = N_[i];
        }
        else {
            // update task and null space
            task->computeModel();
            N_[i+1] = N_[i]*task->N_;
        }
    }
}


void TaskController::computeTaskTorques()
{
    // calculate ahead of time, so tasks can use for saturation
    if (gravity_compensation_) {
        dynamics_->gravityForce(gravity_);
    }
    if (coriolis_compensation_) {
        dynamics_->coriolisForce(coriolis_);
    }

    F_.setZero();

    // compute TaskController torque
    for(Task* task : tasks_)
    {
        if (task != NULL) {
            task->computeTorque();
            F_ += task->F_;
        } else {
            std::cout << "EMPTY TASK" << std::endl;
        }
    }

    // add gravity compensation
    if (gravity_compensation_) {
        F_ += gravity_;
    }

    // add coriliolis compensation
    if (coriolis_compensation_) {
        F_ += coriolis_;
    }

    // joint force limits
    if (force_saturation_){
        F_ = F_.cwiseMax(F_min_);
        F_ = F_.cwiseMin(F_max_);
    }
}


} /* namespace */
}
