/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_JOINTLIMITSTASK_H_
#define TACO_CONTROLLER_FIXED_JOINTLIMITSTASK_H_

#include "Task.h"


namespace taco {
namespace fixed {

/** \brief Joints limit task, using a repulsive force and null space to remove joint from controllability.
 *
 * For the lower bound, the following algorithm is used. The upper bound is similarly computed.
 *
 *     limit|<--repulsive-->|<--nullspace-->|<--good--
 *
 *     dist_null  = ( (limit+repel_buffer+null_buffer) - q ) / null_buffer
 *     dist_repel = ( (limit+repel_buffer)             - q ) / repel_buffer
 *
 *     if (dist_null>0) and (torque < 0):
 *         if (dist_repel > 0):
 *            nullspace = 0.001
 *         else:
 *            nullspace = 1 - dist_null
 *     if (dist_repulsive > 0):
 *         torque = Kp*(dist_repel^exponent) - Kv*velocity
 *
 * \ingroup fixed
 */
class JointLimitsTask: public Task {
public:
    JointLimitsTask();
    virtual ~JointLimitsTask();

    virtual void initialize();
    virtual void computeTorque();
    virtual void computeModel();
    virtual void printInfo();

    /** set joint limit position, default is positive numerical limit of double */
    Eigen::VectorXd limit_upper_; //  infinity

    /** set joint limit position, default is negative numerical limit of double */
    Eigen::VectorXd limit_lower_; // -infinity

    /** repulsive buffer size. default is 0.131 (7.5 degrees or 0.131 meters) */
    Eigen::VectorXd repel_buffer_; // 0.131 (rad or m)

    /** null space buffer size. default is 0.131 (7.5 degrees or 0.131 meters) */
    Eigen::VectorXd null_buffer_;  // 0.131 (rad or m)

    /** set proportional gain. default is 100*/
    Eigen::VectorXd Kp_;        // 100

    /** set derivative gain. default is 40*/
    Eigen::VectorXd Kd_;        //  20

    /** set exponent of the repuslive force. default is 2*/
    Eigen::VectorXd exponent_;  //   2

    /** maximum repulsive force. default is 100*/
    Eigen::VectorXd F_max_;     // 100

private:

    Eigen::VectorXd F_upper_;
    Eigen::VectorXd F_lower_;
    Eigen::VectorXd F_star_;

    // normalized distances
    Eigen::VectorXd dist_upper_null_;
    Eigen::VectorXd dist_lower_null_;
    Eigen::VectorXd dist_upper_repel_;
    Eigen::VectorXd dist_lower_repel_;

    // mass matrix diagonal terms
    Eigen::VectorXd M_diag_;

    // the null space will scale between epsilon and 1.
    double epsilon_ = 0.001;
};

}
} /* namespace taco */
#endif /* JointLimitsTask_H_ */
