/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_COMTASK_H_
#define TACO_CONTROLLER_FIXED_COMTASK_H_

#include "GoalTask.h"

#include <string>


namespace taco {
namespace fixed {

/** \brief Center of mass task.
 *
 * Typically used to control center of mass position for balancing.
 *
 * \note UNTESTED. PLEASE TEST BEFORE USING.
 *
 * \ingroup fixed
 */
class CoMTask: public GoalTask<Eigen::Vector3d, Eigen::Vector3d> {
public:
    CoMTask();
    virtual ~CoMTask();

    virtual void computeTorque();
    virtual void computeModel();
    virtual void printInfo();

    // --------------------------------
    // Task-specific functions
    // --------------------------------

    const Eigen::Vector3d& position(bool force_update = false);

    const Eigen::Vector3d& velocity(bool force_update = false);

    const Eigen::Vector3d& acceleration(bool force_update = false);

    /// \brief Position gain
    double Kp_ = 100;

    /// \brief Velocity gain
    double Kv_ =  20;

    /// \brief Set the maximum force applied to the operational point, in the operational space.
    /// If the computed operational space force exceeds this value, it will be normalized to this max.
    double max_force_ = 1000;

};

}
} /* namespace taco */
#endif /*   _H_ */
