/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_FIXEDCONTROLLER_H_
#define TACO_CONTROLLER_FIXED_FIXEDCONTROLLER_H_

/**
@defgroup fixed Operational space control with fixed base
\ingroup controller

\namespace fixed
\brief Covers all classes within the operation space controller with fixed base.
*/

#include <taco/controller/fixed/ArmController.h>
#include <taco/controller/fixed/TaskController.h>
#include <taco/controller/fixed/Task.h>
#include <taco/controller/fixed/JointTask.h>
#include <taco/controller/fixed/JointsTask.h>
#include <taco/controller/fixed/PositionTask.h>
#include <taco/controller/fixed/OrientationTask.h>
#include <taco/controller/fixed/CoMTask.h>
#include <taco/controller/fixed/JointLimitsTask.h>
#include <taco/controller/fixed/RelativePositionTask.h>

#endif
