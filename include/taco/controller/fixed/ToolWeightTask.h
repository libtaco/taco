/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_TOOLWEIGHTTASK_H_
#define TACO_CONTROLLER_FIXED_TOOLWEIGHTTASK_H_

#include "Task.h"

#include <string>
#include <fstream>
#include <iostream>
#include <limits>


namespace taco {
namespace fixed {


/** \brief Compensates for the weight of a tool. Uses up no DOF, and priority has no effect.
 *
 * \ingroup fixed
 */
class ToolWeightTask : public Task {
public:
    ToolWeightTask();
    virtual ~ToolWeightTask();

    virtual void initialize();
    virtual void computeTorque();
    virtual void computeModel();
    virtual void printInfo();

    // --------------------------------
    // Task-specific functions
    // --------------------------------

    /** \brief Set the tool center of mass position.
     * \param link_name The name of the link that the tool is rigidly attached to
     * \param position_in_link The position of the tool center of mass in that link
     */
    void setTool(const double mass,
                 const std::string& link_name,
                 const Eigen::Vector3d& position_in_link = Eigen::Vector3d::Zero() );

    virtual const Eigen::Vector3d& position(bool force_update = false);

    virtual const Eigen::Vector3d& velocity(bool force_update = false);

    virtual const Eigen::Vector3d& acceleration(bool force_update = false);

    /// \brief The mass of the tool
    double mass_;

    /// \brief The name of the link that the tool is rigitdly attached to
    std::string link_name_;

    /// \brief The position of the tool center of mass in the link
    Eigen::Vector3d pos_in_link_ = Eigen::Vector3d::Zero();

private:

    /// \brief Tranformation matrix of the link
    Eigen::Affine3d T_link_;

    /// \brief position of the operational frame
    Eigen::Vector3d pos_;

    /// \brief velocity of the operational frame
    Eigen::Vector3d vel_;

    /// \brief acceleration of the operational frame
    Eigen::Vector3d acc_;

};

}
} /* namespace taco */
#endif /* ToolWeightTask_H_ */
