/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_ELBOWTASK_H_
#define TACO_CONTROLLER_FIXED_ELBOWTASK_H_

#include "GoalTask.h"

#include <string>
#include <fstream>
#include <iostream>
#include <limits>


namespace taco {
namespace fixed {


/** \brief Control the position of elbow relative to the base.
 *
 * Elbow will be pulled up to increase workspace
 * If elbow penetrates ceiling, it will be pushed down.
 * If ceiling is infinity (default), then task uses 1 DOF. (i.e. size(J) is 1xN)
 * If ceiling is not infinity then task uses 2 DOF. (i.e. size(J) is 2xN.)
 *
 * If the robot reaches the
 *
 * \ingroup fixed
 */
class ElbowTask: public Task {
public:
    ElbowTask();
    virtual ~ElbowTask();

    virtual void initialize();
    virtual void computeTorque();
    virtual void computeModel();
    virtual void printInfo();

    // --------------------------------
    // Task-specific functions
    // --------------------------------

    /** \brief Set operational point of end-effector.
     * \param link_name The name of the link that the operational point is defined
     * \param position_in_link The position of operational point in that link
     */
    void setEndEffector(const std::string& link_name,
                        const Eigen::Vector3d& position_in_link = Eigen::Vector3d::Zero() );

    /** \brief Set operational point of elbow.
     * \param link_name The name of the link that the operational point is defined
     * \param position_in_link The position of operational point in that link
     */
    void setElbow(const std::string& link_name,
                  const Eigen::Vector3d& position_in_link = Eigen::Vector3d::Zero() );

    /// \brief The name of the link that the end effector operational point is defined
    std::string link_name_ee_;

    /// \brief The position of end effector operational point in the link
    Eigen::Vector3d pos_in_link_ee_ = Eigen::Vector3d::Zero();

    /// \brief The name of the link that the elbow operational point is defined
    std::string link_name_elbow_;

    /// \brief The position of elbow operational point in the link
    Eigen::Vector3d pos_in_link_elbow_ = Eigen::Vector3d::Zero();

    /// \brief Set the maximum force applied to the operational point, in the operational space.
    /// If the computed operational space force exceeds this value, it will be normalized to this max.
    double max_force_ = 1000;

    /// \brief ceiling that the elbow should stay below.
    double ceiling_ = std::numeric_limits<double>::infinity();

    /// \brief if the end effector will fold toward the vertical plane along this angle
    double midplane_angle_ = 0;

    /// \brief normalized force on the elbow, i.e. acceleration
    double applied_acc_ = 1.0;

    /// \brief normalized proportional gain for pushing down elbow
    double repulsive_gain_ = 12.0;

    /// \brief normalized damping gain
    double damping_ =  2*sqrt(repulsive_gain_);

private:

    /// \brief Tranformation matrix of the end-effector link
    Eigen::Affine3d T_link_ee_;

    /// \brief Tranformation matrix of the elbow link
    Eigen::Affine3d T_link_elbow_;

    /// \brief Position of the end-effector link
    Eigen::Vector3d pos_ee_;

    /// \brief Position of the elbow link
    Eigen::Vector3d pos_elbow_;

    /// \brief Velocity of the elbow link
    Eigen::Vector3d vel_elbow_;

    /// \brief Full Jacobian of the elbow, rotated about z, so x points toward the end-effector.
    Eigen::MatrixXd J_elbow_;

    /// \brief Current angle of end effector relative to the base in cylindrical coordinates.
    double ee_angle_ = 0;

    /// \brief Distance penetrated into the ceiling
    double dist_penetrated_ = 0;

    /// \brief Direction to lean. Perspective is from the base, looking towared the end-effector
    enum Lean {
        LEAN_NONE = 0,
        LEAN_RIGHT,
        LEAN_LEFT
    };

    /// \brief Current lean of the elbow, relative to the vertical plane between robot base and end-effector
    Lean lean_ = LEAN_NONE;



};

}
} /* namespace taco */
#endif /* ElbowTask_H_ */
