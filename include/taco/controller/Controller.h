/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_CONTROLLER_H_
#define TACO_CONTROLLER_CONTROLLER_H_

#include <taco/dynamics/Dynamics.h>

#include <Eigen/Core>

#include <limits>
#include <string>
#include <map>
#include <vector>
#include <set>

namespace taco {

/** \brief Base class of robot controller. Computes joint torques using dynamics engine
 *
 * \ingroup controller
 */
class Controller {

public:

    Controller(){}

    virtual ~Controller(){}

    /** \brief Set the dynamics engine of the controller.
     *
     * Dynamics engine contains kinematic model.
     * Initialize controller and allocate memory that requires kinematic knowledge like DOF
     * such as F_, F_min_, F_max_.
     *
     * \param dynamics  The dynamic engine.
     */
    virtual void setDynamics(Dynamics* dynamics)
    {
        if (dynamics->DOF()<=0){
            printWarning("setDynamics(). Dynamic model has 0 DOF. Initialize dynamics before calling setDynamics.");
        }

        dynamics_ = dynamics;

        // resize Force and limits
        F_.setZero(dynamics_->DOF());
        if (std::numeric_limits<float>::is_iec559){
            if (F_min_.size()==0){
                F_min_.setConstant(dynamics_->DOF(), -std::numeric_limits<double>::infinity());
            }
            if (F_max_.size()==0){
                F_max_.setConstant(dynamics_->DOF(), std::numeric_limits<double>::infinity());
            }
        } else {
            printWarning("Cannot set joint limits to infinity.");
        }
    }

    /** @brief The dynamics engine of the controller.
     * \returns The dynamics engine of the controller. */
    Dynamics* dynamics(){ return dynamics_; }

    /** \brief Computes the total joint torques.
     * Updates F_. Should handle enabling/disabling controller, force limits, gravity, coriolis.
     * \returns The computed joint torques. */
    virtual const Eigen::VectorXd& computeTorque()
    {
        if (enabled_){
            F_.setZero();
            if (gravity_compensation_ && dynamics_!=NULL){
                Eigen::VectorXd g;
                dynamics_->gravityForce(g);
                F_ += g;
            }
            if (coriolis_compensation_ && dynamics_ !=NULL){
                Eigen::VectorXd c;
                dynamics_->coriolisForce(c);
                F_ += c;
            }
            if (force_saturation_){
                F_ = F_.cwiseMax(F_min_);
                F_ = F_.cwiseMin(F_max_);
            }
        } else {
            F_.setZero();
        }
        return F_;
    }

    /** \brief Set the controller's status to enabled or diabled.
     * Performs any initialization or destruction required when enabling or disabling the controller.
     * \param enable the controller. */
    virtual void setEnabled(bool enable){
        enabled_=enable;
    }

    /** \brief The status of the controller.
     * \returns True if the controller is enabled, false if disabled. */
    virtual bool enabled(){
        return enabled_;
    }

    /** \brief Set the controller's gravity compensation to enabled or diabled.
     * \param enable gravity compensation. */
    virtual void setGravityCompensation(bool enable){
        gravity_compensation_=enable;
    }

    /** \brief Wheher gravity compensation is active.
     * \returns True if the gravity compensation is enabled, false if disabled. */
    virtual bool gravityCompensation(){
        return gravity_compensation_;
    }

    /** \brief Set the controller's coriolis (and centripetal) compensation to enabled or diabled.
     * \param enable coriolis compensation. */
    virtual void setCoriolisCompensation(bool enable){
        coriolis_compensation_=enable;
    }

    /** \brief Whether coriolis (and centripetal) compensation is active.
     * \returns True if the coriolis compensation is enabled, false if disabled. */
    virtual bool coriolisCompensation(){
        return coriolis_compensation_;
    }

    /** \brief Set the controller's force saturation checks to enabled or diabled.
     * \param enable force saturation checks */
    virtual void setForceSaturation(bool enable){
        force_saturation_=enable;
    }

    /** \brief Whether force saturation checks are active.
     * \returns True if force saturation is enabled, false if disabled. */
    virtual bool forceSaturation(){
        return force_saturation_;
    }

public:

    /// \brief Force compute by computeTorque(). Public for convenience, but read only.
    Eigen::VectorXd F_;

    /// \brief Minimum force for joints. F_min_ < F_ < F_max_.
    Eigen::VectorXd F_min_;

    /// \brief Maximum force for joints. F_min_ < F_ < F_max_.
    Eigen::VectorXd F_max_;

    /// \brief Dynamics for calculating kinematics.
    Dynamics* dynamics_ = NULL;

protected:

    /// \brief Controller status.
    bool enabled_ = false;

    /// \brief Gravity compensation enabled or disabled
    bool gravity_compensation_ = true;

    /// \brief Coriolis and centrifugal compensation enabled or disabled
    bool coriolis_compensation_ = false;

    /// \brief Force limits enabled or disabed
    bool force_saturation_ = false;

    /** \brief Print a warning to the terminal.
     * \param message The message that you wanted printed.
     */
    virtual void printWarning(const std::string& message) {
        std::cout << "WARNING. Controller. " << message << std::endl;
    }

    /** \brief Throw and exception and print error to the terminal.
     * \param message The message that you wanted printed.
     */
    virtual void throwException(const std::string& message) {
        throw std::runtime_error( std::string("ERROR. Controller. ") + message );
    }


};


} /* namespace taco */
#endif /* CONTROLLER_H_ */
