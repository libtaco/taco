/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_CONSTRAINT_MOTIONCONSTRAINT_H_
#define TACO_CONTROLLER_CONSTRAINT_MOTIONCONSTRAINT_H_

#include <taco/controller/constraint/Constraint.h>
#include <taco/dynamics/Dynamics.h>

#include <Eigen/Core>


namespace taco {


/** \brief A motion constraint, linear and angular velocity is zero in 6 DOF
 *
 * \ingroup constraint
 */
class MotionConstraint : public Constraint {

public:

    MotionConstraint();

    virtual ~MotionConstraint();

    /** \brief An equality constraint defined by ddx = J ddq
     * \param J The jacobian of the constraint
     * \param ddx The acceleration of the constraint
     */
    virtual void jacobian(Eigen::MatrixXd& J, Eigen::VectorXd& ddx)
    {
        dynamics_->Jv(Jv_, link_name_, pos_in_link_);
        dynamics_->Jw(Jw_, link_name_, pos_in_link_);

        // stack the jacobians
        J.resize(6,dynamics_->DOF());
        J.middleRows(0,3) = Jv_;
        J.middleRows(3,3) = Jw_;

        // A velocity constraint is given by:
        //     J dq         = dx  = 0
        // => dJ dq + J ddq = ddx = 0
        // =>         J ddq = -dJ dq
        //
        // To calculate dJ dq, we use a trick:
        // =>         dJ dq = ddx when ddq==0
        zero_.setZero(dynamics_->DOF());
        dynamics_->acceleration(ddx, link_name_, pos_in_link_,
                                dynamics_->q_, dynamics_->dq_,zero_);
        ddx = -ddx;
    }

    /** \brief Set operational point.
     * \param link_name The name of the link that the operational point is defined
     * \param position_in_link The position of operational point in that link
     */
    void setOperationalPoint(const std::string& link_name,
                             const Eigen::Vector3d& position_in_link = Eigen::Vector3d::Zero() )
    {
        link_name_ = link_name;
        pos_in_link_ = position_in_link;
        if (dynamics_ && dynamics_->linkID(link_name_)<0){
            throwException("Link ["+link_name_+"] is not a valid link");
        }
    }


protected:

    /// \brief The name of the link that the operational point is defined
    std::string link_name_;

    /// \brief The position of operational point in the link
    Eigen::Vector3d pos_in_link_ = Eigen::Vector3d::Zero();

    /// \brief Linear velocity jacobian of this constraint
    Eigen::MatrixXd Jv_;

    /// \brief Angular velocity jacobian of this constraint
    Eigen::MatrixXd Jw_;

    /// \brief Zero vector
    Eigen::VectorXd zero_;
};

} /* namespace taco */
#endif
