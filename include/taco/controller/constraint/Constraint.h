/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_CONSTRAINT_CONSTRAINT_H_
#define TACO_CONTROLLER_CONSTRAINT_CONSTRAINT_H_

#include <taco/dynamics/Dynamics.h>

#include <Eigen/Core>

#include <string>

namespace taco {


/** \brief A constraint, defined by the acceleration of an operational point
 *
 * \ingroup constraint
 */
class Constraint {

public:

    Constraint();

    Constraint(const std::string& name){name_ = name;}

    virtual ~Constraint();

    /** \brief Set the dynamics engine */
    virtual void setDynamics(Dynamics* dynamics)
    {
        dynamics_ = dynamics;
    }

    /** \brief An equality constraint defined by ddx = J ddq
     * \param J The jacobian of the constraint
     * \param ddx The acceleration of the constraint
     */
    virtual void jacobian(Eigen::MatrixXd& J, Eigen::VectorXd& ddx) = 0;

protected:

    /// \brief Pointer to the dynamics, set by TaskController:addTask()
    Dynamics* dynamics_ = NULL;

    //// \brief name of the task
    std::string name_;

    /// \brief Wether the task is enabled
    bool enabled_ = true;

};

} /* namespace taco */
#endif
