/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_QP_TASK_H_
#define TACO_CONTROLLER_QP_TASK_H_

#include <taco/dynamics/Dynamics.h>
#include <taco/controller/math.h>
#include <taco/controller/qp/TaskController.h>

#include <eigen3/Eigen/Dense>

#include <string>
#include <iostream>
#include <memory>
#include <stdexcept>

namespace taco {
namespace qp {

/** \brief Base class for a qp controller task
 *
 * Computes joint torques in the null space of higher priority tasks.
 * Computes null space matrix that low priority must operate within.
 *
 * Objective = ||J*(ddq_higher_priority + N_higher_priority*ddq) + dJ*dq - ddx_desired|| + regularization_weight_*ddq_T*M*ddq
 *
 * \ingroup qp
 */
class Task
{
    friend class TaskController;

public:

    /** \brief Constructor. */
    Task() {}

    /** \brief Base class for a qp controller task
     *
     * \param name  Name of the task.
     */
    Task(const std::string& name){name_ = name;}

    /** \brief Destructor. */
    virtual ~Task() {}

    /** \brief Enable or disable the task. Enabled by default. */
    virtual void setEnabled(bool enable)
    {
        enabled_ = enable;
    }

    /** \brief The computed acceleration applied to the operational point, in the operational space.
     * \return The computed acceleration applied to of the operational point
     */
    virtual const Eigen::VectorXd& appliedAcceleration(){ return applied_acc_; }

    /** \brief Print info about the task state to the terminal. */
    virtual void printInfo() {}

    /// \brief Set the maximum operational space mass to use in the singular direction,
    /// when computing operational force = operational space mass * commanded acceleration.
    /// \note This does not affect the operational space mass used to compute the null space.
    /// max_singular_mass_ <= 1/epsilon;
    double max_singular_mass_ = 20;

    /** \brief Enable or disable the task. Enabled by default. */
    virtual void setName(const std::string& name)
    {
        name_ = name;
        controller_->task_lookup_[name_] = this;
    }

protected:

    /** \brief Initialize the task, called after the task is added to the controller.
     *
     * Adding the task to TaskController via TaskController::addTask()
     * will set controller_ and dynamics_ for this class, followed by calling this initialize().
     * Initialize task and allocate memory that requires dynamic or controller knowledge.
     */
    virtual void initialize()
    {
        int dof = dynamics_->DOF();
        ddq_.setZero(dof);
        N_.setIdentity(dof,dof);
        J_.setZero(0,dof);
    }

    /** \brief Compute joint accelerations, i.e. compute this->ddq_
     */
    virtual void computeAcceleration()
    {
        //applied_acc_ = ???
        //if (!enabled_){applied_acc_.setZero();}
    }

    /** \brief Compute null space, i.e. compute this->N_
     *
     * Compute null space and other quantities that are computationally expensive
     * and are relatively insensitive to small changes in joint values.
     * May be called less frequently then computeTorque(), depending on TaskController::model_update_period_.
     */
    virtual void computeModel()
    {
        //J_ = ???
        //computeNullSpace();
    }

    /** \brief Compute null space from the Jacobian J_,
     * i.e. compute this->N_ */
    virtual void computeNullSpace()
    {
        int dof = dynamics_->DOF();

        // jacobian projected into the null space of the previous tasks
        JN_ = J_ * controller_->N_[priority_];
        JN_T_ = JN_.transpose();
        NJJN_ = JN_T_*JN_;

        // inverse of the operational space mass matrix
        lambda_inv_ = JN_ * controller_->M_inv_ * JN_T_;
        lambda_ = pseudoInv(JN_ * controller_->M_inv_ * JN_T_, epsilon_);

        //dynamically consistent generalized inverse
        JN_inv_ = controller_->M_inv_ * JN_T_ * lambda_;

        // pseudo inverse null space
        N_ = Eigen::MatrixXd::Identity(dof,dof) - JN_inv_ * JN_;

        //Null space is unchanged if task is disabled
        if (!enabled_){
            N_ = controller_->N_[priority_];
        }
    }

    virtual void printWarning(const std::string& message) {
        std::cout << "WARNING. Task [" << name_ << "]. " << message << std::endl;
    }

    virtual void throwException(const std::string& message) {
        throw std::runtime_error("Task [" + name_ + "]. " + message);
    }

    //// \brief name of the task
    std::string name_;

    /// \brief Wether the task is enabled
    bool enabled_ = true;

    // INITIALIZED BY CONTROLLER

    /// \brief Priority of the task, set by TaskController:addTask()
    unsigned int priority_ = 0;

    /// \brief Pointer to the controller, set by TaskController:addTask()
    TaskController* controller_ = NULL;

    /// \brief Pointer to the dynamics, set by TaskController:addTask()
    Dynamics* dynamics_ = NULL;

    // COMPUTED BY USER

    /// \brief calculated acceleration to apply to the operational frame
    Eigen::VectorXd applied_acc_;

    /// \brief Joint acceleration for this task, computed by computeAcceleration()
    Eigen::VectorXd ddq_;

    /// \brief Jacobian of this task, computed by computeModel()
    Eigen::MatrixXd J_;

    // COMPUTED BY computeNullSpace()

    /// \brief Jacobian of the this task, projected to the null space of higher priority tasks
    Eigen::MatrixXd JN_;

    /// \brief JN_ tranposed
    Eigen::MatrixXd JN_T_;

    /// \brief JN_^T * JN_
    Eigen::MatrixXd NJJN_;

    /// \brief Dynamically consistent inverse of JN_
    Eigen::MatrixXd JN_inv_;

    /// \brief Inverse of the maximum singular value of lambda_, used to compute N_.
    double epsilon_ = 0.001;

    /// \brief Operational space mass matrix
    Eigen::MatrixXd lambda_;

    /// \brief Inverse of the operational space mass matrix, lambda_
    Eigen::MatrixXd lambda_inv_;

    /// \brief Null space of this task, computed by computeNullSpace()
    Eigen::MatrixXd N_;

};

}
} /* namespace taco */
#endif /* TASK_H_ */
