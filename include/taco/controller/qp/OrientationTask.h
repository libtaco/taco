/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_QP_ORIENTATIONTASK_H_
#define TACO_CONTROLLER_QP_ORIENTATIONTASK_H_

#include "GoalTask.h"

#include <string>


namespace taco {
namespace qp {

/** \brief Control the orientation of link relative to the ground frame.
 *
 * \ingroup qp
 */
class OrientationTask: public GoalTask<Eigen::Matrix3d, Eigen::Vector3d> {
public:
    OrientationTask();
    virtual ~OrientationTask();

    virtual void initialize();
    virtual void computeAcceleration();
    virtual void computeModel();
    virtual void printInfo();

    // --------------------------------
    // Task-specific functions
    // --------------------------------

    /** \brief Set operational frame.
     * \param link_name The name of the link that the operational frame is defined
     */
    void setOperationalBody(const std::string& link_name );

    /** \brief The orientation of the operational frame
     * \param force_update  If false, then return last value computed during computeTorque().
     *                      If true, compute the value now from current joint positions.
     * \return The orientation of the operational frame
     */
    const Eigen::Matrix3d& position(bool force_update = false);

    const Eigen::Vector3d& velocity(bool force_update = false);

    const Eigen::Vector3d& acceleration(bool force_update = false);


    /// \brief The name of the link that the operational point is defined
    std::string link_name_;

    /// \brief Position gain
    double Kp_ = 100;

    /// \brief Velocity gain
    double Kv_ =  20;

    /// \brief Set the maximum force applied to the operational point, in the operational space.
    /// If the computed operational space force exceeds this value, it will be normalized to this max.
    double max_acceleration_ = 1000;


private:

    /// \brief Tranformation of the link
    Eigen::Affine3d T_link_; // tranformation of the link


};

}
} /* namespace taco */
#endif /* OrientationTask_H_ */
