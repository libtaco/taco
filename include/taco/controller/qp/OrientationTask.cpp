/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "OrientationTask.h"

#include <iostream>

namespace taco {
namespace qp {


OrientationTask::OrientationTask() : GoalTask("orientation")
{
    desired_pos_.setIdentity();
    desired_vel_.setZero();
    desired_acc_.setZero();
    desired_force_.setZero();
    pos_.setIdentity();
    vel_.setZero();
    acc_.setZero();
    applied_acc_.setZero(3);
}

OrientationTask::~OrientationTask() {}


void OrientationTask::setOperationalBody(const std::string& link_name)
{
    link_name_ = link_name;
    if (dynamics_ && dynamics_->linkID(link_name_)<0){
        throwException("Link ["+link_name_+"] is not a valid link");
    }
}

void OrientationTask::initialize()
{
    if (!link_name_.empty() && dynamics_->linkID(link_name_)<0){
        throwException("Link ["+link_name_+"] is not a valid link");
    }
}


void OrientationTask::computeAcceleration()
{
    dynamics_->transform(T_link_, link_name_);
    pos_ = T_link_.rotation();
    vel_ = J_*dynamics_->dq_;
    //dynamics_->angularAcceleration(acc_, link_name_);

    switch (control_type_) {
        case CONTROL_POS:
        {
            Eigen::Matrix3d dRot = desired_pos_*pos_.inverse();
            Eigen::AngleAxisd dAA(dRot);
            applied_acc_ = (Kp_ * dAA.angle() * dAA.axis() + Kv_ * (desired_vel_ - vel_));
            break;
        }
        case CONTROL_VEL:
            applied_acc_ = Kv_ * (desired_vel_ - vel_);
            break;
        case CONTROL_ACC:
            applied_acc_ = desired_acc_;
            break;
        case CONTROL_FORCE:
            applied_acc_ = lambda_inv_*desired_force_;
            break;
    }

    // check if enabled
    if (!enabled_){ applied_acc_.setZero(); }
}

void OrientationTask::computeModel()
{
    dynamics_->Jw(J_, link_name_);
    computeNullSpace();
}

void OrientationTask::printInfo()
{
    std::cout << "goal        :\n" << desired_pos_.transpose() << "\n";
    std::cout << "pos         : " << pos_.transpose() << "\n";
    std::cout << "vel         : " << vel_.transpose() << "\n";
    std::cout << "applied acc : " << applied_acc_.transpose() << "\n";
}


// --------------------------------
// Task-specific functions
// --------------------------------

const Eigen::Matrix3d& OrientationTask::position(bool force_update)
{
    if (force_update){
        dynamics_->transform(T_link_, link_name_);
        pos_ = T_link_.rotation();
    }
    return pos_;
}

const Eigen::Vector3d& OrientationTask::velocity(bool force_update)
{
    if (force_update){
        dynamics_->Jw(J_, link_name_);
        vel_ = J_*dynamics_->dq_;
    }
    return vel_;
}

const Eigen::Vector3d& OrientationTask::acceleration(bool force_update)
{
    if (force_update){
        dynamics_->angularAcceleration(acc_, link_name_);
    }
    return acc_;
}


}
} /* namespace taco */
