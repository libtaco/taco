/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include <iostream>
#include <stdexcept>

#include "TaskController.h"
#include "Task.h"

namespace taco {
namespace qp {


TaskController::TaskController(){}


TaskController::~TaskController(){}

void TaskController::setQPSolver(taco::QP* solver)
{
    solver_ = solver;
}

const Eigen::VectorXd& TaskController::computeTorque()
{
    if (enabled_)
    {
        if (update_count_ % model_update_period_ == 0) {
            computeRobotModel();
            computeTaskModels();
        }
        computeTaskTorques();

        update_count_++;
    }

    return F_;
}

void TaskController::setEnabled(bool enabled)
{
    int dof = dynamics_->DOF();
    M_.setZero(dof,dof);
    M_inv_.setZero(dof,dof);
    gravity_.setZero(dof);
    coriolis_.setZero(dof);
    F_.setZero(dof);
    ddq_.setZero(dof);

    update_count_ = 0;
    enabled_ = enabled;
}

void TaskController::addTask( Task* task , unsigned int priority)
{
    if (dynamics_==NULL){
        throw std::runtime_error("TaskController::addTask. Add dynamics before adding any tasks.");
    }
    if (task==NULL){
        throwException("addTask(). task is NULL.");
    }

    // set priority in task object
    task->priority_ = priority;

    // link task object
    task->controller_ = this;
    task->dynamics_ = dynamics_;

    // check that the vector large enough size
    if (tasks_.size() <= priority) {
        tasks_.resize( priority+1, NULL );
    }

    // check that priority not already taken
    if (tasks_[priority] != NULL) {
        std::cout << "WARNING. Controller::addTasks. "
                  << "Task of priority [" << priority << "] already exists. "
                  << "Replacing task [" << tasks_[priority]->name_ << "] "
                  << "with task [" << task->name_ << "]." << std::endl;
    }

    // insert task in map-vector and lookup
    tasks_[priority] = task;
    if (!task->name_.empty()){
        task_lookup_[task->name_] = task;
    }

    // initialize task
    task->initialize();

    // resize the stacked jacobian and goal
    int goal_dim = 0;
    for (Task* task : tasks_){
        if (task != NULL) {
            if (task->applied_acc_.size()==0){
                throw std::runtime_error("TaskController::addTask. Cannot determine task dimensionality because task.applied_acc_.size()==0");
            }
            goal_dim += task->applied_acc_.size();
        }
    }
    int dof = dynamics_->DOF();
    J_.setZero(goal_dim,dof);
    ddx_.setZero(goal_dim);
}


void TaskController::printInfo()
{
    std::cout << "=====CONTROLLER FORCE=====" << "\n";
    std::cout << "torque   :\n" << F_.transpose() << "\n";
    std::cout << "gravity  :\n" << gravity_.transpose() << "\n";
    std::cout << "coriolis :\n" << coriolis_.transpose() << "\n";

    for (Task* task : tasks_){
        std::cout << "=====TASK [" << task->name_ << "]=====\n";
        task->printInfo();
    }
}

//=========================================================================
//Private Helper Functions
//=========================================================================

void TaskController::computeRobotModel()
{
    dynamics_->updateDynamics();
    M_.setZero(); //TODO - this should be uncessary after the first time
    dynamics_->massMatrix(M_);

    M_inv_.setZero();
    M_inv_ = M_.inverse();
}


void TaskController::computeTaskModels()
{
    int dof = dynamics_->DOF();
    int num_tasks = tasks_.size();

    if (N_.size() != num_tasks+1){
        N_.resize(num_tasks+1, Eigen::MatrixXd::Identity(dof,dof));
    }

    int idim = 0;
    for (int i=0; i<num_tasks; ++i){
        Task* task = tasks_[i];

        if (task==NULL){
            // copy N over if missing task
            N_[i+1] = N_[i];
        }
        else {
            // update task and null space
            task->computeModel();
            N_[i+1] = N_[i]*task->N_;

            // copy the Jacobian and goal acc to the stacked Jacobian and stacked goal
            if (idim >= J_.rows()){
                throw std::runtime_error("TaskController.computeTasksModels() dimension mismatch.");
            }
            int task_dim = task->J_.rows();
            J_.middleRows(idim, task_dim) = task->JN_;
            idim += task_dim;

        }
    }
}


void TaskController::computeTaskTorques()
{
    // calculate ahead of time, so tasks can use for saturation
    if (gravity_compensation_) {
        dynamics_->gravityForce(gravity_);
    }
    if (coriolis_compensation_) {
        dynamics_->coriolisForce(coriolis_);
    }

    ddq_.setZero();

    // compute TaskController torque
    int idim = 0;
    for(Task* task : tasks_)
    {
        if (task != NULL) {
            // compute goal acc (ddx)
            task->computeAcceleration();

            // add to stacked goal acc (ddx)
            if (idim >= ddx_.size()){
                throw std::runtime_error("TaskController.computeTasksModels() dimension mismatch.");
            }
            int task_dim = task->J_.rows();
            ddx_.segment(idim, task_dim) = task->applied_acc_;
            idim += task_dim;
        } else {
            std::cout << "EMPTY TASK" << std::endl;
        }
    }
    // compute QP Problem
    computeRegularization();
    F_ = M_*ddq_;

    //F_.setZero();

    // add gravity compensation
    if (gravity_compensation_) {
        F_ += gravity_;
    }

    // add coriliolis compensation
    if (coriolis_compensation_) {
        F_ += coriolis_;
    }

    // joint force limits
    if (force_saturation_){
        F_ = F_.cwiseMax(F_min_);
        F_ = F_.cwiseMin(F_max_);
    }
}


bool TaskController::computeRegularization()
{
    // cost = ||ddx_desired_ - J ddq_|| + ddq^T M ddq
    //      = ddq^T (J^T*J + M) ddq - 2 ddx_desired^T J ddq

    Eigen::MatrixXd A =  J_.transpose()*J_ +  regularization_weight_*M_;
    Eigen::VectorXd B = -2*ddx_.transpose()*J_;

    solver_->setObjective(A,B);

    if (force_saturation_){
        solver_->setInequalities( M_,  F_max_ - M_*ddq_);
        solver_->setInequalities(-M_, -F_min_ + M_*ddq_);
    }

    cost_ = solver_->solve(ddq_);

    if ( std::isinf(cost_) ){
        printWarning("No solution found.");
        ddq_.setZero();
        return false;
    }
    return true;
}


} /* namespace */
}
