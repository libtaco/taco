#ifndef TACO_CONTROLLER_QP_ARMCONTROLLER_CPP_
#define TACO_CONTROLLER_QP_ARMCONTROLLER_CPP_

#include <taco/dynamics/Dynamics.h>

namespace taco {
namespace qp {


template <class DynamicsT, class QP_T>
ArmController<DynamicsT, QP_T>::ArmController(const std::string& robot_file,
                                              const std::string& end_effector_link_name,
                                              const Eigen::Vector3d& end_effector_pos_in_link,
                                              const std::string& elbow_link_name,
                                              const Eigen::Vector3d& elbow_pos_in_link)
{
    // initialize dynamic engine for controller.
    Dynamics* dynamics = dynamic_cast<Dynamics*>(new DynamicsT());
    dynamics->importRobot(robot_file, true);    // import robot model
    this->setDynamics(dynamics);

    // add prioritized tasks to controller. controller will delete tasks.
    this->addTask(&orientation_task,0);
    this->addTask(&position_task,1);
    this->addTask(&elbow_task,2);
    this->addTask(&joints_task,3);

    // add the qp solver
    this->setQPSolver(new QP_T());

    // end effector position control
    // desired position = current position
    position_task.setOperationalPoint(end_effector_link_name, end_effector_pos_in_link);
    position_task.control_type_ = position_task.CONTROL_POS;
    position_task.desired_pos_ = position_task.position(true);
    position_task.Kp_ = 5.0;
    position_task.Kv_ = 2*sqrt(position_task.Kp_); //critical damping

    // end effector orientation control    
    // desired orientation  = default (identity)
    orientation_task.setOperationalBody(end_effector_link_name);
    orientation_task.control_type_ = orientation_task.CONTROL_POS;
    orientation_task.desired_pos_ = orientation_task.position(true);
    orientation_task.Kp_ = 5.0;
    orientation_task.Kv_ = 2*sqrt(orientation_task.Kp_); //critical damping

    // end effector orientation control
    // desired orientation  = default (identity)
    elbow_task.setOperationalPoint(elbow_link_name, elbow_pos_in_link);
    elbow_task.desired_force_ = 1.0;
    elbow_task.damping_ = 1.0;

    // null space joint damping
    joints_task.Kp_.setZero();
    joints_task.Kv_.setConstant(2*sqrt(5));
}

template <class DynamicsT, class QP_T>
ArmController<DynamicsT, QP_T>::~ArmController()
{
    delete dynamics_;
    delete solver_;
}

}
}

#endif

