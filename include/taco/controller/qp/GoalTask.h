/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_QP_GOALTASK_H_
#define TACO_CONTROLLER_QP_GOALTASK_H_

#include "Task.h"

#include <string>
#include <fstream>
#include <iostream>
#include <limits>


namespace taco {
namespace qp {


/** \brief An API for a task to control a goal point or orientation or frame.
 *
 * \ingroup qp
 */
template<typename POSITION_T, typename VELOCITY_T>
class GoalTask: public Task {
public:
    GoalTask(const std::string& name) : Task(name)
    {
    }

    virtual ~GoalTask(){}

    /** \brief The position of the operational point
     * \param force_update  If false, then return last value computed during computeTorque().
     *                      If true, compute the value now from current joint positions.
     * \return The position of the operational point
     */
    virtual const POSITION_T& position(bool force_update = false) = 0;

    /** \brief The velocity of the operational point
     * \param force_update  If false, then return last value computed during computeTorque().
     *                      If true, compute the value now from current joint positions.
     * \return The velocity of the operational point
     */
    virtual const VELOCITY_T& velocity(bool force_update = false) = 0;

    /** \brief The acceleration of the operational point
     * \param force_update  If false, then return last value computed during computeTorque().
     *                      If true, compute the value now from current joint positions.
     * \return The acceleration of the operational point
     */
    virtual const VELOCITY_T& acceleration(bool force_update = false) = 0;


    virtual void printInfo()
    {
        std::cout << "goal        : " << desired_pos_   << "\n";
        std::cout << "pos         : " << pos_           << "\n";
        std::cout << "vel         : " << vel_           << "\n";
        std::cout << "applied acc : " << applied_acc_   << "\n";
    }

    /// \brief Types of control available
    enum ControlType {
        CONTROL_POS = 0,
        CONTROL_VEL,
        CONTROL_ACC,
        CONTROL_FORCE
    };

    /// \brief Current control mode
    ControlType control_type_ = CONTROL_POS;

    /// \brief Desired position if in CONTROL_POS mode.
    /// commanded acceleration = (Kp_ * (desired_pos_ - current position) + Kv_ * (desired_vel_ - current velocity)
    POSITION_T desired_pos_;

    /// \brief Desired velocity if in CONTROL_VEL mode.
    /// commanded acceleration = Kv_ * (desired_vel_ - current velocity)
    VELOCITY_T desired_vel_;

    /// \brief Desired acceleration if in CONTROL_ACC mode
    VELOCITY_T desired_acc_;

    /// \brief Desired force if in CONTROL_FORCE mode
    VELOCITY_T desired_force_;

protected:

    /// \brief position of the operational frame
    POSITION_T pos_;

    /// \brief velocity of the operational frame
    VELOCITY_T vel_;

    /// \brief acceleration of the operational frame
    VELOCITY_T acc_;

};

}
} /* namespace taco */
#endif /* GoalTask_H_ */
