/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_MATH_H_
#define TACO_CONTROLLER_MATH_H_

#include <eigen3/Eigen/Dense>


namespace taco {

/** \brief An SVD based implementation of the Moore-Penrose pseudo-inverse.
 *
 * \param M The matrix to be inverted
 * \param epsilon The minimum singular value of m that will be inverted. Otherwise 1/epsilon will be used.
 */
inline Eigen::MatrixXd pseudoInv(const Eigen::MatrixXd& M, const double epsilon = 0.001)
{
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(M, Eigen::ComputeFullU | Eigen::ComputeFullV);
    const Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType singVals = svd.singularValues();
    //Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType invSingVals = singVals;
    Eigen::MatrixXd invSingVals = Eigen::MatrixXd::Constant(M.cols(),M.rows(),0.0);
    for(int i=0; i<singVals.rows(); ++i) {
        if(singVals(i) <= epsilon) {
            //invSingVals(i) = 1.0 / epsilon;
            invSingVals(i,i) = 1.0 / epsilon;
        }
        else {
            //invSingVals(i) = 1.0 / singVals(i);
            invSingVals(i,i) = 1.0 / singVals(i);
        }
    }
    return Eigen::MatrixXd(svd.matrixV() *
                           //invSingVals.asDiagonal() *
                           invSingVals *
                           svd.matrixU().transpose());

}


/** Returns a matrix with singular values that are the minimum of the singular values of M and max_SV.
 *
 * \param M The matrix that should have bounded singular values
 * \param max_SV The maximum singular value that the returned matrix should have.
 *
 */
inline Eigen::MatrixXd boundedSVD(const Eigen::MatrixXd& M, const double max_SV)
{
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(M, Eigen::ComputeFullU | Eigen::ComputeFullV);
    const Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType singVals = svd.singularValues();
    Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType boundedSingVals = singVals;
    for(int i=0; i<singVals.rows(); ++i) {
        if(singVals(i) > max_SV) {
            boundedSingVals(i) = max_SV;
        }
    }
    return Eigen::MatrixXd(svd.matrixU() *
                           boundedSingVals.asDiagonal() *
                           svd.matrixV().transpose());
}


/** For a problem Ax=b, returns a new problem A_full_rank x = b_full_rank
 * with singular directions removed such that the problem is always feasible
 * and solves the corresponding problem.
 *
 * \note Matrix A must be square or wide
 *
 * \param A The possibly rank deficient matrix A
 * \param b The vector b
 * \param A_full_rank The full rank version of matrix A
 * \param b_full_rank The corresponding version of b
 * \param epsilon The singular value threshold, below which will be considered rank deficient
 * \return The number of dimensions removed. 0 corresponds to an unchanged problem.
 */
inline unsigned int reducedLeastSquares(const Eigen::MatrixXd& A, const Eigen::VectorXd& b,
                                        Eigen::MatrixXd& A_full_rank, Eigen::VectorXd& b_full_rank,
                                        double epsilon = 0.0001)
{
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
    const Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType singVals = svd.singularValues();

    // find rank of A
    unsigned int original_rank = A.rows();
    unsigned int new_rank = original_rank;
    for(int i=0; i<singVals.rows(); ++i) {
        if(singVals(i) < epsilon) {
            --new_rank;
        }
    }

    // if A is full rank, return original problem
    if (new_rank==original_rank){
        A_full_rank = A;
        b_full_rank = b;
        return 0;
    }

    // transform by singular directions
    A_full_rank = svd.matrixU()*A;
    b_full_rank = svd.matrixU()*b;

    // remove singular directions
    int i_full_rank = 0;
    for(int i=0; i<singVals.rows(); ++i) {
        if(singVals(i) >= epsilon) {
            A_full_rank.row(i_full_rank) = A_full_rank.row(i);
            ++i_full_rank;
        }
    }
    A_full_rank.resize(new_rank, A.cols());
    b_full_rank.resize(new_rank);

    return original_rank-new_rank;
}


}

#endif /* MATH_H_ */
