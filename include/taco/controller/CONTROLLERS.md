Controllers
===========

| Controller | Reference       | Description                                                   |
| ---------- | --------------- | ------------------------------------------------------------- |
| fixed      | [Khatib 1987]   | Fixed base analytical operational space controller            |
| qp         | [Peters 2008]   | Fixed base quadratic programming operational space controller |
| floating   | See Below       | Floating base (whole-body) QP operational space controller    |


Floating based references:

* [Sentis and Khatib 2005]
* [Chung and Khatib 2016, in publication]
* [Mistry 2010]
* [Righetti 2011]
* [Righetti 2013]


[Khatib 1987]:http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=1087068
[Peters 2008]:https://link.springer.com/article/10.1007%2Fs10514-007-9051-x?LI=true
[Sentis and Khatib 2005]:http://www.worldscientific.com/doi/abs/10.1142/S0219843605000594
[Chung and Khatib 2016, in publication]:https://www.google.com/search?q=constraint-consistent+task-oriented+whole+body+robot+formulation
[Mistry 2010]:http://ieeexplore.ieee.org/document/5509646/
[Righetti 2011]:http://ieeexplore.ieee.org/document/5980156/
[Righetti 2013]:http://journals.sagepub.com/doi/abs/10.1177/0278364912469821





