/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "RBDLDynamics.h"

#include <rbdl/rbdl.h>
#ifndef RBDL_BUILD_ADDON_URDFREADER
  #error "Error: RBDL addon urdfmodel not enabled."
#endif
#include <urdfreader/urdfreader.h>

#include <urdf_parser/urdf_parser.h>
#include <urdf_model/model.h>

#include <iostream>
#include <map>
#include <vector>
#include <stdexcept>

using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

namespace taco {

RBDLDynamics::RBDLDynamics(){}

RBDLDynamics::~RBDLDynamics(){}


void RBDLDynamics::importRobot(const std::string& file, bool verbose)
{
    // parse rbdl model from urdf
    bool success = Addons::URDFReadFromFile(file.c_str(), &model_, verbose);
    if (!success) {
        printWarning("Error loading model [" + file + "]");
    }

    // parse urdf model again, to obtain joint names and initial joint configuration
    const boost::shared_ptr<urdf::ModelInterface> urdf_model = urdf::parseURDFFile(file.c_str());

    // resize q
    q_.setZero(model_.dof_count);
    dq_.setZero(model_.dof_count);
    ddq_.setZero(model_.dof_count);

    // The RBDL link indices are offset by one, because the first link of a
    // model is always a link called ROOT. The first user-specified link has an
    // id of 1. Thus the indices for degrees of freedom must be offset by one.
    const int offset = 1;
    link_name_.resize(model_.dof_count);
    joint_name_.resize(model_.dof_count);
    for(std::map<std::string,unsigned int>::iterator iter = model_.mBodyNameMap.begin();
        iter != model_.mBodyNameMap.end();
        ++iter)
    {   
        // obtain link to generalized coordinate mapping
        std::string link_name = iter->first;

        std::cout << "link: " << link_name << std::endl;

        int id = (int)(iter->second) - offset;
        if (id < 0) { continue; }

        std::cout << "id: " << id << std::endl;

        link_id_[link_name] = id;
        link_name_[id] = link_name;

        // obtain joint to generalized coordinate mapping
        boost::shared_ptr<urdf::Link> urdf_link;
        urdf_model->getLink(link_name, urdf_link);
        if (urdf_link==NULL){
            printWarning("Could not parse link ["+link_name+"].");
            continue;
        }
        boost::shared_ptr<const urdf::Joint> urdf_joint = urdf_link->parent_joint;
        if (urdf_joint==NULL){
            printWarning("Could not parse joint for link ["+link_name+"].");
            continue;
        }
        joint_id_[urdf_joint->name] = id;
        joint_name_[id] = urdf_joint->name;
    }

    updateDynamics();
}

int RBDLDynamics::DOF()
{
    return model_.dof_count;
}


void RBDLDynamics::updateDynamics()
{
    UpdateKinematicsCustom(model_, &q_, &dq_, &ddq_);
}


void RBDLDynamics::massMatrix(Eigen::MatrixXd& M){
    RBDLDynamics::massMatrix(M, q_);
}

void RBDLDynamics::massMatrix(Eigen::MatrixXd& M,
                              const Eigen::MatrixXd& q)
{
    int dof = model_.dof_count;
    if (M.rows()!=dof|| M.cols()!=dof){
        M.setZero(dof,dof);
    }

    CompositeRigidBodyAlgorithm(model_, q, M, false);
}


void  RBDLDynamics::gravityForce(Eigen::VectorXd& F,
                                 const Eigen::Vector3d& gravity)
{
    this->gravityForce(F, q_, gravity);
}

void  RBDLDynamics::gravityForce(Eigen::VectorXd& F,
                                 const Eigen::VectorXd& q,
                                 const Eigen::Vector3d& gravity)
{
    int dof = model_.dof_count;
    if (F.size() != dof){ F.resize(dof); }
    F.setZero();

    std::vector<RigidBodyDynamics::Body>::iterator it_body;
    int body_id;

    for (it_body = model_.mBodies.begin(), body_id=0;
         it_body != model_.mBodies.end();
         ++it_body, ++body_id) {

        double mass = it_body->mMass;

        MatrixNd Jv = MatrixNd::Zero(3, dof);
        CalcPointJacobian(model_, q, body_id, it_body->mCenterOfMass, Jv, false);

        F += Jv.transpose() * (-mass * gravity);
    }
}


void RBDLDynamics::coriolisForce(Eigen::VectorXd& F)
{
    this->coriolisForce(F,q_,dq_);
}

void RBDLDynamics::coriolisForce(Eigen::VectorXd& F,
                                 const Eigen::VectorXd& q,
                                 const Eigen::VectorXd& dq)
{
    NonlinearEffects(model_,q,dq,F);

    printWarning("nonlinearForce() is untested.");
}


void RBDLDynamics::Jv(Eigen::MatrixXd& J,
                      const std::string& link_name,
                      const Eigen::Vector3d& pos_in_link)
{
    this->Jv(J, link_name, pos_in_link, q_);
}

void RBDLDynamics::Jv(Eigen::MatrixXd& J,
                      const std::string& link_name,
                      const Eigen::Vector3d& pos_in_link,
                      const Eigen::VectorXd& q)
{
    int dof = model_.dof_count;
    if (J.rows()!=3 || J.cols()!=dof){
        J.setZero(3,dof);
    }

    CalcPointJacobian(model_, q, bodyID(link_name), pos_in_link, J, false);
}


void RBDLDynamics::Jv(Eigen::MatrixXd& J,
                      const std::string& link_name,
                      const std::string& base_name,
                      const Eigen::Vector3d& pos_in_link)
{
    this->Jv(J, link_name, base_name, pos_in_link, q_);
}

void RBDLDynamics::Jv(Eigen::MatrixXd& J,
                      const std::string& link_name,
                      const std::string& base_name,
                      const Eigen::Vector3d& pos_in_link,
                      const Eigen::VectorXd& q)
{
    // get JV from ground to link
    this->Jv(J, link_name, pos_in_link, q);

    // tranverse from base link to ground
    // setting columns of jacobian to zero
    unsigned int body_id = bodyID(base_name);;
    while( body_id != 0 )
    {
        J.col(body_id-1).setZero();
        body_id = model_.lambda[body_id];
    }

    // TODO
    printWarning("Jv() is untested.");
}


void RBDLDynamics::Jw(Eigen::MatrixXd& J,
                      const std::string& link_name)
{
    this->Jw(J, link_name, q_);
}

void RBDLDynamics::Jw(Eigen::MatrixXd& J,
                      const std::string& link_name,
                      const Eigen::VectorXd& q)
{
    unsigned int body_id = bodyID(link_name);

    // spatial jacobian in body frame
    Eigen::MatrixXd J_temp = Eigen::MatrixXd::Zero(6,DOF());
    CalcBodySpatialJacobian (model_, q, body_id, J_temp, false);

    // rotate to base frame
    Eigen::Matrix3d rot = CalcBodyWorldOrientation(model_, q, body_id, false);
    J = rot.transpose()*J_temp.topRows<3>();
}

void RBDLDynamics::Jw(Eigen::MatrixXd& J,
                         const std::string& link_name,
                         const std::string& base_name)
{
    this->Jw(J, link_name, base_name, q_);
}

void RBDLDynamics::Jw(Eigen::MatrixXd& J,
                         const std::string& link_name,
                         const std::string& base_name,
                         const Eigen::VectorXd& q)
{
    // get JV from ground to link
    this->Jw(J, link_name, q);

    // tranverse from base link to ground
    // setting columns of jacobian to zero
    unsigned int body_id = bodyID(base_name);;
    while( body_id != 0 )
    {
        J.col(body_id-1).setZero();
        body_id = model_.lambda[body_id];
    }

    // TODO
    printWarning("Jw() is untested.");
}

void RBDLDynamics::transform(Eigen::Affine3d& T,
                             const std::string& link_name)
{
    const SpatialTransform& st = model_.X_base[bodyID(link_name)];
    T = Eigen::Translation3d(st.r) * st.E.transpose();
}

void RBDLDynamics::transform(Eigen::Affine3d& T,
                             const std::string& link_name,
                             const Eigen::VectorXd& q)
{
    unsigned int body_id = bodyID(link_name);
    Eigen::Vector3d pos_in_body(0,0,0);
    T = Eigen::Translation3d(CalcBodyToBaseCoordinates(model_, q, body_id, pos_in_body, false))
                           * CalcBodyWorldOrientation(model_, q, body_id, false).transpose();
}

void RBDLDynamics::transform(Eigen::Affine3d& T,
                             const std::string& link_name,
                             const std::string& base_name)
{
    const SpatialTransform& st_link = model_.X_base[bodyID(link_name)];
    const SpatialTransform& st_base = model_.X_base[bodyID(base_name)];

    Eigen::Vector3d dpos = st_base.E * (st_link.r - st_base.r);
    Eigen::Matrix3d drot = st_link.E.transpose() * st_base.E.transpose();
    T = Eigen::Translation3d(dpos) * drot;
}

void RBDLDynamics::transform(Eigen::Affine3d& T,
                             const std::string& link_name,
                             const std::string& base_name,
                             const Eigen::VectorXd& q)
{
    unsigned int link_id = bodyID(link_name);
    unsigned int base_id = bodyID(base_name);
    Eigen::Vector3d pos_in_body(0,0,0);
    Eigen::Matrix3d rot_base = CalcBodyWorldOrientation(model_, q, base_id, false);

    Eigen::Vector3d dpos = rot_base * ( CalcBodyToBaseCoordinates(model_, q, link_id, pos_in_body, false)
                                       -CalcBodyToBaseCoordinates(model_, q, base_id, pos_in_body, false) );
    Eigen::Matrix3d drot = CalcBodyWorldOrientation(model_, q, link_id, false).transpose() * rot_base.transpose();

    T = Eigen::Translation3d(dpos) * drot;
}


void RBDLDynamics::position(Eigen::Vector3d& pos,
                            const std::string& link_name,
                            const Eigen::Vector3d& pos_in_link)
{
    const SpatialTransform& st = model_.X_base[bodyID(link_name)];
    pos = st.r + st.E*pos_in_link;
}

void RBDLDynamics::position(Eigen::Vector3d& pos,
                            const std::string& link_name,
                            const Eigen::Vector3d& pos_in_link,
                            const Eigen::VectorXd& q)
{
    pos = CalcBodyToBaseCoordinates(model_, q, bodyID(link_name), pos_in_link, false);
}

void RBDLDynamics::position(Eigen::Vector3d& pos,
                            const std::string& link_name,
                            const std::string& base_name,
                            const Eigen::Vector3d& pos_in_link)
{
    const SpatialTransform& st_link = model_.X_base[bodyID(link_name)];
    const SpatialTransform& st_base = model_.X_base[bodyID(base_name)];
    pos = st_base.E * (st_link.r + st_link.E*pos_in_link - st_base.r);

    printWarning("position(). untested.");
}

void RBDLDynamics::position(Eigen::Vector3d& pos,
                            const std::string& link_name,
                            const std::string& base_name,
                            const Eigen::Vector3d& pos_in_link,
                            const Eigen::VectorXd& q)
{
    Eigen::Vector3d zero = Eigen::Vector3d::Zero();
    Eigen::Matrix3d rot_base = CalcBodyWorldOrientation(model_, q, bodyID(base_name), false);
    pos = rot_base * ( CalcBodyToBaseCoordinates(model_, q, bodyID(link_name), pos_in_link, false)
                      -CalcBodyToBaseCoordinates(model_, q, bodyID(base_name), zero, false) );

    printWarning("position(). untested.");
}

void RBDLDynamics::velocity(Eigen::Vector3d& vel,
                            const std::string& link_name,
                            const Eigen::Vector3d& pos_in_link)
{
    this->velocity(vel,link_name,pos_in_link,q_,dq_);
}

void RBDLDynamics::velocity(Eigen::Vector3d& vel,
                            const std::string& link_name,
                            const Eigen::Vector3d& pos_in_link,
                            const Eigen::VectorXd& q,
                            const Eigen::VectorXd& dq)
{
    vel = CalcPointVelocity(model_,q,dq,bodyID(link_name),pos_in_link,false);
}

void RBDLDynamics::velocity(Eigen::Vector3d& vel,
                            const std::string& link_name,
                            const std::string& base_name,
                            const Eigen::Vector3d& pos_in_link)
{
    this->velocity(vel, link_name, base_name, pos_in_link, q_, dq_);
}

void RBDLDynamics::velocity(Eigen::Vector3d& vel,
                              const std::string& link_name,
                              const std::string& base_name,
                              const Eigen::Vector3d& pos_in_link,
                              const Eigen::VectorXd& q,
                              const Eigen::VectorXd& dq)
{
    Eigen::Matrix3d rot_base = CalcBodyWorldOrientation(model_, q, bodyID(base_name), false);
    vel = rot_base * (CalcPointVelocity(model_,q,dq,bodyID(link_name),pos_in_link,false)
                     -CalcPointVelocity(model_,q,dq,bodyID(base_name),pos_in_link,false));
}

void RBDLDynamics::acceleration(Eigen::Vector3d& accel,
                                  const std::string& link_name,
                                  const Eigen::Vector3d& pos_in_link)
{
    this->acceleration(accel, link_name, pos_in_link, q_, dq_, ddq_);
}

void RBDLDynamics::acceleration(Eigen::Vector3d& accel,
                                const std::string& link_name,
                                const Eigen::Vector3d& pos_in_link,
                                const Eigen::VectorXd& q,
                                const Eigen::VectorXd& dq,
                                const Eigen::VectorXd& ddq)
{
    accel = CalcPointAcceleration(model_,q,dq,ddq,bodyID(link_name),pos_in_link,false);
}

void RBDLDynamics::acceleration(Eigen::Vector3d& accel,
                                const std::string& link_name,
                                const std::string& base_name,
                                const Eigen::Vector3d& pos_in_link)
{
    this->acceleration(accel, link_name, base_name, pos_in_link, q_, dq_, ddq_);
}

void RBDLDynamics::acceleration(Eigen::Vector3d& accel,
                                const std::string& link_name,
                                const std::string& base_name,
                                const Eigen::Vector3d& pos_in_link,
                                const Eigen::VectorXd& q,
                                const Eigen::VectorXd& dq,
                                const Eigen::VectorXd& ddq)
{
    Eigen::Matrix3d rot_base = CalcBodyWorldOrientation(model_, q, bodyID(base_name), false);
    accel = rot_base * (CalcPointAcceleration(model_,q,dq,ddq,bodyID(link_name),pos_in_link,false)
                       -CalcPointAcceleration(model_,q,dq,ddq,bodyID(base_name),pos_in_link,false));
}

void RBDLDynamics::rotation(Eigen::Matrix3d& rot,
                            const std::string& link_name)
{
    const SpatialTransform& st = model_.X_base[bodyID(link_name)];
    rot = st.E.transpose();
}

void RBDLDynamics::rotation(Eigen::Matrix3d& rot,
                            const std::string& link_name,
                            const Eigen::VectorXd& q)
{
    rot = CalcBodyWorldOrientation(model_, q, bodyID(link_name), false).transpose();
}

void RBDLDynamics::rotation(Eigen::Matrix3d& rot,
                            const std::string& link_name,
                            const std::string& base_name)
{
    const SpatialTransform& st_link = model_.X_base[bodyID(link_name)];
    const SpatialTransform& st_base = model_.X_base[bodyID(base_name)];

    rot = st_link.E.transpose() * st_base.E.transpose();
}

void RBDLDynamics::rotation(Eigen::Matrix3d& rot,
                            const std::string& link_name,
                            const std::string& base_name,
                            const Eigen::VectorXd& q)
{
    rot = CalcBodyWorldOrientation(model_, q, bodyID(link_name), false).transpose()
        * CalcBodyWorldOrientation(model_, q, bodyID(base_name), false).transpose();

}

void RBDLDynamics::angularVelocity(Eigen::Vector3d& vel,
                                    const std::string& link_name)
{
    Math::SpatialVector vel6D = model_.v[bodyID(link_name)];
    vel = vel6D.head(3);

    printWarning("angularVelocity. untested.");
}

void RBDLDynamics::angularVelocity(Eigen::Vector3d& vel,
                                   const std::string& link_name,
                                   const Eigen::VectorXd& q,
                                   const Eigen::VectorXd& dq)
{
    /*
    Eigen::Vector3d pos_in_link = Eigen::Vector3d(0,0,0);
    Math::SpatialVector vel6D =  CalcPointVelocity6D(model_, q, dq, bodyID(link_name), pos_in_link, false);
    vel = vel6d.head(3);
    */

    printWarning("angularVelocity. NOT IMPLEMENTED, REQUIRES RBDL-EXPERIMENTS.");
}

void RBDLDynamics::angularVelocity(Eigen::Vector3d& vel,
                                   const std::string& link_name,
                                   const std::string& base_name)
{
    Math::SpatialVector vel6D_link = model_.v[bodyID(link_name)];
    Math::SpatialVector vel6D_base = model_.v[bodyID(base_name)];

    Eigen::Vector3d vel_link = vel6D_link.head(3);
    Eigen::Vector3d vel_base = vel6D_base.head(3);

    SpatialTransform& st_base = model_.X_base[bodyID(base_name)];
    vel = st_base.E * (vel_link - vel_base);

    printWarning("angularVelocity. untested.");
}

void RBDLDynamics::angularVelocity(Eigen::Vector3d& vel,
                                   const std::string& link_name,
                                   const std::string& base_name,
                                   const Eigen::VectorXd& q,
                                   const Eigen::VectorXd& dq)
{
    /*
    pos_in_link = Eigen::Vector3d(0,0,0);
    Math::SpatialVector vel6D_link =  CalcPointVelocity6D(model_, q, dq, bodyID(link_name), pos_in_link, false);
    Math::SpatialVector vel6D_base =  CalcPointVelocity6D(model_, q, dq, bodyID(link_name), pos_in_link, false);

    Eigen::Matrix3d rot_base = CalcBodyWorldOrientation(model_, q_, base_id, false);
    vel = rot_base * (vel6D_link.head(3) - vel6D_base.head(3));
    */

    printWarning("angularVelocity. NOT IMPLEMENTED, REQUIRES RBDL-EXPERIMENTS.");
}

void RBDLDynamics::angularAcceleration(Eigen::Vector3d& accel,
                                       const std::string& link_name)
{
    Math::SpatialVector accel6D = model_.a[bodyID(link_name)];
    accel = accel6D.head(3);

    printWarning("angularAcceleration. untested.");
}

void RBDLDynamics::angularAcceleration(Eigen::Vector3d& accel,
                                       const std::string& link_name,
                                       const Eigen::VectorXd& q,
                                       const Eigen::VectorXd& dq,
                                       const Eigen::VectorXd& ddq)
{
    /*
    pos_in_link = Eigen::Vector3d(0,0,0);
    Math::SpatialVector accel6D =  CalcPointAcceleration6D(model_, q, dq, bodyID(link_name), pos_in_link, false);
    accel = accel6d.head(3);
    */

    printWarning("angularAcceleration. NOT IMPLEMENTED, REQUIRES RBDL-EXPERIMENTS.");
}

void RBDLDynamics::angularAcceleration(Eigen::Vector3d& accel,
                                       const std::string& link_name,
                                       const std::string& base_name)
{
    Math::SpatialVector accel6D_link = model_.a[bodyID(link_name)];
    Math::SpatialVector accel6D_base = model_.a[bodyID(base_name)];

    Eigen::Vector3d vel_link = accel6D_link.head(3);
    Eigen::Vector3d vel_base = accel6D_base.head(3);

    SpatialTransform& st_base = model_.X_base[bodyID(base_name)];
    accel = st_base.E * (vel_link - vel_base);

    printWarning("angularAcceleration. untested.");
}

void RBDLDynamics::angularAcceleration(Eigen::Vector3d& accel,
                                       const std::string& link_name,
                                       const std::string& base_name,
                                       const Eigen::VectorXd& q,
                                       const Eigen::VectorXd& dq,
                                       const Eigen::VectorXd& ddq)
{
    /*
    pos_in_link = Eigen::Vector3d(0,0,0);
    Math::SpatialVector accel6D_link =  CalcPointAcceleration6D(model_, q, dq, bodyID(link_name), pos_in_link, false);
    Math::SpatialVector accel6D_base =  CalcPointAcceleration6D(model_, q, dq, bodyID(link_name), pos_in_link, false);

    Eigen::Matrix3d rot_base = CalcBodyWorldOrientation(model_, q_, base_id, false);
    vel = rot_base * (accel6D_link.head(3) - accel6D_base.head(3));
    */

    printWarning("angularAcceleration. NOT IMPLEMENTED, REQUIRES RBDL-EXPERIMENTS.");
}

void RBDLDynamics::CoMPosition(Eigen::Vector3d& pos)
{
    this->CoMPosition(pos, q_);
}

void RBDLDynamics::CoMPosition(Eigen::Vector3d& pos, const Eigen::VectorXd& q)
{
    double mass_total = 0;
    Math::Vector3d com_pos;
    Eigen::VectorXd qdot = Eigen::VectorXd::Zero(model_.dof_count);
    Utils::CalcCenterOfMass(model_, q, qdot, mass_total, com_pos, NULL, NULL, false);
    pos = com_pos;

    printWarning("CoMPosition. untested.");
}


void RBDLDynamics::JvCoM(Eigen::MatrixXd& J)
{
    JvCoM(J, q_);
}

void RBDLDynamics::JvCoM(Eigen::MatrixXd& J,
                         const Eigen::VectorXd& q)
{
    int dof = model_.dof_count;
    J.setZero(3,dof);

    double mass_total = 0;
    Eigen::MatrixXd Jv_com;
    Jv_com.setZero(3,dof);

    std::vector<RigidBodyDynamics::Body>::iterator it_body;
    int body_id;

    for (it_body = model_.mBodies.begin(), body_id = 0;
         it_body != model_.mBodies.end();
         ++it_body, ++body_id) {

         double mass = it_body->mMass;
         mass_total += mass;

         MatrixNd Jv = MatrixNd::Zero(3, dof);
         CalcPointJacobian(model_, q, body_id, it_body->mCenterOfMass, Jv, false);

         J += mass * Jv;
    }

    J = (1.0/mass_total) * J;
}

void RBDLDynamics::printRobotInfo()
{  
    std::cout<<"robot info..."<<std::endl;
    for(auto entry : model_.mBodyNameMap)
    {
        std::cout<<"link: "<<entry.first<<", "<<entry.second<<'\n';


        std::cout<<"children: ";
        for(unsigned int i : model_.mu[entry.second]){
            std::cout<<i<<" ";
        }
        std::cout<<std::endl;
    }
}

unsigned int RBDLDynamics::bodyID(const std::string& link_name)
{
    auto iter = model_.mBodyNameMap.find(link_name);
    unsigned int body_id = iter->second;

    #ifndef NDEBUG
    if (iter == model_.mBodyNameMap.end()) {
        printWarning("link ["+link_name+"] does not exists");
    }
    #endif

    return body_id;
}

void RBDLDynamics::printWarning(const std::string &message)
{
    std::cout << "WARNING. RBDLDynamics. " << message << std::endl;
}

} /* namespace taco */
