/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_DYNAMICS_RBDLDYNAMICS_H_
#define TACO_DYNAMICS_RBDLDYNAMICS_H_

#include "Dynamics.h"

#include <rbdl/Model.h>
#include <eigen3/Eigen/Dense>
#include <boost/shared_ptr.hpp>
#include <string>


namespace RigidBodyDynamics {
    class Model;
}

namespace taco {

/** @brief Dynamics engine based upon RBDL
 *
 * @ingroup  dynamics
 */
class RBDLDynamics : public Dynamics {
public:
    RBDLDynamics();
    virtual ~RBDLDynamics();

    virtual void importRobot(const std::string& file_name, bool verbose = true);

    virtual int DOF();

    virtual void updateDynamics();

    virtual void massMatrix(Eigen::MatrixXd& M);
    virtual void massMatrix(Eigen::MatrixXd& M,
                            const Eigen::MatrixXd& q);

    virtual void gravityForce(Eigen::VectorXd& F,
                              const Eigen::Vector3d& gravity = Eigen::Vector3d(0,0,-9.81));
    virtual void gravityForce(Eigen::VectorXd& F,
                              const Eigen::VectorXd& q,
                              const Eigen::Vector3d& gravity = Eigen::Vector3d(0,0,-9.81));

    virtual void coriolisForce(Eigen::VectorXd& F);
    virtual void coriolisForce(Eigen::VectorXd& F,
                                const Eigen::VectorXd& q,
                                const Eigen::VectorXd& dq);

    virtual void Jv(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const Eigen::Vector3d& pos_in_link);
    virtual void Jv(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const Eigen::Vector3d& pos_in_link,
                    const Eigen::VectorXd& q);

    virtual void Jv(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const std::string& base_name,
                    const Eigen::Vector3d& pos_in_link);
    virtual void Jv(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const std::string& base_name,
                    const Eigen::Vector3d& pos_in_link,
                    const Eigen::VectorXd& q);

    virtual void Jw(Eigen::MatrixXd& J,
                    const std::string& link_name);
    virtual void Jw(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const Eigen::VectorXd& q);

    virtual void Jw(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const std::string& base_name);
    virtual void Jw(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const std::string& base_name,
                    const Eigen::VectorXd& q);

    virtual void transform(Eigen::Affine3d& T,
                           const std::string& link_name);
    virtual void transform(Eigen::Affine3d& T,
                           const std::string& link_name,
                           const Eigen::VectorXd& q);

    virtual void transform(Eigen::Affine3d& T,
                           const std::string& link_name,
                           const std::string& base_name);
    virtual void transform(Eigen::Affine3d& T,
                           const std::string& link_name,
                           const std::string& base_name,
                           const Eigen::VectorXd& q);

    virtual void position(Eigen::Vector3d& pos,
                          const std::string& link_name,
                          const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0));
    virtual void position(Eigen::Vector3d& pos,
                          const std::string& link_name,
                          const Eigen::Vector3d& pos_in_link,
                          const Eigen::VectorXd& q);

    virtual void position(Eigen::Vector3d& pos,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0));
    virtual void position(Eigen::Vector3d& pos,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::Vector3d& pos_in_link,
                          const Eigen::VectorXd& q);

    virtual void velocity(Eigen::Vector3d& vel,
                          const std::string& link_name,
                          const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0) );
    virtual void velocity(Eigen::Vector3d& vel,
                          const std::string& link_name,
                          const Eigen::Vector3d& pos_in_link,
                          const Eigen::VectorXd& q,
                          const Eigen::VectorXd& dq);

    virtual void velocity(Eigen::Vector3d& vel,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0));
    virtual void velocity(Eigen::Vector3d& vel,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::Vector3d& pos_in_link,
                          const Eigen::VectorXd& q,
                          const Eigen::VectorXd& dq);

    virtual void acceleration(Eigen::Vector3d& accel,
                              const std::string& link_name,
                              const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0));
    virtual void acceleration(Eigen::Vector3d& accel,
                              const std::string& link_name,
                              const Eigen::Vector3d& pos_in_link,
                              const Eigen::VectorXd& q,
                              const Eigen::VectorXd& dq,
                              const Eigen::VectorXd& ddq);

    virtual void acceleration(Eigen::Vector3d& accel,
                              const std::string& link_name,
                              const std::string& base_name,
                              const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0));
    virtual void acceleration(Eigen::Vector3d& accel,
                              const std::string& link_name,
                              const std::string& base_name,
                              const Eigen::Vector3d& pos_in_link,
                              const Eigen::VectorXd& q,
                              const Eigen::VectorXd& dq,
                              const Eigen::VectorXd& ddq);

    virtual void rotation(Eigen::Matrix3d& rot,
                          const std::string& link_name);
    virtual void rotation(Eigen::Matrix3d& rot,
                          const std::string& link_name,
                          const Eigen::VectorXd& q);

    virtual void rotation(Eigen::Matrix3d& rot,
                          const std::string& link_name,
                          const std::string& base_name);
    virtual void rotation(Eigen::Matrix3d& rot,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::VectorXd& q);

    virtual void angularVelocity(Eigen::Vector3d& vel,
                                 const std::string& link_name);
    virtual void angularVelocity(Eigen::Vector3d& vel,
                                 const std::string& link_name,
                                 const Eigen::VectorXd& q,
                                 const Eigen::VectorXd& dq);

    virtual void angularVelocity(Eigen::Vector3d& vel,
                                 const std::string& link_name,
                                 const std::string& base_name);
    virtual void angularVelocity(Eigen::Vector3d& vel,
                                 const std::string& link_name,
                                 const std::string& base_name,
                                 const Eigen::VectorXd& q,
                                 const Eigen::VectorXd& dq);

    virtual void angularAcceleration(Eigen::Vector3d& accel,
                                     const std::string& link_name);
    virtual void angularAcceleration(Eigen::Vector3d& accel,
                                     const std::string& link_name,
                                     const Eigen::VectorXd& q,
                                     const Eigen::VectorXd& dq,
                                     const Eigen::VectorXd& ddq);

    virtual void angularAcceleration(Eigen::Vector3d& accel,
                                     const std::string& link_name,
                                     const std::string& base_name);
    virtual void angularAcceleration(Eigen::Vector3d& accel,
                                     const std::string& link_name,
                                     const std::string& base_name,
                                     const Eigen::VectorXd& q,
                                     const Eigen::VectorXd& dq,
                                     const Eigen::VectorXd& ddq);

    virtual void CoMPosition(Eigen::Vector3d& pos);
    virtual void CoMPosition(Eigen::Vector3d& pos,
                             const Eigen::VectorXd& q);

    virtual void JvCoM(Eigen::MatrixXd& J);
    virtual void JvCoM(Eigen::MatrixXd& J,
                       const Eigen::VectorXd& q);

    void printRobotInfo();

private:

    unsigned int bodyID(const std::string& link_name);

    virtual void printWarning(const std::string& message);

    RigidBodyDynamics::Model model_;

};

} /* namespace taco */
#endif /* CRBDLDynamics_H_ */
