/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_DYNAMICS_DYNAMICS_H_
#define TACO_DYNAMICS_DYNAMICS_H_

#include <eigen3/Eigen/Dense>

#include <vector>
#include <map>
#include <string>
#include <iostream>


namespace taco {

/** @brief Base class of dynamic engine used to calculate kinematics for the controller
 *
 * @ingroup  dynamics
 */
class Dynamics
{

public:
    Dynamics() {}
    virtual ~Dynamics() {}

    /** parse kinematics from file.
     *  also sets the generalized coordinate to link/joint name  maps */
    virtual void importRobot(const std::string& file_name, bool verbose = true) {}

    ////////////////////////////////////////////////////////////////
    //           KINEMATICS
    ////////////////////////////////////////////////////////////////

    /** number of degrees of freedom */
    virtual int DOF() = 0;

    /** update the dynamics. call after setting joint positions and velocities. */
    virtual void updateDynamics(){}

    /** mass matrix (dof x dof) */
    virtual void massMatrix(Eigen::MatrixXd& M) = 0;
    virtual void massMatrix(Eigen::MatrixXd& M,
                            const Eigen::MatrixXd& q) = 0;

    /** force/torque on each generalized coordinate from gravity */
    virtual void gravityForce(Eigen::VectorXd& F,
                              const Eigen::Vector3d& gravity = Eigen::Vector3d(0,0,-9.8)) = 0;
    virtual void gravityForce(Eigen::VectorXd& F,
                              const Eigen::VectorXd& q,
                              const Eigen::Vector3d& gravity = Eigen::Vector3d(0,0,-9.8)) = 0;

    /** force/torque on each generalized coordinate from coriolis and centrifugal */
    virtual void coriolisForce(Eigen::VectorXd& F) = 0;
    virtual void coriolisForce(Eigen::VectorXd& F,
                                const Eigen::VectorXd& q,
                                const Eigen::VectorXd& dq) = 0;

    /** velocity jacobian for point on link, relative to base (id=0) */
    virtual void Jv(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const Eigen::Vector3d& pos_in_link) = 0;
    virtual void Jv(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const Eigen::Vector3d& pos_in_link,
                    const Eigen::VectorXd& q) = 0;

    /** velocity jacobian for point on link, relative to a specified base link */
    virtual void Jv(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const std::string& base_name,
                    const Eigen::Vector3d& pos_in_link) = 0;
    virtual void Jv(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const std::string& base_name,
                    const Eigen::Vector3d& pos_in_link,
                    const Eigen::VectorXd& q) = 0;

    /** angular velocity jacobian for link, relative to base (id=0) */
    virtual void Jw(Eigen::MatrixXd& J,
                    const std::string& link_name) = 0;
    virtual void Jw(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const Eigen::VectorXd& q) = 0;

    /** angular velocity jacobian for link, relative to a specified base link (id=0) */
    virtual void Jw(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const std::string& base_name) = 0;
    virtual void Jw(Eigen::MatrixXd& J,
                    const std::string& link_name,
                    const std::string& base_name,
                    const Eigen::VectorXd& q) = 0;

    /** transformation from ground to link, in ground coordinates. */
    virtual void transform(Eigen::Affine3d& T,
                           const std::string& link_name) = 0;
    virtual void transform(Eigen::Affine3d& T,
                           const std::string& link_name,
                           const Eigen::VectorXd& q) = 0;

    /** transformation from base to link, in base coordinates. */
    virtual void transform(Eigen::Affine3d& T,
                           const std::string& link_name,
                           const std::string& base_name) = 0;
    virtual void transform(Eigen::Affine3d& T,
                           const std::string& link_name,
                           const std::string& base_name,
                           const Eigen::VectorXd& q) = 0;

    /** position from ground to link, in ground coordinates. */
    virtual void position(Eigen::Vector3d& pos,
                          const std::string& link_name,
                          const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0)) = 0;
    virtual void position(Eigen::Vector3d& pos,
                          const std::string& link_name,
                          const Eigen::Vector3d& pos_in_link,
                          const Eigen::VectorXd& q) = 0;

    /** position from base to link, in base coordinates. */
    virtual void position(Eigen::Vector3d& pos,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0)) = 0;
    virtual void position(Eigen::Vector3d& pos,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::Vector3d& pos_in_link,
                          const Eigen::VectorXd& q) = 0;

    /** velocity from ground to link, in ground coordinates. */
    virtual void velocity(Eigen::Vector3d& vel,
                          const std::string& link_name,
                          const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0) ) = 0;
    virtual void velocity(Eigen::Vector3d& vel,
                          const std::string& link_name,
                          const Eigen::Vector3d& pos_in_link,
                          const Eigen::VectorXd& q,
                          const Eigen::VectorXd& dq) = 0;

    /** velocity from base to link, in base coordinates. */
    virtual void velocity(Eigen::Vector3d& vel,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0)) = 0;
    virtual void velocity(Eigen::Vector3d& vel,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::Vector3d& pos_in_link,
                          const Eigen::VectorXd& q,
                          const Eigen::VectorXd& dq) = 0;

    /** acceleration from ground to link, in ground coordinates. */
    virtual void acceleration(Eigen::Vector3d& accel,
                              const std::string& link_name,
                              const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0)) = 0;
    virtual void acceleration(Eigen::Vector3d& accel,
                              const std::string& link_name,
                              const Eigen::Vector3d& pos_in_link,
                              const Eigen::VectorXd& q,
                              const Eigen::VectorXd& dq,
                              const Eigen::VectorXd& ddq) = 0;

    /** acceleration from base to link, in base coordinates. */
    virtual void acceleration(Eigen::Vector3d& accel,
                              const std::string& link_name,
                              const std::string& base_name,
                              const Eigen::Vector3d& pos_in_link = Eigen::Vector3d(0,0,0)) = 0;
    virtual void acceleration(Eigen::Vector3d& accel,
                              const std::string& link_name,
                              const std::string& base_name,
                              const Eigen::Vector3d& pos_in_link,
                              const Eigen::VectorXd& q,
                              const Eigen::VectorXd& dq,
                              const Eigen::VectorXd& ddq) = 0;

    /** rotation from ground to link, in ground coordinates. */
    virtual void rotation(Eigen::Matrix3d& rot,
                          const std::string& link_name) = 0;
    virtual void rotation(Eigen::Matrix3d& rot,
                          const std::string& link_name,
                          const Eigen::VectorXd& q) = 0;

    /** rotation from base to link, in base coordinates. */
    virtual void rotation(Eigen::Matrix3d& rot,
                          const std::string& link_name,
                          const std::string& base_name) = 0;
    virtual void rotation(Eigen::Matrix3d& rot,
                          const std::string& link_name,
                          const std::string& base_name,
                          const Eigen::VectorXd& q) = 0;

    /** rotation from ground to link, in ground coordinates. */
    virtual void angularVelocity(Eigen::Vector3d& vel,
                                 const std::string& link_name) = 0;
    virtual void angularVelocity(Eigen::Vector3d& vel,
                                 const std::string& link_name,
                                 const Eigen::VectorXd& q,
                                 const Eigen::VectorXd& dq) = 0;

    /** rotation from base to link, in base coordinates. */
    virtual void angularVelocity(Eigen::Vector3d& vel,
                                 const std::string& link_name,
                                 const std::string& base_name) = 0;
    virtual void angularVelocity(Eigen::Vector3d& vel,
                                 const std::string& link_name,
                                 const std::string& base_name,
                                 const Eigen::VectorXd& q,
                                 const Eigen::VectorXd& dq) = 0;

    /** rotation from ground to link, in ground coordinates. */
    virtual void angularAcceleration(Eigen::Vector3d& accel,
                                     const std::string& link_name) = 0;
    virtual void angularAcceleration(Eigen::Vector3d& accel,
                                     const std::string& link_name,
                                     const Eigen::VectorXd& q,
                                     const Eigen::VectorXd& dq,
                                     const Eigen::VectorXd& ddq) = 0;

    /** rotation from base to link, in base coordinates. */
    virtual void angularAcceleration(Eigen::Vector3d& accel,
                                     const std::string& link_name,
                                     const std::string& base_name) = 0;
    virtual void angularAcceleration(Eigen::Vector3d& accel,
                                     const std::string& link_name,
                                     const std::string& base_name,
                                     const Eigen::VectorXd& q,
                                     const Eigen::VectorXd& dq,
                                     const Eigen::VectorXd& ddq) = 0;

    /** center of mass position */
    virtual void CoMPosition(Eigen::Vector3d& pos) = 0;
    virtual void CoMPosition(Eigen::Vector3d& pos,
                             const Eigen::VectorXd& q) = 0;

    /** center of mass velocity Jacobian */
    virtual void JvCoM(Eigen::MatrixXd& J) = 0;
    virtual void JvCoM(Eigen::MatrixXd& J,
                       const Eigen::VectorXd& q) = 0;

    ////////////////////////////////////////////////////////////////
    //           ROBOT INFORMATION
    ////////////////////////////////////////////////////////////////

    /** print robot info */
    virtual void printRobotInfo()
    {
        std::cout << "index\tjoint\tlink\n";
        for (int i=0;i<DOF();++i){
            std::cout << i << ",\t" << joint_name_[i] << ",\t" << link_name_[i] << '\n';
        }
    }

    /** generalized coordinate (index) of link.
     *  returns -1 if link does not exist. */
    virtual int linkID(const std::string& link_name)
    {
        auto iter = link_id_.find(link_name);
        if (iter==link_id_.end()){
            //printWarning("linkID. not a valid link name ["+link_name+"].");
            return -1;
        } else {
            return iter->second;
        }
    }

    /** name of link at generalized coordinate (index) */
    virtual std::string linkName(const int link_id){
        return link_name_[link_id];
    }

    /** vector of link names, ordered by generalized coordinate */
    virtual const std::vector<std::string>& links(){
        return link_name_;
    }

    /** generalized coordinate (index) of joint.
     *  returns -1 if joint does not exist. */
    virtual int jointID(const std::string& joint_name)
    {
        auto iter = joint_id_.find(joint_name);
        if (iter==joint_id_.end()){
            //printWarning("jointID. not a valid joint name ["+joint_name+"].");
            return -1;
        } else {
            return iter->second;
        }
    }

    /** name of joint at generalized coordinate (index) */
    virtual std::string jointName(const int joint_id){
        return joint_name_[joint_id];
    }

    /** vector of joint names, ordered by generalized coordinate */
    virtual const std::vector<std::string>& joints(){
        return joint_name_;
    }

public:

    /** set joint position by name */
    inline void setq(const std::string& joint_name, double position){
        q_(joint_id_[joint_name]) = position;
    }

    /** set joint velocity by name */
    void setdq(const std::string& joint_name, double velocity){
        dq_(joint_id_[joint_name]) = velocity;
    }

    /** set joint acceleration by name */
    void setddq(const std::string& joint_name, double acceleration){
        ddq_(joint_id_[joint_name]) = acceleration;
    }

    /** generalized coordinates */
    Eigen::VectorXd q_;

    /** generalized coordinates velocity*/
    Eigen::VectorXd dq_;

    /** generalized coordinate acceleration */
    Eigen::VectorXd ddq_;

protected:

    void printWarning(const std::string& message) {
        std::cout << "WARNING. Dynamics. " << message << std::endl;
    }

    // we assume each link has only one child/parent joint.
    std::map<std::string,int> link_id_;
    std::map<std::string,int> joint_id_;
    std::vector<std::string> link_name_;
    std::vector<std::string> joint_name_;

};

}
#endif /* DYNAMICS_H_ */
