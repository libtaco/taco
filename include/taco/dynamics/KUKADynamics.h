/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_DYNAMICS_KUKADYNAMICS_H_
#define TACO_DYNAMICS_KUKADYNAMICS_H_

#include <KukaLBRDynamics/Robot.h>
#include <taco/dynamics/RBDLDynamics.h>
#include <eigen3/Eigen/Dense>
#include <boost/shared_ptr.hpp>
#include <string>


namespace taco {

/** @brief Dynamics engine based upon RBDL,
 * with mass/inertia dependent functions overloaded with Kuka proprietary computation.
 *
 * @ingroup  dynamics
 */
class KukaDynamics : public ::taco::RBDLDynamics {
public:
    KukaDynamics(kuka::Robot::robotName robot_type) : kuka_lbr_dynamics_(robot_type) {}

    virtual ~KukaDynamics(){}

    virtual void massMatrix(Eigen::MatrixXd& M)
    {
        kuka_lbr_dynamics_.getMassMatrix(M, q_);
    }

    virtual void massMatrix(Eigen::MatrixXd& M,
                            const Eigen::VectorXd& q)
    {
        Eigen::VectorXd q_copy = q;
        kuka_lbr_dynamics_.getMassMatrix( M, q_copy);
    }

    virtual void gravityForce(Eigen::VectorXd& F,
                              const Eigen::Vector3d& gravity = Eigen::Vector3d(0,0,-9.81))
    {
        int dof = this->DOF();
        Eigen::VectorXd c(dof);
        Eigen::VectorXd dq = Eigen::VectorXd::Zero(dof);
        kuka_lbr_dynamics_.getCoriolisAndGravityVector(c, F, q_, dq);
    }

    virtual void gravityForce(Eigen::VectorXd& F,
                              const Eigen::VectorXd& q,
                              const Eigen::Vector3d& gravity = Eigen::Vector3d(0,0,-9.81))
    {
        int dof = this->DOF();
        Eigen::VectorXd c(dof);
        Eigen::VectorXd dq = Eigen::VectorXd::Zero(dof);
        Eigen::VectorXd q_copy = q;
        kuka_lbr_dynamics_.getCoriolisAndGravityVector(c, F, q_copy, dq);
    }

    virtual void coriolisForce(Eigen::VectorXd& F)
    {
        Eigen::VectorXd g(this->DOF());
        kuka_lbr_dynamics_.getCoriolisAndGravityVector(F, g, q_, dq_);
    }

    virtual void coriolisForce(Eigen::VectorXd& F,
                                const Eigen::VectorXd& q,
                                const Eigen::VectorXd& dq)
    {
        Eigen::VectorXd g(this->DOF());
        Eigen::VectorXd q_copy = q;
        Eigen::VectorXd dq_copy = dq;
        kuka_lbr_dynamics_.getCoriolisAndGravityVector(F, g, q_copy, dq_copy);
    }

protected:
    kuka::Robot kuka_lbr_dynamics_;

};

} /* namespace taco */
#endif /* CKukaDynamics_H_ */
