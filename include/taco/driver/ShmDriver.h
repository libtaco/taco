/** \file
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_DRIVERS_SHMDRIVER_H_
#define TACO_DRIVERS_SHMDRIVER_H_

#include <taco/comm/shm/SharedMemory.h>
#include <taco/comm/shm/JointCommand.h>
#include <taco/comm/shm/JointState.h>
#include <taco/comm/topics.h>
#include <taco/comm/copy.h>
#include <taco/model/models.h>

#include <time.h>
#include <chrono>
#include <string>
#include <sstream>
#include <stdexcept>

namespace taco {


/** \brief Object that contains shared memory data, and convenient functionality.
 *
 * Use as follows:
 *
 * 1. Call initializeSharedMemory() in constructor
 * 2. Start control while loop, and within while loop:
 * 3. Update joint_state_
 * 4. Check for commandTimeout()
 * 5. If no timeout, then set robot torques from joint_command_
 * 6. If timeout, then updateTimeoutTorque() and set robot torques from timeout_torque_
 * 7. Repeat steps 3-7
*/
template <unsigned int DOF>
class ShmDriver
{
   
public:
      
    ShmDriver()
    {
    }

    ~ShmDriver()
    {
        {
            TACO_SHM_SCOPED_LOCK(joint_state_->sync.mutex);
            joint_state_->sync.removed = true;
        }
        {
            TACO_SHM_SCOPED_LOCK(joint_command_->sync.mutex);
            joint_command_->sync.removed = true;
        }
    }

    /** Initialize the shared memory */
    virtual void initializeSharedMemory(const std::string& robot_name,
                                        const std::vector<std::string>& joint_names)
    {
        robot_name_ = robot_name;

        // Initialize the shared memory for the joint states
        joint_state_ = shm_joint_state_.create(robot_name_+":"+taco::topic::JOINT_STATE);

        // Initialize the shared memory for the joint commands
        joint_command_ = shm_joint_command_.create(robot_name_+":"+taco::topic::JOINT_COMMAND);

        {
            TACO_SHM_SCOPED_LOCK(joint_state_->sync.mutex);
            for (unsigned int i=0; i<DOF; ++i){
                if (joint_names[i].size() >= taco::shm::STR_LEN){
                    throwException("joint name ["+joint_names[i]+"] exceeds shared memory char array size");
                }
                taco::copy(std::string(joint_names[i]), joint_state_->joint_names[i]);
            }
        }
        {
            TACO_SHM_SCOPED_LOCK(joint_command_->sync.mutex);
            for (unsigned int i=0; i<DOF; ++i){
                if (joint_names[i].size() >= taco::shm::STR_LEN){
                    throwException("joint name ["+joint_names[i]+"] exceeds shared memory char array size");
                }
                taco::copy(std::string(joint_names[i]), joint_command_->joint_names[i]);
            }
            joint_command_->sync.initialized = true;
        }
        t_start_ = std::chrono::system_clock::now();
    }

    /** \brief override timeout position */
    virtual void setTimeoutPosition()
    {
        for (unsigned int i=0; i<DOF; ++i){
            timeout_position_[i] = joint_state_->position[i];
        }

        std::stringstream ss;
        ss << "Timeout position: ";
        for (unsigned int i=0; i<DOF; ++i){
            ss << timeout_position_[i] << " ";
        }
        printMessage(ss.str());
    }

    /** \brief Check for command timeout. Call within the joint command scoped lock.
     *
     * If command timeout occurs (switches to true), then this sets the timeout position.
     *
     * \return True if the time between commands has exceeded command_timeout_ */
    virtual bool commandTimeout()
    {
        if (joint_command_->header.seq==last_seq_){
            // old command, check for timeout
            if (timeout_mode_==false){
                // old command, check for timeout
                t_end_ = std::chrono::system_clock::now();
                std::chrono::duration<double> elapsed_seconds = t_end_-t_start_;
                if (elapsed_seconds.count() > timeout_){
                    // it timeout occured, set timeout_position_
                    printWarning("Command timeout.");

                    timeout_mode_ = true;
                    for (unsigned int i=0; i<DOF; ++i){
                        timeout_position_[i] = joint_state_->position[i];
                    }

                    std::stringstream ss;
                    ss << "Holding timeout position: ";
                    for (unsigned int i=0; i<DOF; ++i){
                        ss << timeout_position_[i] << " ";
                    }
                    printMessage(ss.str());
                }
            }
        } else {
            // new command
            if (timeout_mode_==true){
                printMessage("Receiving commands.");
                timeout_mode_ = false;
            }
            last_seq_ = joint_command_->header.seq;
            t_start_ = std::chrono::system_clock::now();
        }

        return timeout_mode_;
    }

    /** \brief Calculate the torques required to hold the timeout position, i.e. timeout_torque_ */
    virtual void updateTimeoutTorque()
    {
        if (!timeout_mode_){
            //printWarning("updateTimeoutTorque() called when not in timeout.\n");
        }
        for (unsigned int i=0; i<DOF; ++i){
            timeout_torque_[i] = timeout_Kp_[i]*(timeout_position_[i] - joint_state_->position[i])
                               - timeout_Kd_[i]*joint_state_->velocity[i];
        }
    }

    /** \brief Timeout position */
    double timeoutPosition(unsigned int index)
    {
        return timeout_position_[index];
    }

    /** \brief Timeout position */
    double timeoutTorque(unsigned int index)
    {
        return timeout_torque_[index];
    }

    virtual void printWarning(const std::string& message){
        std::cout << "WARNING. ShmDriver [" << robot_name_ << "]. " << message << std::endl;
    }

    virtual void printMessage(const std::string& message){
        std::cout << "ShmDriver [" << robot_name_ << "]. " << message << '\n';
    }

    virtual void throwException(const std::string& message){
        throw std::runtime_error("ShmDriver [" + robot_name_ + "]. " + message);
    }

    /** Joint command shared memory. Read only. Lock before reading. */
    taco::shm::JointCommand<DOF>* joint_command_ = NULL;

    /** Joint state shared memory. Read/write. Lock before writing. */
    taco::shm::JointState  <DOF>* joint_state_ = NULL;

    /** \brief if time between commands exceeds command_timeout_ (in seconds), robot will switch to floating. */
    double timeout_ = 0.5;

    /** \brief Proportional gain for holding position if command timeout. */
    double timeout_Kp_[DOF] = {100};

    /** \brief Derivative gain for damping motion if command timeout. Should be positive. */
    double timeout_Kd_[DOF] = {2*sqrt(100)};

    /** brief Elapsed time, for convenience. */
    inline double elapsedTime(timespec start, timespec end)
    {
        timespec temp;
        if ((end.tv_nsec-start.tv_nsec)<0) {
            temp.tv_sec = end.tv_sec-start.tv_sec-1;
            temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
        } else {
            temp.tv_sec = end.tv_sec-start.tv_sec;
            temp.tv_nsec = end.tv_nsec-start.tv_nsec;
        }
        return temp.tv_sec + 0.000000001*temp.tv_nsec;
    }

protected:

    /** \brief Position to hold when timeout occurs */
    double timeout_position_[DOF] = {0};

    /** \brief Proportional gain for daming motion if command timeout */
    double timeout_torque_[DOF] = {0};

    /** \brief Time between commands */
    std::chrono::time_point<std::chrono::system_clock> t_start_, t_end_;

    /** \brief last command time stamp */
    unsigned long last_seq_ = 0;

    /** \brief Check if in timeout mode */
    bool timeout_mode_ = false;

    /** \brief robot name used for the shared memory segment name. */
    std::string robot_name_;

    /** \brief Shared memory for joint command */
    taco::shm::SharedMemory<taco::shm::JointCommand<DOF> >shm_joint_command_;

    /** \brief Shared memory for joint state */
    taco::shm::SharedMemory<taco::shm::JointState  <DOF> >shm_joint_state_;
};

}

#endif // _KUKA_FRI_MY_LBR_CLIENT_H
