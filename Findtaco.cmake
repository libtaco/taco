# ====== USER CONFIGURATION ======

# copy these lines into your CMakeLists.txt, above find_package(taco)
# with option set as ON or OFF
option(TACO_USE_RBDL    "TACO_USE_RBDL"     ON)
option(TACO_USE_GAZEBO  "TACO_USE_GAZEBO"   OFF)
option(TACO_USE_ROS     "TACO_USE_ROS"      OFF)
option(TACO_USE_SHM     "TACO_USE_SHM"      ON)

# ====== FIND AND LINK LIBRARIES ======

# TACO
set(TACO_DIR ${CMAKE_CURRENT_LIST_DIR})
set(TACO_INCLUDE_DIR ${TACO_DIR}/include)
if(NOT TARGET taco)
    find_library( TACO_LIBRARY NAMES taco PATHS ${TACO_DIR}/lib/ )
endif()

# EIGEN LIBARARY
find_package (Eigen3 REQUIRED)

# RBDL
if(TACO_USE_RBDL)
    set(RBDL_DIR ${TACO_DIR}/3rdparty/rbdl)
    set(RBDL_INCLUDE_DIRS  ${RBDL_DIR}/include
                           ${RBDL_DIR}/addons
                           ${RBDL_DIR}/build/include)
    find_library( RBDL_LIBRARIES NAMES rbdl PATHS ${RBDL_DIR}/build required)
    find_library( RBDL_URDF_LIBRARIES NAMES rbdl_urdfreader PATHS ${RBDL_DIR}/build/addons/urdfreader )
endif(TACO_USE_RBDL)

# ROS
if (TACO_USE_ROS)
    find_package(           roscpp REQUIRED)
    find_package(           std_msgs REQUIRED)
endif(TACO_USE_ROS)

# SHARED MEMORY
if (TACO_USE_SHM)
    find_package(           Boost 1.36.0 REQUIRED)
    set(TACO_SHM_LIBRARIES  -lrt)
endif(TACO_USE_SHM)

# GAZEBO
if (TACO_USE_GAZEBO)
    find_package(           gazebo REQUIRED)
    list(APPEND             CMAKE_CXX_FLAGS "${GAZEBO_CXX_FLAGS}")
    file(GLOB TACO_GAZEBO_LIBRARIES "${TACO_DIR}/include/taco/gazebo/lib/*.so")
endif(TACO_USE_GAZEBO)

# create headers list
set(TACO_INCLUDE_DIRS   ${TACO_INCLUDE_DIR}
                        ${EIGEN3_INCLUDE_DIR}
                        ${QUADPROG_INCLUDE_DIR}
                        ${RBDL_INCLUDE_DIRS}
                        ${GAZEBO_INCLUDE_DIRS}
                        ${Boost_INCLUDE_DIRS}
                        ${roscpp_INCLUDE_DIRS}
                        ${std_msgs_INCLUDE_DIRS}
)

# create libraries list
set(TACO_LIBRARIES      ${TACO_LIBRARY}
                        ${RBDL_LIBRARIES}
                        ${RBDL_URDF_LIBRARIES}
                        ${GAZEBO_LIBRARY_DIRS}
                        ${TACO_GAZEBO_LIBRARIES}
                        ${Boost_LIBRARIES}
                        ${TACO_SHM_LIBRARIES}
                        ${roscpp_LIBRARIES}
)

#message("TACO HEADERS:\n" ${TACO_INCLUDE_DIRS})
#message("TACO LIBS:\n" ${TACO_LIBRARIES})
