/** @example _08_rcm_sim.cc

Teleoperation with Share Memory Plugin
======================================

This example shows how to control the robot using teleoperation.
It is similar to the @ref _06_arm_opspace_sim.cc example.
However, here we use shared memory to share joint states, joint commands, and the force sensor
between the Gazebo simulation and the controller, which are running as separate processes. This is the preferred framework.
Shared memory allows modularity, has minimal delay, and allows for real-time control.
However, this does not guarantees the controller is called each time step, although it should be quite consistent.
It will also publish ROS messages for joint states and the force sensor

To control the robot's end effector position, use either the
@ref _10_keyboard_teleoperation.cc or @ref _11_haptic_teleoperation.cc example.

To publish the shared memory to ros, and plot the joint states, use @ref _12_shm_ros_publisher.cc .

See also: @ref md_examples_EXAMPLES.
*/

#include <taco/model/models.h>
#include <taco/controller/ControlLoopTimer.h>
#include <taco/controller/fixed/ArmController.h>
#include <taco/dynamics/RBDLDynamics.h>
#include <taco/comm/shm/SharedMemory.h>
#include <taco/comm/shm/JointState.h>
#include <taco/comm/shm/JointCommand.h>
#include <taco/comm/shm/ForceSensor.h>
#include <taco/comm/copy.h>
#include <taco/comm/topics.h>
#include <taco/primitive/teleoperation/robot/TeleRobotPrimitive.h>

#include <string>
#include <iostream>


const std::string robot_name = "iiwa14";
const std::string tool_name = "link_7";
const std::string force_sensor_name = "link_7";


// some functions to exit while loop
volatile bool running_ = false;
void stop(int){running_ = false;}


int main(int argc, char* argv[])
{
	std::cout << "hello world" << std::endl;

    //  Declare the dynamics engine and import robot model
    taco::RBDLDynamics dynamics;
    dynamics.importRobot(taco::model::FILEPATH+"/"+taco::model::IIWA14+".urdf", true);


    // Declare the controller, and operational space tasks
    taco::fixed::TaskController controller;
    taco::fixed::PositionTask rcm_task;
    taco::fixed::PositionTask pos_task;
    taco::fixed::JointsTask joint_task;

    // Add the dynamics engine and tasks to the controller
    // Position task is highest priority
    controller.setDynamics(&dynamics);
    controller.addTask(&rcm_task,0);    
    controller.addTask(&pos_task,1);
    controller.addTask(&joint_task,2);

    // Set parameters for position task
    // Control the remote center of motion (link 6)
    Eigen::Vector3d rcm_global_pos;
    Eigen::Vector3d rcm_local_pos(0,0,0.15);
    rcm_task.setOperationalPoint("link_7",rcm_local_pos);
    rcm_task.Kp_ = 5.0;
    rcm_task.Kv_ = 2*sqrt(rcm_task.Kp_);
    rcm_task.setEnabled(false);

    // Set parameters for position task
    // Control the end-effector (link 6)
    Eigen::Vector3d tool_tip_local_pos(0,0,0.3);
    pos_task.setOperationalPoint("link_7",tool_tip_local_pos);
    pos_task.Kp_ = 5.0;
    pos_task.Kv_ = 2.0*sqrt(pos_task.Kp_);
    pos_task.setEnabled(false);

    // Set joint task parameters
    // Add null space joint damping
    joint_task.Kp_.setConstant(20.0);
    joint_task.Kv_.setConstant(2.0*sqrt(20.0));

    // Enable controller
    controller.setGravityCompensation(true);
    controller.setEnabled(true);

    std::cout << "compute torque to start" << std::endl;

    // Compute the controller torques
    controller.computeTorque();
    controller.printInfo();

    std::cout << "compute torque end" << std::endl;



    bool moving_to_start = true;

    // shared memory to communicate between controller
    // and the robot hardware or simulator
    taco::shm::SharedMemory<taco::shm::JointState<7> > shm_joint_state;
    taco::shm::JointState<7>* joint_state = NULL;

    taco::shm::SharedMemory<taco::shm::JointCommand<7> > shm_joint_command;
    taco::shm::JointCommand<7>* joint_command = NULL;

    // shared memory to read the force sensor from robot hardware or simulator
    taco::shm::SharedMemory<taco::shm::ForceSensor > shm_force_sensor;
    taco::shm::ForceSensor* force_sensor = NULL;

    // create a primitive for teleoperation
    taco::TeleRobotPrimitive teleoperation(robot_name,tool_name);

    // create a loop timer
    double control_freq = 1000;
    taco::ControlLoopTimer timer;
    timer.setLoopFrequency(control_freq);   // 1 KHz
    timer.setThreadHighPriority();  // make timing more accurate. requires running executable as sudo.
    timer.setCtrlCHandler(stop);    // exit while loop on ctrl-c
    timer.initializeTimer(1000000); // 1 ms pause before starting loop

    // run the servo loop
    std::cout << "Waiting for robot to create joint state shared memory\n";
    running_ = true;
    while(running_)
    {
        // wait for next scheduled loop
        timer.waitForNextLoop();        

        // open shared memory
        if (joint_state==NULL){
            joint_state = shm_joint_state.open(robot_name+":"+taco::topic::JOINT_STATE);
            if (joint_state==NULL){ continue; }
            std::cout << "Opened joint state shared memory\n";
            if (joint_state->dof != controller.dynamics()->DOF()){
                throw std::runtime_error("Joint state shared memory DOF mismatch");
            }
        }

        if (joint_command==NULL){
            joint_command = shm_joint_command.open(robot_name+":"+taco::topic::JOINT_COMMAND);
            if (joint_command==NULL){continue;}
            std::cout << "Opened joint command shared memory\n";
            if (joint_state->dof != controller.dynamics()->DOF()){
                throw std::runtime_error("Joint command shared memory DOF mismatch");
            }
        }

        if (force_sensor==NULL){
            force_sensor = shm_force_sensor.open(robot_name+":"+taco::topic::FORCE_SENSOR+":"+force_sensor_name);
            if (force_sensor==NULL){ continue; }
            std::cout << "Opened force sensor shared memory\n";
        }

        // check if shared memory initialized or has closed
        if (!joint_state->sync.initialized){continue;}
        if (joint_state->sync.removed){
            std::cout << "Joint state shared memory has closed.\n";
            shm_joint_state.remove();
            joint_state = NULL;
            continue;
        }

        if (!joint_command->sync.initialized){continue;}
        if (joint_command->sync.removed){
            std::cout << "Joint command shared memory has closed.\n";
            shm_joint_command.remove();
            joint_command = NULL;
            continue;
        }

        if (!force_sensor->sync.initialized){continue;}
        if (force_sensor->sync.removed){
            std::cout << "Force sensor shared memory has closed.\n";
            shm_force_sensor.remove();
            force_sensor = NULL;
            continue;
        }

        // copy the joint state from shared memory to the dynamics
        {
            size_t dof = controller.dynamics()->DOF();
            TACO_SHM_SCOPED_LOCK(joint_state->sync.mutex);
            for (int i=0; i<dof; ++i){
                std::string joint_name;
                taco::copy(joint_state->joint_names[i], joint_name);
                controller.dynamics_->setq(joint_name, joint_state->position[i]);
                controller.dynamics_->setdq(joint_name, joint_state->velocity[i]);
                controller.dynamics_->setddq(joint_name, joint_state->acceleration[i]);
            }
        }

        if (moving_to_start) {

            // desired initial pose
            Eigen::VectorXd init_joint_pose(controller.dynamics()->DOF());
            init_joint_pose << 0,0.4,0,-1.5,0,1.1,0;

            // move to initial pose
            double trajectory_time = 5; // seconds
            double move_fraction = controller.updateCount()/(trajectory_time*control_freq);
            joint_task.desired_pos_ += init_joint_pose/(trajectory_time*control_freq);

            if (move_fraction >= 1){

                rcm_global_pos        = rcm_task.position();
                rcm_task.desired_pos_ = rcm_task.position();
                rcm_task.setEnabled(true);

                pos_task.desired_pos_ = pos_task.position();
                pos_task.setEnabled(true);

                std::cout << joint_task.position() << std::endl;
                std::cout << rcm_global_pos << std::endl;
                std::cout << pos_task.desired_pos_ << std::endl;

                moving_to_start=false;
            }
        }
        else {
            // update controller goal positions from tool command
            ros::spinOnce();
            if (teleoperation.toolCommandUpdated())
            {
                if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_POSITION){
                   pos_task.control_type_ = pos_task.CONTROL_POS;
                   taco::copy(teleoperation.tool_command_.linear_desired_pos,
                                                  pos_task.desired_pos_);
                }
                else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_VELOCITY){
                   pos_task.control_type_ = pos_task.CONTROL_VEL;
                   taco::copy(teleoperation.tool_command_.linear_desired_vel,
                                                  pos_task.desired_vel_);
                }
                else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_ACCELERATION){
                   pos_task.control_type_ = pos_task.CONTROL_ACC;
                   taco::copy(teleoperation.tool_command_.linear_desired_acc,
                                                  pos_task.desired_acc_);
                }
                else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_FORCE){
                   pos_task.control_type_ = pos_task.CONTROL_FORCE;
                   taco::copy(teleoperation.tool_command_.linear_desired_force,
                                                  pos_task.desired_force_);
                }
            }

            // update the rcm
            double dist_rcm_to_tool_tip = (pos_task.desired_pos_ - rcm_global_pos).norm();
            rcm_local_pos = tool_tip_local_pos;
            rcm_local_pos(2) -= dist_rcm_to_tool_tip;
            rcm_task.setOperationalPoint("link_7",rcm_local_pos);
        }


        // compute the next control torque
        controller.computeTorque();

        // copy torque to shared memory
        {
            size_t dof = controller.dynamics()->DOF();
            TACO_SHM_SCOPED_LOCK(joint_command->sync.mutex);
            joint_command->header.seq += 1;
            joint_command->control_type = joint_command->CONTROL_FORCE;
            for (int i=0; i<dof; ++i){
                std::string joint_name;
                taco::copy(joint_command->joint_names[i], joint_name);
                int joint_id = controller.dynamics_->jointID(joint_name);
                joint_command->desired_force[i] = controller.F_(joint_id);
            }
        }

        // update tool position for teleoperation
        taco::copy(pos_task.position(),
                                       teleoperation.tool_state_.linear_position);
        taco::copy(pos_task.velocity(),
                                       teleoperation.tool_state_.linear_velocity);
        taco::copy(pos_task.appliedForce(),
                                       teleoperation.tool_state_.linear_force);

        // publish new tool position
        teleoperation.sendToolState();

        // print out controller info every 1000 updates
        if (controller.updateCount()%1000==1){
            //controller.printInfo();
        }
    }

    double end_time = timer.elapsedTime();
    std::cout << "\n";
    std::cout << "Loop run time  : " << end_time << " seconds\n";
    std::cout << "Loop updates   : " << timer.elapsedCycles() << "\n";
    std::cout << "Loop frequency : " << timer.elapsedCycles()/end_time << "Hz\n";

    return 0;
}


