/** @example _05_arm_plugin_sim.cc

Teleoperation with Gazebo Plugin
================================

This example shows how to control the robot using teleoperation.
It is an extension of the @ref _03_gazebo_cpp_sim.cc example, using a prebuilt controller.
This example uses the teleoperation primitive, which uses ROS for message passing.
Before using this example, we must build the ros messages and create a symbolic link to them.
The "taco/build_all.sh" script will do this automatically if ROS is enabled.

To control the robot's end effector position, use either the
@ref _10_keyboard_teleoperation.cc or @ref _11_haptic_teleoperation.cc example.

See also: @ref md_examples_EXAMPLES.

\htmlonly
<html>
 <body>
  <iframe width="420" height="315" src="https://www.youtube.com/embed/ST6_1RJc5sY" frameborder="0" allowfullscreen></iframe>
 </body>
</html>
\endhtmlonly
*/

#include <taco/gazebo/ForceControlCppPlugin.h>
#include <taco/dynamics/RBDLDynamics.h>
#include <taco/controller/fixed/ArmController.h>
#include <taco/model/models.h>
#include <taco/primitive/teleoperation/robot/TeleRobotPrimitive.h>
#include <taco/comm/copy.h>

#include <Eigen/Dense>

#include <string>

namespace gazebo{

class ControlLoop : public ForceControlCppPlugin
{
public:

    ControlLoop(){}
    virtual ~ControlLoop(){
        delete controller;
        delete teleoperation;
    }

    /** called when model is loaded */
    virtual void userLoad()
    {
        // Find the model urdf, to initialize the controller->dynamics_
        // HARDCODED BECAUSE WE NEED THE PATH TO THE MODEL
        std::string local_path;
        if (model_->GetName()=="iiwa7"){
            local_path = taco::model::IIWA7;
        } else if (model_->GetName()=="iiwa14"){
            local_path = taco::model::IIWA14;
        } else {
            local_path = model_->GetName()+"/"+model_->GetName();
        }
        std::string model_path = taco::model::FILEPATH+"/"+local_path+".urdf";
        std::cout << "Loading model from [" << model_path << "]." << std::endl;

        // create controller-> using prexisting arm controller->
        controller = new taco::fixed::ArmController<taco::RBDLDynamics>(model_path,
                                                                        "link_7",
                                                                        Eigen::Vector3d(0,0,0));

        // initialize goal position to current position
        controller->position_task.desired_pos_ = controller->position_task.position(true);
        controller->orientation_task.desired_pos_ = controller->orientation_task.position(true);

        controller->setEnabled(true);

        // create teleoperation primitive
        teleoperation = new taco::TeleRobotPrimitive(model_->GetName(), "link_7");
    }

    /** called each gazebo update */
    virtual void userUpdate(const common::UpdateInfo &)
    {
        // update the joint positions and velocities
        int dof = controller->dynamics_->DOF();
        for (int i=0; i<controller->dynamics_->DOF(); ++i)
        {
            // get the joint name, to lookup value in gazebo dynamic simulation.
            // NOTE: gazebo uses lowercase version of joint names.
            std::string joint_name = controller->dynamics_->jointName(i);
            double joint_position = this->jointPosition(joint_name);
            double joint_velocity = this->jointVelocity(joint_name);

            controller->dynamics_->q_[i]  = joint_position;
            controller->dynamics_->dq_[i] = joint_velocity;
        }
        controller->dynamics_->updateDynamics();

        // move to an initial new position
        Eigen::Vector3d new_goal_pos(0.6,0.0,-0.8);
        double new_goal_angle = M_PI;
        double trajectory_time = 3000;
        if (moving_to_start){
            double move_fraction = controller->updateCount()/trajectory_time;
            controller->position_task.desired_pos_ += new_goal_pos/trajectory_time;
            double theta = move_fraction*(new_goal_angle);
            controller->orientation_task.desired_pos_ << cos(theta), 0, sin(theta),
                                                            0     , 1,     0     ,
                                                       -sin(theta), 0, cos(theta);
            if (move_fraction >= 1){moving_to_start=false;}
        }

        // move to the new teleoperated goal position
        else
        {
            ros::spinOnce();
            if (teleoperation->toolCommandUpdated())
            {
                if (teleoperation->tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_POSITION){
                   controller->position_task.control_type_ = controller->position_task.CONTROL_POS;
                   taco::copy(teleoperation->tool_command_.linear_desired_pos,
                                                  controller->position_task.desired_pos_);
                }
                else if (teleoperation->tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_VELOCITY){
                   controller->position_task.control_type_ = controller->position_task.CONTROL_VEL;
                   taco::copy(teleoperation->tool_command_.linear_desired_vel,
                                                  controller->position_task.desired_vel_);
                }
                else if (teleoperation->tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_ACCELERATION){
                   controller->position_task.control_type_ = controller->position_task.CONTROL_ACC;
                   taco::copy(teleoperation->tool_command_.linear_desired_acc,
                                                  controller->position_task.desired_acc_);
                }
                else if (teleoperation->tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_FORCE){
                   controller->position_task.control_type_ = controller->position_task.CONTROL_FORCE;
                   taco::copy(teleoperation->tool_command_.linear_desired_force,
                                                  controller->position_task.desired_force_);
                }

                if (teleoperation->tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_POSITION){
                   controller->orientation_task.control_type_ = controller->orientation_task.CONTROL_POS;
                   taco::copy(teleoperation->tool_command_.angular_desired_pos,
                                                  controller->orientation_task.desired_pos_);
                }
                else if (teleoperation->tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_VELOCITY){
                   controller->orientation_task.control_type_ = controller->orientation_task.CONTROL_VEL;
                   taco::copy(teleoperation->tool_command_.angular_desired_vel,
                                                  controller->orientation_task.desired_vel_);
                }
                else if (teleoperation->tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_ACCELERATION){
                   controller->orientation_task.control_type_ = controller->orientation_task.CONTROL_ACC;
                   taco::copy(teleoperation->tool_command_.angular_desired_acc,
                                                  controller->orientation_task.desired_acc_);
                }
                else if (teleoperation->tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_FORCE){
                   controller->orientation_task.control_type_ = controller->orientation_task.CONTROL_FORCE;
                   taco::copy(teleoperation->tool_command_.angular_desired_force,
                                                  controller->orientation_task.desired_force_);
                }
            }
        }

        // compute control torques
        controller->computeTorque();

        // set torques on simulated robot
        for (int i=0; i<controller->dynamics_->DOF(); ++i)
        {
            std::string joint_name = controller->dynamics_->jointName(i);
            setJointForce(joint_name, controller->F_(i));
        }

        // update tool position for teleoperation
        taco::copy(controller->position_task.position(),
                                       teleoperation->tool_state_.linear_position);
        taco::copy(controller->position_task.velocity(),
                                       teleoperation->tool_state_.linear_velocity);
        taco::copy(controller->position_task.appliedForce(),
                                       teleoperation->tool_state_.linear_force);
        taco::copy(controller->orientation_task.position(),
                                       teleoperation->tool_state_.angular_position);
        taco::copy(controller->orientation_task.velocity(),
                                       teleoperation->tool_state_.angular_velocity);
        taco::copy(controller->orientation_task.appliedForce(),
                                       teleoperation->tool_state_.angular_force);

        // publish new tool position
        teleoperation->sendToolState();

        // print info
        if (controller->updateCount()%1000==0){
            controller->printInfo(); //print info every 1000 updates
        }
    }

    taco::fixed::ArmController<taco::RBDLDynamics>* controller = NULL;

    taco::TeleRobotPrimitive* teleoperation = NULL;

    bool moving_to_start = true;

};

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ControlLoop)

}
