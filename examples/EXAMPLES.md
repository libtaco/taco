Summary of Examples
===================

These examples show usage of the task-space control library. A bash script is provided to build all the example.

     cd taco/examples
     sh build_examples.sh


| Example                              | Dependency   | Description                                                                     |
| ------------------------------------ | ------------ | ------------------------------------------------------------------------------- |
| @ref _01_operational_space.cc        | None         | create an operational space controller from scratch.                            |
| @ref _02_fixed_controller.cc         | None         | use the taco fixed base operational space controller.                           |
| @ref _03_gazebo_cpp_sim.cc           | Gazebo       | control of robot in gazebo, creating controller in a gazebo c++ plugin.         |
| @ref _04_opspace_vs_qp_controller.cc | None         | compare results from the fixed and qp controller                                |
| @ref _05_arm_plugin_sim.cc           | Gazebo, ROS  | teleoperation of op space controller, with controller called in gazebo plugin.  |
| @ref _06_arm_opspace_sim.cc          | Gazebo, ROS  | teleoperation of op space controller, with gazebo<->shared memory<->controller. |
| @ref _07_arm_qp_sim.cc               | Gazebo, ROS  | teleoperation of qp controller, with gazebo<->shared memory<->controller.       |
| @ref _08_rcm_sim.cc                  | Gazebo, ROS  | like arm_opspace_sim, but with a remote center of motion null space.            |
| @ref _09_quadruped_sim.cc            | Gazebo, ROS  | like arm_opspace_sim, but controlling a quadraped with floating base dynamics . |
| @ref _10_keyboard_teleoperation.cc   | ncurses, ROS | control of a teleoperation example using keyboard input (asdqwe).               |
| @ref _11_haptic_teleoperation.cc     | Chai3d, ROS  | control of a teleoperation example using haptic device. Uses force sensor.      |
| @ref _12_shm_ros_publisher.cc        | ROS          | reads the shared memory, and publishes to ros.                                  |
| @ref _13_contact_stability.cc        | Gazebo       | plot the contact forces for a box and mesh sitting on the ground.               |
| @ref _14_robot_driver.cc             | Kuka Driver  | control a real Kuka LBR iiwa using the kuka driver.                             |

