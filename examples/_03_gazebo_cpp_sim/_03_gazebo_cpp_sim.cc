/** @example _03_gazebo_cpp_sim.cc

Gazebo Plugin Example
=====================

This example shows a simulation of the Kuka IIWA.
This example creates an operational space controller directly inside a Gazebo model plugin.
We then compile this plugin into a library, and add it to Gazebo's simulation.world launch file.
The bash script "run_simulation.sh" exports the library path (so Gazebo can find it) and starts Gazebo.

Each simulation time step, this plugin will perform the following:

1. Get the joint states from the Gazebo model, and update the controller's dynamic engine.
2. Calculate the new torques from the controller
3. Set the torques for the Gazebo model.

The controller is initialized and updated similar to the simple_sim example. However, this arm controller has three tasks in the following priority (highest to lowest):

1. End-effector Orientation
2. End-effector Position
3. Joint damping.

See also: @ref md_examples_EXAMPLES.

\htmlonly
<html>
 <body>
  <iframe width="420" height="315" src="https://www.youtube.com/embed/_nSPFeNHdJo" frameborder="0" allowfullscreen></iframe>
 </body>
</html>
\endhtmlonly
*/

#include <taco/gazebo/ForceControlCppPlugin.h>
#include <taco/controller/fixed/FixedController.h>
#include <taco/dynamics/RBDLDynamics.h>
#include <taco/model/models.h>

#include <Eigen/Dense>

#include <string>

namespace gazebo{

class ControlLoop : public ForceControlCppPlugin
{
public:

    ControlLoop(){}
    virtual ~ControlLoop(){}

    /** called when model is loaded */
    virtual void userLoad()
    {
        // Find the model urdf, to initialize the controller->dynamics_
        // HARDCODED BECAUSE WE NEED THE PATH TO THE MODEL
        std::string local_path;
        if (model_->GetName()=="iiwa7"){
            local_path = taco::model::IIWA7;
        } else if (model_->GetName()=="iiwa14"){
            local_path = taco::model::IIWA14;
        } else {
            local_path = model_->GetName()+"/"+model_->GetName();
        }
        std::string model_path = taco::model::FILEPATH+"/"+local_path+".urdf";
        std::cout << "Loading model from [" << model_path << "]." << std::endl;

        // import robot model to create kinematic/dynamic model
        dynamics.importRobot(model_path, true);

        // create task space controller
        controller.setDynamics(&dynamics);
        controller.addTask(&ori_task,0);
        controller.addTask(&pos_task,1);
        controller.addTask(&joint_task,2);

        // end effector position control
        // desired position = current position
        pos_task.setOperationalPoint("link_7");
        initial_position = pos_task.position(true);
        pos_task.desired_pos_ = initial_position;
        pos_task.Kp_ = 5.0;
        pos_task.Kv_ = 2*sqrt(pos_task.Kp_); //critical damping

        // end effector orientation control                 
        ori_task.setOperationalBody("link_7");
        ori_task.desired_pos_ = ori_task.position(true);
        double theta = M_PI/2;
        ori_task.desired_pos_ << cos(theta), 0, sin(theta),
                                     0     , 1,     0     ,
                                -sin(theta), 0, cos(theta);

        ori_task.Kp_ = 5.0;
        ori_task.Kv_ = 2*sqrt(ori_task.Kv_); //critical damping

        // null space joint damping
        joint_task.Kp_.setZero();
        joint_task.Kv_.setConstant(2*sqrt(5));

        controller.setEnabled(true);
    }

    /** called each gazebo update */
    virtual void userUpdate(const common::UpdateInfo &)
    {
        // update the joint positions and velocities
        int dof = dynamics.DOF();
        for (int i=0; i<dynamics.DOF(); ++i)
        {
            // get the joint name
            std::string joint_name = dynamics.jointName(i);

            // get the joint position and velocity by name from gazebo
            // and copy to the dynamics engine
            dynamics.q_[i]  = this->jointPosition(joint_name);
            dynamics.dq_[i] = this->jointVelocity(joint_name);
        }
        dynamics.updateDynamics();

        // make a circle with the end effector
        pos_task.desired_pos_(0) = initial_position(0) + 0.2*sin(2.0*M_PI*0.1*0.001*controller.updateCount());
        pos_task.desired_pos_(1) = initial_position(1) + 0.2*cos(2.0*M_PI*0.1*0.001*controller.updateCount());
        pos_task.desired_pos_(2) = initial_position(2) - 0.3;

        // compute control torques
        controller.computeTorque();
        if (controller.updateCount()%1000==0){
            controller.printInfo(); //print info every 1000 updates
        }

        // set torques on simulated robot
        for (int i=0; i<dynamics.DOF(); ++i)
        {
            this->setJointForce(dynamics.jointName(i), controller.F_(i));
        }
    }


    taco::RBDLDynamics dynamics;
    taco::fixed::TaskController controller;
    taco::fixed::PositionTask pos_task;
    taco::fixed::OrientationTask ori_task;
    taco::fixed::JointsTask joint_task;

    Eigen::Vector3d initial_position;

};

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ControlLoop)

}
