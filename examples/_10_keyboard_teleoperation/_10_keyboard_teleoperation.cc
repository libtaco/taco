/** @example _10_keyboard_teleoperation.cc

Teleoperation with Keyboard
==========================

This example shows how to control the robot using teleoperation.
Here we use a keyboard to control the robot. You can use this to control the following examples:

* * @ref _05_arm_plugin_sim.cc
* * @ref _06_arm_opspace_sim.cc
* * @ref _07_arm_qp_sim.cc
* * @ref _08_rcm_sim.cc
* * @ref _09_quadruped_sim.cc

| Direction | + key | - key |
| --------- | ----- | ----- |
|    x      |   w   |   s   |
|    y      |   a   |   d   |
|    z      |   q   |   e   |

This example has a dependency on ncurses. To installing ncurses:

     sudo apt-get install libncurses-dev

See also: @ref md_examples_EXAMPLES.
*/

#include <TeleUserPrimitive.h>
#include <taco/comm/topics.h>
#include <taco/model/models.h>

#include <string>
#include <ncurses.h>
#include <unistd.h>
#include <signal.h>
#include <sched.h>
#include <signal.h>
#include <time.h>


const std::string robot_name = "iiwa14";
const std::string tool_name = "link_7";

volatile bool running = false;

void siginthandler(int param)
{
  running = false;
}

int main ()
{
    // initialize the teleoperation
    taco::TeleUserPrimitive teleoperation(robot_name,tool_name);
    teleoperation.tool_command_.linear_control_type = taco_msgs::ToolCommand::CONTROL_POSITION;
    teleoperation.tool_command_.angular_control_type = taco_msgs::ToolCommand::CONTROL_NONE;

    // initialize ncurses for keyboard input
    signal(SIGINT, siginthandler);
    initscr();
    timeout(500); // wait 500 ms every loop for an input

    printw("Use the keyboard to move the robot end effector\n");
    printw("  (+)(-)\n");
    printw("x| w  s \n");
    printw("y| a  d \n");
    printw("z| q  e \n");
    printw("--------\n");
    printw("Waiting for connection to TeleRobotPrimitive.\n");
    refresh();
    int x1,y1,x2,y2,x3,y3;

    // set up control loop
    double control_loop_frequency = 1000;
    unsigned int interval = 1000000000.0/control_loop_frequency;
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC ,&t);
    t.tv_sec++;

    bool initialized = false;
    running = true;
    while(running)
    {
        // get ros updates
        ros::spinOnce();
        bool updated = teleoperation.toolStateUpdated();

        // initialize command to initial position
        if (!initialized && updated){
            teleoperation.tool_command_.linear_desired_pos[0] = teleoperation.tool_state_.linear_position[0];
            teleoperation.tool_command_.linear_desired_pos[1] = teleoperation.tool_state_.linear_position[1];
            teleoperation.tool_command_.linear_desired_pos[2] = teleoperation.tool_state_.linear_position[2];
            initialized = true;

            printw("Initialized connection with TeleRobotPrimitive.\n");
            getyx(stdscr, y1, x1); // save current pos
            printw("Current position : %f\t%f\t%f\n",
                    teleoperation.tool_state_.linear_position[0],
                    teleoperation.tool_state_.linear_position[1],
                    teleoperation.tool_state_.linear_position[2]);
            getyx(stdscr, y2, x2); // save current pos
            printw("Desired position : %f\t%f\t%f\n>",
                    teleoperation.tool_command_.linear_desired_pos[0],
                    teleoperation.tool_command_.linear_desired_pos[1],
                    teleoperation.tool_command_.linear_desired_pos[2]);
            getyx(stdscr, y3, x3); // save current pos
            refresh();
        }

        if (initialized)
        {
            move(y1, x1); // move to begining of line
            clrtoeol();   // clear line
            printw("Current position : %f\t%f\t%f\n",
                    teleoperation.tool_state_.linear_position[0],
                    teleoperation.tool_state_.linear_position[1],
                    teleoperation.tool_state_.linear_position[2]);
            move(y3, x3);

            // get keyboard input
            int i = getch();

            // check for quit condition
            if ( i==27 ){
                printw("Esc pressed.\n");
                break;
            }

            // increment or decrement tool commanded position
            double delta = 0.02;
            if (i!=ERR){
                char c = static_cast<char>(i);
                switch (c){
                    case 'w': teleoperation.tool_command_.linear_desired_pos[0] += delta; break;
                    case 's': teleoperation.tool_command_.linear_desired_pos[0] -= delta; break;
                    case 'a': teleoperation.tool_command_.linear_desired_pos[1] += delta; break;
                    case 'd': teleoperation.tool_command_.linear_desired_pos[1] -= delta; break;
                    case 'q': teleoperation.tool_command_.linear_desired_pos[2] += delta; break;
                    case 'e': teleoperation.tool_command_.linear_desired_pos[2] -= delta; break;
                }

                move(y2, x2); // move to begining of line
                clrtoeol();   // clear line
                printw("Desired position : %f\t%f\t%f\n>",
                        teleoperation.tool_command_.linear_desired_pos[0],
                        teleoperation.tool_command_.linear_desired_pos[1],
                        teleoperation.tool_command_.linear_desired_pos[2]);
            }

            // send tool command
            teleoperation.sendToolCommand();
        }

        // calculate next shot. carry over nanoseconds into microseconds.
        t.tv_nsec += interval;
        while (t.tv_nsec >= 1000000000){
            t.tv_nsec -= 1000000000;
            t.tv_sec++;
        }
    }

    printf("exiting\n");
    endwin();
}


