/** @example _13_contact_stability.cc

Contact Stability
=================

This will print out the contact forces from the contact sensor plugin. It will also print the data to a file. A matlab script is included for plotting. Use the run bash script to launch.

     sh run_simulation.sh

Change Physics Engine
---------------------
In  "run_simulation.sh", change

     gazebo -u -e ode ./simulation.world --verbose &
to

     gazebo -u -e bullet ./simulation.world --verbose &

Change the Model
----------------
In "simulation.world", change:

     <uri>model://test_box</uri>
to

     <uri>model://test_mesh</uri>

Force and Torque Data
---------------------

![raw data from box on ground](examples/_14_contact_stability/images/box_raw.jpg)
![raw data from mesh on ground](examples/_14_contact_stability/images/mesh_raw.jpg)
![filtered data from mesh on ground](examples/_14_contact_stability/images/mesh_filtered.jpg)

See also: @ref md_examples_EXAMPLES.
**/
