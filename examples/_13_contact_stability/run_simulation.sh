ln -sf "$(pwd)/test_box/" ~/.gazebo/models/
ln -sf "$(pwd)/test_mesh/" ~/.gazebo/models/
export GAZEBO_PLUGIN_PATH=${GAZEBO_PLUGIN_PATH}:./build
gazebo -u -e bullet ./simulation.world --verbose &
