clc; clear all; close all;

%% plot the data from last run
filename = 'test_box_force_sensor_link0.txt';
%filename = 'test_mesh_force_sensor_link0.txt';

data = importdata(filename);
time = data(:,1);
force = data(:,2:4);
torque = data(:,5:7);
filtered_force = data(:,8:10);
filtered_torque = data(:,11:13);

subplot(2,1,1)
plot(time,force)
legend('x','y','z')
title(['raw: ',strrep(filename,'_','\_')])

subplot(2,1,2)
plot(time,torque)

figure
subplot(2,1,1)
plot(time,filtered_force)
legend('F_x','F_y','F_z')
title(['filtered: ',strrep(filename,'_','\_')])

subplot(2,1,2)
plot(time,filtered_torque)
legend('\tau_x','\tau_y','\tau_z')

%% plot data comparing ode and bullet for box and mesh
%{
filenames = {'box_ode_data.txt',...
    'box_bullet_data.txt',...
    'mesh_ode_data.txt',...
    'mesh_bullet_data.txt'
    };

fig1 = figure;
fig2 = figure;
for i=1:length(filenames)
    filename = filenames{i};
    data = importdata(filename);
    time = data(:,1);
    force = data(:,2:4);
    torque = data(:,5:7);
    
    figure(fig1)
    subplot(length(filenames)/2,2,i)
    plot(time,force)
    title(strrep(filename,'_','\_'))

    figure(fig2)
    subplot(length(filenames)/2,2,i)
    plot(time,torque)
    %legend('x','y','z')
    title(strrep(filename,'_','\_'))
end
%}