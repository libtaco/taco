/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_GAZEBO_JOINTFORCESENSORLOGGERPLUGIN_H_
#define TACO_GAZEBO_JOINTFORCESENSORLOGGERPLUGIN_H_

#include "../../gazebo/joint_force_sensor_shm_plugin/JointForceSensorShmPlugin.h"

#include <gazebo/common/Plugin.hh>


namespace gazebo
{
  /** \brief A plugin for a contact sensor. Publishes the forces to shared memory.
   *
   * Write force data to shared memory, at name "robot_name/sensor_name", where sensor_name is given by the SDF
   *
   * \ingroup gazebo
   */
  class GAZEBO_VISIBLE JointForceSensorLoggerPlugin : public JointForceSensorShmPlugin
  {

public:

    JointForceSensorLoggerPlugin() {
        startWriteToFile();
    }

    virtual ~JointForceSensorLoggerPlugin() {}

    virtual void userCallback()
    {
        //PrintContacts();

        double t = time_.sec + 0.000000001*time_.nsec;
        if (t >= seconds_to_record){
            this->stopWriteToFile();
        }
    }

    const double seconds_to_record = 5.0;
  };

  GZ_REGISTER_SENSOR_PLUGIN(JointForceSensorLoggerPlugin)

}




#endif
