/** @example _04_op_vs_qp_controller.cc

QP v Operational Space Controller
=================================

This example creates an operational space controller, and the equivalent quadratic programming controller.
It computes the torques for comparison. The results are similar but not identitical.
This is partly because the QP controller using a regularization approximation for stability.
The cost is a weighted sum of the task error, and the regularization term.
A smaller weight gives a better approximation of operational space.

\f$\ddot{x}_{desired} - J \ddot{q} + weight * \ddot{q} M \ddot{q}\f$

\note QuadProgRT is under the GNU General Public License

See also: @ref md_examples_EXAMPLES.
*/

#include <iostream>
#include <taco/dynamics/RBDLDynamics.h>
#include <taco/controller/fixed/FixedController.h>
#include <taco/controller/qp/QPController.h>
#include <taco/optimizer/QuadProgRT.h>
#include <taco/model/models.h>


int main(int argc, char* argv[])
{
	std::cout << "hello world" << std::endl;

	// import robot model to create kinematic/dynamic model
    taco::RBDLDynamics dynamics;
    dynamics.importRobot(taco::model::FILEPATH+"/"+taco::model::IIWA14+".urdf", false);

    // set joint positions and velocities
    dynamics.q_ << 0,M_PI/6,0,M_PI/6,0,M_PI/6,0;
    dynamics.dq_ << 0,0,0,0,0,0,0;
    dynamics.updateDynamics();

    // ====== FIXED (OP SPACE) CONTROLLER INITIALIZATION =====

    taco::fixed::TaskController op_controller;
    taco::fixed::PositionTask op_pos_task;

    // create task space controller
    op_controller.setDynamics(&dynamics);
    op_controller.addTask((&op_pos_task),0);

    // end effector position control
    // desired position = current position + 20 cm each direction
    op_pos_task.setOperationalPoint("link_7");
    op_pos_task.desired_pos_ = op_pos_task.position(true) + Eigen::Vector3d(0.2,0.0,0.0);
    op_pos_task.Kp_ = 1.0;
    op_pos_task.Kv_ = 0.5;

    op_controller.setGravityCompensation(false);
    op_controller.setCoriolisCompensation(false);
    op_controller.setEnabled(true);

    // ====== QP CONTROLLER INITIALIZATION =====

    taco::qp::TaskController qp_controller;
    taco::qp::PositionTask qp_pos_task;

    // create task space controller
    qp_controller.setDynamics(&dynamics);
    qp_controller.setQPSolver(new taco::QuadProgRT());
    qp_controller.addTask((&qp_pos_task),0);

    // end effector position control
    // desired position = current position + 20 cm each direction
    qp_pos_task.setOperationalPoint("link_7");
    qp_pos_task.desired_pos_ = qp_pos_task.position(true) + Eigen::Vector3d(0.2,0.0,0.0);
    qp_pos_task.Kp_ = 1.0;
    qp_pos_task.Kv_ = 0.5;

    qp_controller.setGravityCompensation(false);
    qp_controller.setCoriolisCompensation(false);
    qp_controller.setEnabled(true);

    // ====== FIXED (OP SPACE) CONTROLLER RESULTS =====

    std::cout << "\n";
    std::cout << "============================\n";
    std::cout << "OPERATIONAL SPACE CONTROLLER\n";
    std::cout << "============================" << std::endl;

	// compute controller
    op_controller.computeTorque();
    op_controller.printInfo();

    // ====== QP CONTROLLER RESULTS =====

    std::cout << "\n";
    std::cout << "================================\n";
    std::cout << "QUADRATIC PROGRAMMING CONTROLLER\n";
    std::cout << "================================" << std::endl;

    // compute controller
    qp_controller.computeTorque();
    qp_controller.printInfo();

    std::cout << "finished" << std::endl;
    return 0;

}
