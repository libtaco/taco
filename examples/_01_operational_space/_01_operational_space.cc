/** @example _01_operational_space.cc

Operational Space Example
=========================

This example creates an operational space task and controller from scratch.
It demonstrates how to use the dynamics engine and the base class taco::Controller.
The next example, @ref _02_fixed_controller.cc demonstrates the pre-made operational space controller.

See also: @ref md_examples_EXAMPLES.
*/

#include <iostream>
#include <taco/dynamics/RBDLDynamics.h>
#include <taco/controller/Controller.h>
#include <taco/controller/math.h>
#include <taco/model/models.h>


// Our operational space controller
// containing two tasks: position and joint damping
class MyController : public taco::Controller
{
public:
    MyController(){}
    virtual ~MyController(){}

    virtual const Eigen::VectorXd& computeTorque()
    {
        // update the dynamics
        dynamics_->updateDynamics();
        int dof = dynamics_->DOF();

        // Set joint forces to zero.
        // Size was initialized fo us, when we set dynamics engine
        F_.setZero();

        // ===================================
        //  Position Task
        // ===================================

        // PD force on our operational point
        Eigen::Vector3d curr_pos, curr_vel;
        dynamics_->position(curr_pos, link_name, pos_in_link);
        dynamics_->velocity(curr_vel, link_name, pos_in_link);
        Eigen::Vector3d force = Kp*(desired_pos - curr_pos) - Kd*curr_vel;

        // linear velocity jacobian
        Eigen::MatrixXd J;
        dynamics_->Jv(J, link_name, pos_in_link);

        // joint space mass matrix
        Eigen::MatrixXd M(dof,dof);
        M.setZero();
        dynamics_->massMatrix(M);

        // operational space mass matrix
        Eigen::MatrixXd lambda = taco::pseudoInv(J * M.inverse() * J.transpose());

        // scale the force by the operational space matrix
        // so our PID behaves like acting on a unit mass
        Eigen::Vector3d scaled_force = lambda*force;

        // calculate our joint forces from our operational space force
        Eigen::VectorXd F_position_task = J.transpose()*scaled_force;
        F_ += F_position_task;

        // Calculate the null space of the task
        // using the dynamically consistent generalized inverse of jacobian
        Eigen::MatrixXd J_inv = M.inverse() * J.transpose() * lambda;
        Eigen::MatrixXd N = Eigen::MatrixXd::Identity(dof,dof) - J_inv * J;

        std::cout << "==============================\n";
        std::cout << "Position task\n";
        std::cout << "goal   : " << desired_pos.transpose() << "\n";
        std::cout << "pos    : " << curr_pos.transpose() << "\n";
        std::cout << "vel    : " << curr_vel.transpose() << "\n";
        std::cout << "force  : " << scaled_force.transpose() << "\n";
        std::cout << "torque : " << F_position_task.transpose() << "\n";

        // ===================================
        //  Joint Damping Task
        // ===================================

        // damping force
        Eigen::VectorXd damping_force = - damping*dynamics_->dq_;

        // scale by mass matrix
        // in this task, the operational space matrix equals the joint space mass matrix
        Eigen::VectorXd scaled_damping_force = M*damping_force;

        // add force in the null space of the higher priority position task
        Eigen::VectorXd F_joint_task = N.transpose()*scaled_damping_force;
        F_ += F_joint_task;

        std::cout << "==============================\n";
        std::cout << "Joint damping task\n";
        std::cout << "torque:\n" << F_joint_task.transpose() << "\n";

        return F_;
    }

    std::string link_name;
    Eigen::Vector3d pos_in_link;
    Eigen::Vector3d desired_pos;
    double Kp = 5;
    double Kd = 2*sqrt(Kp);
    double damping = 1.0;
};




int main(int argc, char* argv[])
{
	std::cout << "hello world" << std::endl;

    //  Create the dynamics engine. RBDLDynamics is currently the only one offered.
    taco::RBDLDynamics dynamics;

    // Import robot model
    // TACO_MODEL_PATH is an absolute path set by the TaCo build_all.sh script.
    dynamics.importRobot(taco::model::FILEPATH+"/"+taco::model::IIWA14+".urdf", true);
    //dynamics.q_  << 0,M_PI/6,0,M_PI/6,0,M_PI/6,0;
    dynamics.q_  << 0,0,0,0,0,0,0;
    dynamics.dq_ << 0,0,0,0,0,0,0;
    dynamics.updateDynamics();

    Eigen::MatrixXd Jv, Jw;
    dynamics.Jv(Jv, "link_7", Eigen::Vector3d(0,0,0));
    dynamics.Jw(Jw, "link_7");

    // Create the controller;
    MyController controller;
    controller.link_name = "link_7";
    controller.pos_in_link = Eigen::Vector3d(0,0,0);

    Eigen::Vector3d curr_pos;
    dynamics.position(curr_pos, "link_7", Eigen::Vector3d(0,0,0));
    controller.desired_pos = curr_pos + Eigen::Vector3d(0.2, 0.2, -0.2);

    // Add the dynamics engine to the controller
    controller.setDynamics(&dynamics);

    // Compute the torques
    Eigen::MatrixXd F = controller.computeTorque();

    std::cout << "==================\n";
    std::cout << "Total joint force:\n" << F.transpose() << std::endl;

    return 0;

}
