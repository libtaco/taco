/** @example _06_arm_opspace_sim.cc

Teleoperation with Share Memory Plugin
======================================

This example shows how to control the robot using teleoperation.
It is similar to the @ref _05_arm_plugin_sim.cc example.
However, here we use shared memory to share joint states, joint commands, and the force sensor
between the Gazebo simulation and the controller, which are running as separate processes. This is the preferred framework.
Shared memory allows modularity, has minimal delay, and allows for real-time control.
However, this does not guarantees the controller is called each time step, although it should be quite consistent.
It will also publish ROS messages for joint states and the force sensor

To control the robot's end effector position, use either the
@ref _10_keyboard_teleoperation.cc or @ref _11_haptic_teleoperation.cc example.

To publish the shared memory to ros, and plot the joint states, use @ref _12_shm_ros_publisher.cc .

See also: @ref md_examples_EXAMPLES.
*/

#include <taco/model/models.h>
#include <taco/controller/ControlLoopTimer.h>
#include <taco/controller/fixed/ArmController.h>
#include <taco/dynamics/RBDLDynamics.h>
#include <taco/comm/shm/SharedMemory.h>
#include <taco/comm/shm/JointState.h>
#include <taco/comm/shm/JointCommand.h>
#include <taco/comm/shm/ForceSensor.h>
#include <taco/comm/copy.h>
#include <taco/comm/topics.h>
#include <taco/primitive/teleoperation/robot/TeleRobotPrimitive.h>

#include <string>
#include <iostream>


const std::string robot_name = "iiwa14";
const std::string tool_name = "link_7";
const std::string force_sensor_name = "link_7";


// some functions to exit while loop
volatile bool running_ = false;
void stop(int){running_ = false;}


int main(int argc, char* argv[])
{
	std::cout << "hello world" << std::endl;

    // create controller. using prexisting arm controller.
    taco::fixed::ArmController<taco::RBDLDynamics>controller(taco::model::FILEPATH+"/"
                                                            +taco::model::IIWA14+".urdf",
                                                             "link_7",Eigen::Vector3d(0,0,0),
                                                             "link_4",Eigen::Vector3d(0,0,0));

    bool initialized = false;
    bool moving_to_start = true;

    // shared memory to communicate between controller
    // and the robot hardware or simulator
    taco::shm::SharedMemory<taco::shm::JointState<7> > shm_joint_state;
    taco::shm::JointState<7>* joint_state = NULL;

    taco::shm::SharedMemory<taco::shm::JointCommand<7> > shm_joint_command;
    taco::shm::JointCommand<7>* joint_command = NULL;

    // shared memory to read the force sensor from robot hardware or simulator
    taco::shm::SharedMemory<taco::shm::ForceSensor > shm_force_sensor;
    taco::shm::ForceSensor* force_sensor = NULL;

    // create a primitive for teleoperation
    taco::TeleRobotPrimitive teleoperation(robot_name, tool_name);

    // create a loop timer
    double control_freq = 1000;
    taco::ControlLoopTimer timer;
    timer.setLoopFrequency(control_freq);   // 1 KHz
    timer.setThreadHighPriority();  // make timing more accurate. requires running executable as sudo.
    timer.setCtrlCHandler(stop);    // exit while loop on ctrl-c
    timer.initializeTimer(1000000); // 1 ms pause before starting loop

    // run the servo loop
    std::cout << "Waiting for robot to create joint state shared memory\n";
    running_ = true;
    while(running_)
    {
        // wait for next scheduled loop
        timer.waitForNextLoop();        

        // open shared memory
        if (joint_state==NULL){
            joint_state = shm_joint_state.open(robot_name+":"+taco::topic::JOINT_STATE);
            if (joint_state==NULL){ continue; }
            std::cout << "Opened joint state shared memory\n";
            if (joint_state->dof != controller.dynamics()->DOF()){
                throw std::runtime_error("Joint state shared memory DOF mismatch");
            }
        }

        if (joint_command==NULL){
            joint_command = shm_joint_command.open(robot_name+":"+taco::topic::JOINT_COMMAND);
            if (joint_command==NULL){continue;}
            std::cout << "Opened joint command shared memory\n";
            if (joint_state->dof != controller.dynamics()->DOF()){
                throw std::runtime_error("Joint command shared memory DOF mismatch");
            }
        }

        if (force_sensor==NULL){
            force_sensor = shm_force_sensor.open(robot_name+":"+taco::topic::FORCE_SENSOR+":"+force_sensor_name);
            if (force_sensor==NULL){ continue; }
            std::cout << "Opened force sensor shared memory\n";
        }

        // check if shared memory initialized or has closed
        if (!joint_state->sync.initialized){continue;}
        if (joint_state->sync.removed){
            std::cout << "Joint state shared memory has closed.\n";
            shm_joint_state.remove();
            joint_state = NULL;
            continue;
        }

        if (!joint_command->sync.initialized){continue;}
        if (joint_command->sync.removed){
            std::cout << "Joint command shared memory has closed.\n";
            shm_joint_command.remove();
            joint_command = NULL;
            continue;
        }

        if (!force_sensor->sync.initialized){continue;}
        if (force_sensor->sync.removed){
            std::cout << "Force sensor shared memory has closed.\n";
            shm_force_sensor.remove();
            force_sensor = NULL;
            continue;
        }

        // copy the joint state from shared memory to the dynamics
        {
            size_t dof = controller.dynamics()->DOF();
            TACO_SHM_SCOPED_LOCK(joint_state->sync.mutex);
            for (int i=0; i<dof; ++i){
                std::string joint_name;
                taco::copy(joint_state->joint_names[i], joint_name);
                controller.dynamics_->setq(joint_name, joint_state->position[i]);
                controller.dynamics_->setdq(joint_name, joint_state->velocity[i]);
                controller.dynamics_->setddq(joint_name, joint_state->acceleration[i]);
            }
        }

        // initialize goal position to current position
        if (initialized==false)
        {
            controller.dynamics_->updateDynamics();
            controller.position_task.desired_pos_ = controller.position_task.position(true);
            controller.orientation_task.desired_pos_ = controller.orientation_task.position(true);
            controller.setEnabled(true);
            initialized = true;
            std::cout << "Initialized starting position from first joint state :)\n";
        }

        if (initialized)
        {
            if (moving_to_start) {
                // move to an initial new position
                Eigen::Vector3d new_goal_pos(0.6,0.0,-0.8);
                double new_goal_angle = M_PI;
                double trajectory_time = 3; // seconds
                double move_fraction = controller.updateCount()/(trajectory_time*control_freq);
                controller.position_task.desired_pos_ += new_goal_pos/(trajectory_time*control_freq);
                double theta = move_fraction*(new_goal_angle);
                controller.orientation_task.desired_pos_ << cos(theta), 0, sin(theta),
                                                                0     , 1,     0     ,
                                                           -sin(theta), 0, cos(theta);
                if (move_fraction >= 1){moving_to_start=false;}
            }
            else {
                // update controller goal positions from tool command
                ros::spinOnce();
                if (teleoperation.toolCommandUpdated())
                {
                    if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_POSITION){
                       controller.position_task.control_type_ = controller.position_task.CONTROL_POS;
                       taco::copy(teleoperation.tool_command_.linear_desired_pos,
                                                      controller.position_task.desired_pos_);
                    }
                    else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_VELOCITY){
                       controller.position_task.control_type_ = controller.position_task.CONTROL_VEL;
                       taco::copy(teleoperation.tool_command_.linear_desired_vel,
                                                      controller.position_task.desired_vel_);
                    }
                    else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_ACCELERATION){
                       controller.position_task.control_type_ = controller.position_task.CONTROL_ACC;
                       taco::copy(teleoperation.tool_command_.linear_desired_acc,
                                                      controller.position_task.desired_acc_);
                    }
                    else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_FORCE){
                       controller.position_task.control_type_ = controller.position_task.CONTROL_FORCE;
                       taco::copy(teleoperation.tool_command_.linear_desired_force,
                                                      controller.position_task.desired_force_);
                    }

                    if (teleoperation.tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_POSITION){
                       controller.orientation_task.control_type_ = controller.orientation_task.CONTROL_POS;
                       taco::copy(teleoperation.tool_command_.angular_desired_pos,
                                                      controller.orientation_task.desired_pos_);
                    }
                    else if (teleoperation.tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_VELOCITY){
                       controller.orientation_task.control_type_ = controller.orientation_task.CONTROL_VEL;
                       taco::copy(teleoperation.tool_command_.angular_desired_vel,
                                                      controller.orientation_task.desired_vel_);
                    }
                    else if (teleoperation.tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_ACCELERATION){
                       controller.orientation_task.control_type_ = controller.orientation_task.CONTROL_ACC;
                       taco::copy(teleoperation.tool_command_.angular_desired_acc,
                                                      controller.orientation_task.desired_acc_);
                    }
                    else if (teleoperation.tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_FORCE){
                       controller.orientation_task.control_type_ = controller.orientation_task.CONTROL_FORCE;
                       taco::copy(teleoperation.tool_command_.angular_desired_force,
                                                      controller.orientation_task.desired_force_);
                    }
                }
            }

            // compute the next control torque
            controller.computeTorque();

            // copy torque to shared memory
            {
                size_t dof = controller.dynamics()->DOF();
                TACO_SHM_SCOPED_LOCK(joint_command->sync.mutex);
                joint_command->header.seq += 1;
                joint_command->control_type = joint_command->CONTROL_FORCE;
                for (int i=0; i<dof; ++i){
                    std::string joint_name;
                    taco::copy(joint_command->joint_names[i], joint_name);
                    int joint_id = controller.dynamics_->jointID(joint_name);
                    joint_command->desired_force[i] = controller.F_(joint_id);
                }
            }

            // update tool position for teleoperation
            taco::copy(controller.position_task.position(),
                                           teleoperation.tool_state_.linear_position);
            taco::copy(controller.position_task.velocity(),
                                           teleoperation.tool_state_.linear_velocity);
            taco::copy(controller.position_task.appliedForce(),
                                           teleoperation.tool_state_.linear_force);
            taco::copy(controller.orientation_task.position(),
                                           teleoperation.tool_state_.angular_position);
            taco::copy(controller.orientation_task.velocity(),
                                           teleoperation.tool_state_.angular_velocity);
            taco::copy(controller.orientation_task.appliedForce(),
                                           teleoperation.tool_state_.angular_force);

            // publish new tool position
            teleoperation.sendToolState();

            // print out controller info every 1000 updates
            if (controller.updateCount()%1000==1){
                controller.printInfo();
            }
        }
    }

    double end_time = timer.elapsedTime();
    std::cout << "\n";
    std::cout << "Loop run time  : " << end_time << " seconds\n";
    std::cout << "Loop updates   : " << timer.elapsedCycles() << "\n";
    std::cout << "Loop frequency : " << timer.elapsedCycles()/end_time << "Hz\n";

    return 0;
}


