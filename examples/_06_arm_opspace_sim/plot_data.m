clc; clear all; close all;

%% plotting parameters
time_lim   = [0 10];
force_lim  = [-10 5];
torque_lim = [-0.5 0.5];

fig1 = figure;
fig2 = figure;

%% plot data from the link force sensor
filename = 'kuka_iiwa_force_sensor_link6.txt';
data = importdata(filename);
time = data(:,1);
force = data(:,2:4);
torque = data(:,5:7);
filtered_force = data(:,8:10);
filtered_torque = data(:,11:13);

figure(fig1)
subplot(3,1,1)
plot(time,force)
legend('x','y','z')
title(['raw: ',strrep(filename,'_','\_')])
%xlim(time_lim)
ylim(force_lim)

subplot(3,1,2)
plot(time,filtered_force)
legend('F_x','F_y','F_z')
title(['filtered: ',strrep(filename,'_','\_')])
%xlim(time_lim)
ylim(force_lim)

figure(fig2)
subplot(3,1,1)
plot(time,torque)
%xlim(time_lim)
ylim(torque_lim)

subplot(3,1,2)
plot(time,filtered_torque)
legend('\tau_x','\tau_y','\tau_z')
%xlim(time_lim)
ylim(torque_lim)

%% plot data from the joint force sensor
filename = 'kuka_iiwa_force_sensor_j6.txt';
data = importdata(filename);
time = data(:,1);
force = data(:,2:4);
torque = data(:,5:7);

figure(fig1)
subplot(3,1,3)
plot(time,force)
legend('x','y','z')
title(['raw: ',strrep(filename,'_','\_')])
%xlim(time_lim)
ylim(force_lim)

figure(fig2)
subplot(3,1,3)
plot(time,torque)
%xlim(time_lim)
ylim(torque_lim)
