/** @example _02_fixed_controller.cc

Simple Simulation Example
=========================

This example shows usage of the fixed base operational space controller.
It demonstrates how to use construct a controller with multiple tasks.

See also: @ref md_examples_EXAMPLES.
*/



#include <iostream>
#include <taco/dynamics/RBDLDynamics.h>
#include <taco/controller/fixed/FixedController.h>
#include <taco/model/models.h>

int main(int argc, char* argv[])
{
	std::cout << "hello world" << std::endl;

    //  Declare the dynamics engine, controller, and operational space tasks
    taco::RBDLDynamics dynamics;
    taco::fixed::TaskController controller;
    taco::fixed::JointsTask joint_task;
    taco::fixed::PositionTask pos_task;

    // Import robot model to create kinematic/dynamic model of robot
    dynamics.importRobot(taco::model::FILEPATH+"/"+taco::model::IIWA14+".urdf", true);

    // Set the joint positions (q) and velocities (dq) in the dynamics engine
    dynamics.q_ << 0,M_PI/6,0,M_PI/6,0,M_PI/6,0;
    dynamics.dq_ << 0,0,0,0,0,0,0;
    dynamics.updateDynamics();

    // Add the dynamics engine and tasks to the controller
    // Position task is highest priority
    controller.setDynamics(&dynamics);
    controller.addTask(&pos_task,0);
    controller.addTask(&joint_task,1);

    // Set parameters for position task
    // Control the end-effector (link_7)
    // desired position = current position + 20 cm each direction
    pos_task.setOperationalPoint("link_7");
    pos_task.desired_pos_ = pos_task.position(true);
    pos_task.desired_pos_ += Eigen::Vector3d(0.2,0.2,-0.2);
    pos_task.Kp_ = 5.0;
    pos_task.Kv_ = 2*sqrt(pos_task.Kp_);

    // Set joint task parameters
    // Add null space joint damping
    joint_task.Kp_.setZero();
    joint_task.Kv_.setConstant(1.0);

    // Enable controller
    controller.setGravityCompensation(false);
    controller.setEnabled(true);

    // Compute the controller torques
    controller.computeTorque();
    controller.printInfo();

    return 0;

}
