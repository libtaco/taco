/** @example _14_robot_driver.cc

Controlling a Real Robot using a Driver
=======================================

This example shows how to control the robot using a shared memory driver.
It is similar to the @ref _08_tele_op_sim.cc example.
However, here we use the KUKA LBR iiwa.
We use Kuka's proprietary dynamics library to calculate the mass matrix and coriolis terms.

1. Build and run the Kuka LBR iiwa driver, by following @ref md_drivers_kuka_iiwa_KUKA_IIWA_README "KUKA LBR iiwa Readme"
2. To visualize the robot, use the bash script

        sh run_visualization.sh

3. Obtain KukaLBRDynamics from KUKA Roboter GmbH, and download to "taco/examples/_14_robot_driver/KukaLBRDynamics".
   Graduate students in the Stanford Robotics Lab can clone from the private repository.

        cd taco/examples/_14_robot_driver/
        git clone https://bitbucket.org/libtaco/kukalbrdynamics.git KukaLBRDynamics

   Alternatively, link KukaLBRDynamics from the driver folder.

        cd taco/examples/_14_robot_driver/
        ln -sf "$(pwd)/../../drivers/kuka_iiwa/KukaLBRDynamics"

   @note KukaLBRDynamics was compiled on GCC 4.X, so this example should be run on a Ubuntu 14.04 computer.

4. Build this examples

        mkdir build && cd build
        cmake .. && make

5. Run the controller

        ./build/robot_driver

6. To control the robot's end effector, use either @ref _10_keyboard_teleoperation.cc or @ref _11_haptic_teleoperation.cc example.
7. To publish the shared memory to ros and plot the joint states, use @ref _12_shm_ros_publisher.cc .


@note It is also possible to run a simulation of the robot, using the "run_simulation.sh" script.
The controller will be ill-behaved because the inertial model used by the controller (Kuka provided dynamics)
poorly match the inertial model used by the simulator (parsed from taco/models/kuka_iiwa). Gravity compensation
must be enabled in the controller. By default, the controller's gravity compensation is off because
the real robot internally compensated for gravity.


See also: @ref md_examples_EXAMPLES.
*/

#include "KukaArmController.h"
#include <taco/model/models.h>
#include <taco/controller/ControlLoopTimer.h>
#include <taco/comm/shm/SharedMemory.h>
#include <taco/comm/shm/JointState.h>
#include <taco/comm/shm/JointCommand.h>
#include <taco/comm/shm/ForceSensor.h>
#include <taco/comm/copy.h>
#include <taco/comm/topics.h>
#include <taco/primitive/teleoperation/robot/TeleRobotPrimitive.h>

#include <string>
#include <iostream>


const std::string robot_name = "iiwa14";
const std::string tool_name = "link_7";


// some functions to exit while loop
volatile bool running_ = false;
void stop(int){running_ = false;}


int main(int argc, char* argv[])
{
    std::cout << "hello world" << std::endl;

    // create controller. using prexisting arm controller.
    const std::string model_file = taco::model::FILEPATH+"/"+taco::model::iiwa14+".urdf";
    taco::fixed::KukaArmController controller(kuka::Robot::LBRiiwa,
                                              model_file,
                                              "link6",Eigen::Vector3d(0,0,0),
                                              "link3",Eigen::Vector3d(0,0,0));
    controller.setGravityCompensation(false);

    bool initialized = false;

    // shared memory to communicate between controller
    // and the robot hardware or simulator
    taco::shm::SharedMemory<taco::shm::JointState<7> > shm_joint_state;
    taco::shm::JointState<7>* joint_state = NULL;

    taco::shm::SharedMemory<taco::shm::JointCommand<7> > shm_joint_command;
    taco::shm::JointCommand<7>* joint_command = NULL;

    // create a primitive for teleoperation
    taco::TeleRobotPrimitive teleoperation(robot_name, tool_name);

    // create a loop timer
    double control_freq = 1000;
    taco::ControlLoopTimer timer;
    timer.setLoopFrequency(control_freq);   // 1 KHz
    timer.setThreadHighPriority();  // make timing more accurate. requires running executable as sudo.
    timer.setCtrlCHandler(stop);    // exit while loop on ctrl-c
    timer.initializeTimer(1000000); // 1 ms pause before starting loop

    // run the servo loop
    std::cout << "Waiting for robot to create joint state shared memory\n";
    running_ = true;
    while(running_)
    {
        // wait for next scheduled loop
        timer.waitForNextLoop();

        // open shared memory
        if (joint_state==NULL){
            joint_state = shm_joint_state.open(robot_name+":"+taco::topic::JOINT_STATE);
            if (joint_state==NULL){ continue; }
            std::cout << "Opened joint state shared memory\n";
            if (joint_state->dof != controller.dynamics()->DOF()){
                throw std::runtime_error("Joint state shared memory DOF mismatch");
            }
        }

        if (joint_command==NULL){
            joint_command = shm_joint_command.open(robot_name+":"+taco::topic::JOINT_COMMAND);
            if (joint_command==NULL){continue;}
            std::cout << "Opened joint command shared memory\n";
            if (joint_state->dof != controller.dynamics()->DOF()){
                throw std::runtime_error("Joint command shared memory DOF mismatch");
            }
        }

        // check if shared memory initialized or has closed
        if (!joint_state->sync.initialized){continue;}
        if (joint_state->sync.removed){
            std::cout << "Joint state shared memory has closed.\n";
            shm_joint_state.remove();
            joint_state = NULL;
            continue;
        }

        if (!joint_command->sync.initialized){continue;}
        if (joint_command->sync.removed){
            std::cout << "Joint command shared memory has closed.\n";
            shm_joint_command.remove();
            joint_command = NULL;
            continue;
        }

        // copy the joint state from shared memory to the dynamics
        {
            size_t dof = controller.dynamics()->DOF();
            TACO_SHM_SCOPED_LOCK(joint_state->sync.mutex);
            for (int i=0; i<dof; ++i){
                std::string joint_name;
                taco::copy(joint_state->joint_names[i], joint_name);
                controller.dynamics_->setq(joint_name, joint_state->position[i]);
                controller.dynamics_->setdq(joint_name, joint_state->velocity[i]);
                controller.dynamics_->setddq(joint_name, joint_state->acceleration[i]);
            }
        }

        // initialize goal position to current position
        if (initialized==false)
        {
            controller.dynamics_->updateDynamics();
            controller.position_task.desired_pos_ = controller.position_task.position(true);
            controller.orientation_task.desired_pos_ = controller.orientation_task.position(true);
            controller.setEnabled(true);
            initialized = true;
            std::cout << "Initialized starting position from first joint state :)\n";
        }

        if (initialized)
        {
            // update controller goal positions from tool command
            ros::spinOnce();
            if (teleoperation.toolCommandUpdated())
            {
                if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_POSITION){
                   controller.position_task.control_type_ = controller.position_task.CONTROL_POS;
                   taco::copy(teleoperation.tool_command_.linear_desired_pos,
                                                  controller.position_task.desired_pos_);
                }
                else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_VELOCITY){
                   controller.position_task.control_type_ = controller.position_task.CONTROL_VEL;
                   taco::copy(teleoperation.tool_command_.linear_desired_vel,
                                                  controller.position_task.desired_vel_);
                }
                else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_ACCELERATION){
                   controller.position_task.control_type_ = controller.position_task.CONTROL_ACC;
                   taco::copy(teleoperation.tool_command_.linear_desired_acc,
                                                  controller.position_task.desired_acc_);
                }
                else if (teleoperation.tool_command_.linear_control_type==taco_msgs::ToolCommand::CONTROL_FORCE){
                   controller.position_task.control_type_ = controller.position_task.CONTROL_FORCE;
                   taco::copy(teleoperation.tool_command_.linear_desired_force,
                                                  controller.position_task.desired_force_);
                }

                if (teleoperation.tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_POSITION){
                   controller.orientation_task.control_type_ = controller.orientation_task.CONTROL_POS;
                   taco::copy(teleoperation.tool_command_.angular_desired_pos,
                                                  controller.orientation_task.desired_pos_);
                }
                else if (teleoperation.tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_VELOCITY){
                   controller.orientation_task.control_type_ = controller.orientation_task.CONTROL_VEL;
                   taco::copy(teleoperation.tool_command_.angular_desired_vel,
                                                  controller.orientation_task.desired_vel_);
                }
                else if (teleoperation.tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_ACCELERATION){
                   controller.orientation_task.control_type_ = controller.orientation_task.CONTROL_ACC;
                   taco::copy(teleoperation.tool_command_.angular_desired_acc,
                                                  controller.orientation_task.desired_acc_);
                }
                else if (teleoperation.tool_command_.angular_control_type==taco_msgs::ToolCommand::CONTROL_FORCE){
                   controller.orientation_task.control_type_ = controller.orientation_task.CONTROL_FORCE;
                   taco::copy(teleoperation.tool_command_.angular_desired_force,
                                                  controller.orientation_task.desired_force_);
                }
            }

            // compute the next control torque
            controller.computeTorque();

            // copy torque to shared memory
            {
                size_t dof = controller.dynamics()->DOF();
                TACO_SHM_SCOPED_LOCK(joint_command->sync.mutex);
                joint_command->header.seq += 1;
                joint_command->control_type = joint_command->CONTROL_FORCE;
                for (int i=0; i<dof; ++i){
                    std::string joint_name;
                    taco::copy(joint_command->joint_names[i], joint_name);
                    int joint_id = controller.dynamics_->jointID(joint_name);
                    joint_command->desired_force[i] = controller.F_(joint_id);
                }
            }

            // update tool position for teleoperation
            taco::copy(controller.position_task.position(),
                                           teleoperation.tool_state_.linear_position);
            taco::copy(controller.position_task.velocity(),
                                           teleoperation.tool_state_.linear_velocity);
            taco::copy(controller.position_task.appliedForce(),
                                           teleoperation.tool_state_.linear_force);
            taco::copy(controller.orientation_task.position(),
                                           teleoperation.tool_state_.angular_position);
            taco::copy(controller.orientation_task.velocity(),
                                           teleoperation.tool_state_.angular_velocity);
            taco::copy(controller.orientation_task.appliedForce(),
                                           teleoperation.tool_state_.angular_force);

            // publish new tool position
            teleoperation.sendToolState();

            // print out controller info every 1000 updates
            if (controller.updateCount()%1000==1){
                controller.printInfo();
            }
        }
    }

    double end_time = timer.elapsedTime();
    std::cout << "\n";
    std::cout << "Loop run time  : " << end_time << " seconds\n";
    std::cout << "Loop updates   : " << timer.elapsedCycles() << "\n";
    std::cout << "Loop frequency : " << timer.elapsedCycles()/end_time << "Hz\n";

    return 0;
}

