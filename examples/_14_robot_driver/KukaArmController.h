/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_CONTROLLER_FIXED_KukaArmController_H_
#define TACO_CONTROLLER_FIXED_KukaArmController_H_

#include <taco/controller/fixed/TaskController.h>
#include <taco/controller/fixed/PositionTask.h>
#include <taco/controller/fixed/OrientationTask.h>
#include <taco/controller/fixed/ElbowTask.h>
#include <taco/controller/fixed/JointsTask.h>
#include <taco/controller/fixed/JointLimitsTask.h>
#include <taco/controller/fixed/ToolWeightTask.h>
#include <taco/dynamics/KUKADynamics.h>

#include <Eigen/Dense>

#include <string>


namespace taco {
namespace fixed {

/** \brief Operational space controller for a robotic arm.
 *
 * \param DynamicsT Dynamics engine to use, of Dynamics derived class
 *
 * Tasks from highest to lowest priority:
 * (1) Joint limits
 * (2) Orientation of link_name
 * (3) Position of link_name
 * (4) Elbow task (optional)
 * (5) Joint damping
 * n/a ToolWeigthTask
 *
 * \note No assumption is made about DOF. The task hierarchy should handle various kinematic configurations.
 *
 * \ingroup fixed
 */

class KukaArmController : public taco::fixed::TaskController
{
public:
    
    /** \brief Constructor for arm controller with all required entries.
     *
     * \param robot_file    Path to the urdf description of the robot
     * \param link_name     Name of end effector link to control, from the urdf description
     * \param pos_in_link   Position of the point on the link to control, relative to the link origin
     */
    KukaArmController(kuka::Robot::robotName robot_type,
                      const std::string& robot_file,
                      const std::string& link_name,
                      const Eigen::Vector3d& pos_in_link)
    {
        // initialize dynamic engine for controller.
        Dynamics* dynamics = new KukaDynamics(robot_type);
        dynamics->importRobot(robot_file, true);    // import robot model
        this->setDynamics(dynamics);

        // add prioritized tasks to controller. controller will delete tasks.
        this->addTask(&limit_task,0);
        this->addTask(&orientation_task,1);
        this->addTask(&position_task,2);
        this->addTask(&joints_task,3);
        this->addTask(&tool_weight_task,4);

        position_task.setOperationalPoint(link_name,pos_in_link);
        orientation_task.setOperationalBody(link_name);
        tool_weight_task.setTool(0, link_name, pos_in_link);

        setDefaultParameters();
    }

    /** \brief Constructor for arm controller with all required entries.
     *
     * \param robot_file                Path to the urdf description of the robot
     * \param end_effector_link_name    Name of end effector link to control, from the urdf description
     * \param end_effector_pos_in_link  Position of the point on the link to control, relative to the link origin
     * \param elbow_link_name           Name of elbow link to control, from the urdf description
     * \param elbow_pos_in_link         Position of the point on the link to control, relative to the link origin
     */
    KukaArmController(kuka::Robot::robotName robot_type,
                      const std::string& robot_file,
                      const std::string& end_effector_link_name,
                      const Eigen::Vector3d& end_effector_pos_in_link,
                      const std::string& elbow_link_name,
                      const Eigen::Vector3d& elbow_pos_in_link)
    {
        // initialize dynamic engine for controller.
        Dynamics* dynamics = new KukaDynamics(robot_type);
        dynamics->importRobot(robot_file, true);    // import robot model
        this->setDynamics(dynamics);

        // add prioritized tasks to controller. controller will delete tasks.
        this->addTask(&limit_task,0);
        this->addTask(&orientation_task,1);
        this->addTask(&position_task,2);
        this->addTask(&elbow_task,3);
        this->addTask(&joints_task,4);
        this->addTask(&tool_weight_task, 5);

        position_task.setOperationalPoint(end_effector_link_name,end_effector_pos_in_link);
        orientation_task.setOperationalBody(end_effector_link_name);
        elbow_task.setEndEffector(end_effector_link_name, end_effector_pos_in_link);
        elbow_task.setElbow(      elbow_link_name,        elbow_pos_in_link);
        tool_weight_task.setTool(0, end_effector_link_name, end_effector_pos_in_link);

        setDefaultParameters();
    }

    virtual ~KukaArmController()
    {
        delete dynamics_;
    }

    /** \brief set the default task parameters. called in constructors */
    virtual void setDefaultParameters()
    {
        if (this->dynamics_ == NULL){
            printWarning("Cannot set default parameters. Dynamics is not set.");
            return;
        }

        // joint limits
        // nothing

        // end effector position control
        // desired position = current position
        position_task.control_type_ = position_task.CONTROL_POS;
        position_task.desired_pos_ = position_task.position(true);
        position_task.Kp_ = 5.0;
        position_task.Kv_ = 2*sqrt(position_task.Kp_); //critical damping

        // end effector orientation control
        // desired orientation  = default (identity)
        orientation_task.control_type_ = orientation_task.CONTROL_POS;
        orientation_task.desired_pos_ = orientation_task.position(true);
        orientation_task.Kp_ = 5.0;
        orientation_task.Kv_ = 2*sqrt(orientation_task.Kp_); //critical damping

        // elbow posture control
        // nothing

        // null space joint damping
        joints_task.Kp_.setZero();
        joints_task.Kv_.setConstant(2*sqrt(5));

        // tool weight task
        // nothing
    }

    /// \brief Task controlling end effector position.
    taco::fixed::PositionTask position_task;

    /// \brief Task controlling end effector orientation.
    taco::fixed::OrientationTask orientation_task;

    /// \brief Task controlling elbow posture
    taco::fixed::ElbowTask elbow_task;

    /// \brief Task controlling joints.
    taco::fixed::JointsTask joints_task;

    /// \brief Task joint limits.
    taco::fixed::JointLimitsTask limit_task;

    /// \brief ToolWeigthTask.
    taco::fixed::ToolWeightTask tool_weight_task;

protected:

    virtual void printWarning(const std::string& message) {
        std::cout << "WARNING. KukaArmController. " << message << std::endl;
    }

};


}
}

#endif
