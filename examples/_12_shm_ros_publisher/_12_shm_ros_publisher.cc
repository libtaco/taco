/** @example _12_shm_ros_publisher.cc

Shared Memory ROS Publisher
=======================

This example reads the joint state, joint command, and force sensor shared memory
and published these to ROS. This can be used for the follow examples:

* * @ref _06_arm_opspace_sim.cc
* * @ref _07_arm_qp_sim.cc
* * @ref _08_rcm_sim.cc
* * @ref _09_quadruped_sim.cc

After launching one of the above simulations, run the publish and plot bash script.
This uses the ROS rqt_plot to plot the message variables. Feel free to modify the plotted variables in the script.
See also @ref md_include_taco_comm_ROS_COMMANDS for additional information on ROS messages.

     ./build/shm_ros_publisher
     sh plot.sh

\note Mutexes are not locked, so that this will not delay other processes. Therefore, data may be invalid at times.

See also: @ref md_examples_EXAMPLES.
*/

#include <taco/model/models.h>
#include <taco/controller/ControlLoopTimer.h>
#include <taco/comm/shm/SharedMemory.h>
#include <taco/comm/shm/JointState.h>
#include <taco/comm/shm/JointCommand.h>
#include <taco/comm/shm/ForceSensor.h>
#include <taco/comm/copy.h>
#include <taco/comm/topics.h>
#include <taco/comm/ros/JointState.h>
#include <taco/comm/ros/JointCommand.h>
#include <taco/comm/ros/ForceSensor.h>
#include <ros/ros.h>

#include <string>
#include <iostream>


const std::string robot_name = "iiwa14";
const std::string force_sensor_name = "link_7";
const size_t DOF = 7;



// some functions to exit while loop
volatile bool running_ = false;
void stop(int){running_ = false;}

void copy(const taco::shm::Header& shm, ::std_msgs::Header& msg);
void copy(taco::shm::JointState<DOF>* shm, taco_msgs::JointState& msg);
void copy(taco::shm::JointCommand<DOF>* shm, taco_msgs::JointCommand& msg);
void copy(taco::shm::ForceSensor* shm, taco_msgs::ForceSensor& msg);



int main(int argc, char* argv[])
{
    std::cout << "hello world" << std::endl;

    // shared memory to communicate between controller
    // and the robot hardware or simulator
    taco::shm::SharedMemory<taco::shm::JointState<DOF> > shm_joint_state;
    taco::shm::JointState<DOF>* joint_state = NULL;

    taco::shm::SharedMemory<taco::shm::JointCommand<DOF> > shm_joint_command;
    taco::shm::JointCommand<DOF>* joint_command = NULL;

    // shared memory to read the force sensor from robot hardware or simulator
    taco::shm::SharedMemory<taco::shm::ForceSensor > shm_force_sensor;
    taco::shm::ForceSensor* force_sensor = NULL;

    // create a ros node to publish joint state, and force sensor data
    // initialize ros, if it has not already bee initialized.
    if (!ros::isInitialized()){
      int argc = 0; char **argv = NULL;
      ros::init(argc, argv, "shm_ros_publisher_"+robot_name, ros::init_options::NoSigintHandler);
    }
    ros::NodeHandle ros_node(robot_name);
    ros::Publisher joint_state_pub = ros_node.advertise<taco_msgs::JointState>(taco::topic::JOINT_STATE, 1);
    ros::Publisher joint_command_pub = ros_node.advertise<taco_msgs::JointCommand>(taco::topic::JOINT_COMMAND, 1);
    ros::Publisher force_sensor_pub = ros_node.advertise<taco_msgs::ForceSensor>(taco::topic::FORCE_SENSOR+"/"+force_sensor_name, 1);
    taco_msgs::JointState joint_state_msg;
    taco_msgs::JointCommand joint_command_msg;
    taco_msgs::ForceSensor force_sensor_msg;

    // allocate message size
    joint_state_msg.joint_names.resize(DOF);
    joint_state_msg.position.resize(DOF,0.0);
    joint_state_msg.velocity.resize(DOF,0.0);
    joint_state_msg.acceleration.resize(DOF,0.0);
    joint_state_msg.force_commanded.resize(DOF,0.0);
    joint_state_msg.force_measured.resize(DOF,0.0);

    joint_command_msg.control_type = taco_msgs::JointCommand::CONTROL_NONE;
    joint_command_msg.joint_names.resize(DOF);
    joint_command_msg.desired_pos.resize(DOF,0.0);
    joint_command_msg.desired_vel.resize(DOF,0.0);
    joint_command_msg.desired_acc.resize(DOF,0.0);
    joint_command_msg.desired_force.resize(DOF,0.0);

    // create a loop timer
    double control_freq = 1000;
    taco::ControlLoopTimer timer;
    timer.setLoopFrequency(control_freq);   // 1 KHz
    timer.setCtrlCHandler(stop);    // exit while loop on ctrl-c
    timer.initializeTimer(1000000); // 1 ms pause before starting loop

    // run the servo loop
    std::cout << "Waiting for robot to create joint state shared memory\n";
    running_ = true;
    while(running_)
    {
        // wait for next scheduled loop
        timer.waitForNextLoop();

        // open shared memory
        if (joint_state==NULL){
            joint_state = shm_joint_state.open(robot_name+":"+taco::topic::JOINT_STATE);
            if (joint_state!=NULL){
                std::cout << "Opened joint state shared memory\n";
            }
        }

        if (joint_command==NULL){
            joint_command = shm_joint_command.open(robot_name+":"+taco::topic::JOINT_COMMAND);
            if (joint_command!=NULL){
                std::cout << "Opened joint command shared memory\n";
            }
        }

        if (force_sensor==NULL){
            force_sensor = shm_force_sensor.open(robot_name+":"+taco::topic::FORCE_SENSOR+":"+force_sensor_name);
            if (force_sensor!=NULL){
                std::cout << "Opened force sensor shared memory\n";
            }
        }

        // check if shared memory initialized or has closed
        if (joint_state!=NULL && joint_state->sync.initialized && joint_state->sync.removed){
            std::cout << "Joint state shared memory has closed.\n";
            shm_joint_state.remove();
            joint_state = NULL;
            continue;
        }

        if (joint_command!=NULL && joint_command->sync.initialized && joint_command->sync.removed){
            std::cout << "Joint command shared memory has closed.\n";
            shm_joint_command.remove();
            joint_command = NULL;
            continue;
        }

        if (force_sensor!=NULL && force_sensor->sync.initialized && force_sensor->sync.removed){
            std::cout << "Force sensor shared memory has closed.\n";
            shm_force_sensor.remove();
            force_sensor = NULL;
            continue;
        }

        // send robot joint state and force sensor to ros
        if (joint_state!=NULL){
            copy(joint_state, joint_state_msg);
            joint_state_pub.publish(joint_state_msg);
        }
        if (joint_command!=NULL){
            copy(joint_command, joint_command_msg);
            joint_command_pub.publish(joint_command_msg);
        }
        if (force_sensor!=NULL){
            copy(force_sensor, force_sensor_msg);
            force_sensor_pub.publish(force_sensor_msg);
        }
    }

    double end_time = timer.elapsedTime();
    std::cout << "\n";
    std::cout << "Loop run time  : " << end_time << " seconds\n";
    std::cout << "Loop updates   : " << timer.elapsedCycles() << "\n";
    std::cout << "Loop frequency : " << timer.elapsedCycles()/end_time << "Hz\n";

    return 0;
}


void copy(const taco::shm::Header& shm, ::std_msgs::Header& msg)
{
    msg.frame_id = shm.frame_id;
    msg.seq = shm.seq;
    msg.stamp.sec = shm.stamp.sec;
    msg.stamp.nsec = shm.stamp.nsec;
}

void copy(taco::shm::JointState<DOF>* shm, taco_msgs::JointState& msg)
{
    //TACO_SHM_SCOPED_LOCK(shm->sync.mutex);
    copy(shm->header, msg.header);
    for (size_t i=0; i<DOF; ++i){
        msg.joint_names[i] = std::string(shm->joint_names[i]);
    }
    for (size_t i=0; i<DOF; ++i){
        msg.position[i] = shm->position[i];
        msg.velocity[i] = shm->velocity[i];
        msg.acceleration[i] = shm->acceleration[i];
        msg.force_commanded[i] = shm->force_commanded[i];
        msg.force_measured[i] = shm->force_measured[i];
    }
}

void copy(taco::shm::JointCommand<DOF>* shm, taco_msgs::JointCommand& msg)
{
    //TACO_SHM_SCOPED_LOCK(shm->sync.mutex);
    copy(shm->header, msg.header);
    for (size_t i=0; i<DOF; ++i){
        msg.joint_names[i] = std::string(shm->joint_names[i]);
    }
    msg.control_type = shm->control_type;
    for (size_t i=0; i<DOF; ++i){
        msg.desired_pos[i] = shm->desired_pos[i];
        msg.desired_vel[i] = shm->desired_vel[i];
        msg.desired_acc[i] = shm->desired_acc[i];
        msg.desired_force[i] = shm->desired_force[i];
    }
}

void copy(taco::shm::ForceSensor* shm, taco_msgs::ForceSensor& msg)
{
    //TACO_SHM_SCOPED_LOCK(shm->sync.mutex);
    copy(shm->header, msg.header);
    for (int i=0; i<3; ++i){
        msg.force[i] = shm->force[i];
        msg.moment[i] = shm->moment[i];
    }
}




