/** @example _11_haptic_teleoperation.cc

Teleoperation with Haptics
==========================

This example shows how to control the robot using teleoperation.
Here we use a haptic device to control the robot. You can use this to control the following examples:

* * @ref _05_arm_plugin_sim.cc
* * @ref _06_arm_opspace_sim.cc
* * @ref _07_arm_qp_sim.cc
* * @ref _08_rcm_sim.cc
* * @ref _09_quadruped_sim.cc


This example has a dependency on [chai3d] for the haptic device interface. Download and build chai3d, then create a link to chai3d in this folder. 

     cd _11_haptic_teleoperation
     ln -s path-to-chai3d chai3d

Add usb permission to your udev rules (permanent fix). You can also try changing [temporary usb permissions], or running the program with sudo.


     sudo cp 97-forcedimension.rules /etc/udev/rules.d/
     sudo udevadm control --reload-rules

Now plug in the haptics device. You might also try "sudo udevadm trigger".


[chai3d]:http://www.chai3d.org/
[temporary usb permissions]:https://github.com/radarsat1/dimple/blob/master/doc/novint_falcon_howto.md

See also: @ref md_examples_EXAMPLES.
*/

#include <taco/primitive/teleoperation/user/TeleUserPrimitive.h>
#include <taco/comm/topics.h>
#include <taco/model/models.h>

#include <taco/comm/ros/ForceSensor.h>
#include <taco/comm/topics.h>
#include <ros/ros.h>
#include <ros/transport_hints.h>

#include <chai3d.h>

#include <string>
#include <iostream>
#include <unistd.h>
#include <signal.h>


const std::string robot_name = "iiwa14";
const std::string tool_name = "link_7";
const std::string force_sensor_name = "link_7";
const bool use_absolute_orientation = true;
const bool use_orientation_control = false;



// while loop
bool initialized = false;
volatile bool running = false;
void siginthandler(int param){
  std::cout << "exiting..." << std::endl;
  running = false;
}


// initial position
chai3d::cVector3d haptic_pos_offset(0,0,0);
chai3d::cMatrix3d haptic_rot_offset_T(1,0,0,0,1,0,0,0,1);
chai3d::cVector3d tool_pos_offset(3,0,0);
chai3d::cMatrix3d tool_rot_offset(1,0,0,0,1,0,0,0,1);


// haptic device parameters
double pos_scaling = 10;
double force_scaling = 1.0;
chai3d::cGenericHapticDevicePtr device;
chai3d::cHapticDeviceInfo device_info;


taco_msgs::ForceSensor force_sensor_;
void updateForce(const taco_msgs::ForceSensor& msg){
    force_sensor_ = msg;
}


int main ()
{
    // initialize haptic device
    chai3d::cHapticDeviceHandler handler;
    int num_devices = handler.getNumDevices();
    std::cout << "Found [" << num_devices << "] haptic devices.\n";
    if (num_devices>0){
        handler.getDevice(device, 0);
        chai3d::cHapticDeviceInfo info = device->getSpecifications();
        std::cout << "=====Device Details=====\n"
                  << "Manufacturer: " << info.m_manufacturerName << "\n"
                  << "Model       : " << info.m_model            << "\n"
                  << "Name        : " << info.m_modelName        << "\n";
    }
    device->open();
    device->calibrate();
    device->setEnableGripperUserSwitch(true);
    device_info = device->getSpecifications();

    // initialize the teleoperation
    taco::TeleUserPrimitive teleoperation(robot_name, tool_name);
    if (device_info.m_sensedPosition){
        std::cout << "Haptic device supports position control\n";
        teleoperation.tool_command_.linear_control_type = taco_msgs::ToolCommand::CONTROL_POSITION;
    } else {
        std::cout << "Haptic device does not support position control\n";
        teleoperation.tool_command_.linear_control_type = taco_msgs::ToolCommand::CONTROL_NONE;
    }
    if (device_info.m_sensedPosition){
        std::cout << "Haptic device supports orientation control\n";
        if (use_orientation_control){
            teleoperation.tool_command_.angular_control_type = taco_msgs::ToolCommand::CONTROL_POSITION;
        } else {
            teleoperation.tool_command_.angular_control_type = taco_msgs::ToolCommand::CONTROL_NONE;
        }
    } else {
        std::cout << "Haptic device does not support orientation control\n";
        teleoperation.tool_command_.angular_control_type = taco_msgs::ToolCommand::CONTROL_NONE;
    }

    std::cout << "aaa" << std::endl;

    // initialize the force sensor
    // initialize ros, if it has not already bee initialized.
    if (!ros::isInitialized()){
      int argc = 0;
      char **argv = NULL;
      ros::init(argc, argv, "haptic_teleoperation", ros::init_options::NoSigintHandler);
    }

    std::cout << "bbb" << std::endl;

    // subscribe and publish ros topics
    ros::NodeHandle ros_node(robot_name);
    ros::Subscriber subscriber = ros_node.subscribe(taco::topic::FORCE_SENSOR+"/"+force_sensor_name, 1,
                                     &::updateForce,
                                     ros::TransportHints().tcpNoDelay());

    std::cout << "ccc" << std::endl;

    // initialize ncurses for keyboard input
    signal(SIGINT, siginthandler);

    // set up control loop
    double control_loop_frequency = 1000;
    unsigned int interval = 1000000000.0/control_loop_frequency;
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC ,&t);
    t.tv_sec++;

    // start haptic loop
    unsigned long loop_counter = 0;
    bool initialized = false;
    running = true;
    while(running)
    {
        // get ros updates
        ros::spinOnce();
        bool updated = teleoperation.toolStateUpdated();

        // initialize command to initial position
        if (!initialized && updated){
            // save initial haptic position and orientation as an offset
            device->getPosition(haptic_pos_offset);
            device->getRotation(haptic_rot_offset_T);
            haptic_rot_offset_T.trans();

            // save initial tool position and orientation as an offset
            tool_pos_offset.set(teleoperation.tool_state_.linear_position[0],
                                teleoperation.tool_state_.linear_position[1],
                                teleoperation.tool_state_.linear_position[2]);
            for (int i=0; i<3; ++i){
                teleoperation.tool_command_.linear_desired_pos[i] = teleoperation.tool_state_.linear_position[i];
            }
            if (use_orientation_control)
            {
                chai3d::cQuaternion(teleoperation.tool_state_.angular_position[0],
                                    teleoperation.tool_state_.angular_position[1],
                                    teleoperation.tool_state_.angular_position[2],
                                    teleoperation.tool_state_.angular_position[3]).toRotMat(tool_rot_offset);
                for (int i=0; i<4; ++i){
                    teleoperation.tool_command_.angular_desired_pos[i] = teleoperation.tool_state_.angular_position[i];
                }
            }
            initialized = true;
        }

        if (initialized){

            // get haptice device button state
            bool button_state;
            device->getUserSwitch(0, button_state);

            // get haptic device position
            chai3d::cVector3d haptic_pos;
            device->getPosition(haptic_pos);

            // set new goal position
            if (button_state){
                haptic_pos_offset = haptic_pos;
                tool_pos_offset.set(teleoperation.tool_command_.linear_desired_pos[0],
                                    teleoperation.tool_command_.linear_desired_pos[1],
                                    teleoperation.tool_command_.linear_desired_pos[2]);
            }
            chai3d::cVector3d new_pos = pos_scaling*(haptic_pos - haptic_pos_offset) + tool_pos_offset;
            teleoperation.tool_command_.linear_desired_pos[0] = new_pos(0);
            teleoperation.tool_command_.linear_desired_pos[1] = new_pos(1);
            teleoperation.tool_command_.linear_desired_pos[2] = new_pos(2);

            if (use_orientation_control)
            {
                 // get haptic device rotation
                chai3d::cMatrix3d haptic_rot;
                device->getRotation(haptic_rot);

                // set the new goal orientation
                if (button_state){
                    haptic_rot_offset_T = haptic_rot;
                    haptic_rot_offset_T.trans();
                    if (!use_absolute_orientation){
                        chai3d::cQuaternion(teleoperation.tool_command_.angular_desired_pos[0],
                                            teleoperation.tool_command_.angular_desired_pos[1],
                                            teleoperation.tool_command_.angular_desired_pos[2],
                                            teleoperation.tool_command_.angular_desired_pos[3]).toRotMat(tool_rot_offset);
                    }
                }
                chai3d::cMatrix3d new_rot;
                if (use_absolute_orientation){
                    new_rot = haptic_rot*tool_rot_offset;
                } else {
                    new_rot = (haptic_rot*haptic_rot_offset_T)*tool_rot_offset;
                }
                chai3d::cQuaternion new_quat; new_quat.fromRotMat(new_rot);
                teleoperation.tool_command_.angular_desired_pos[0] = new_quat.w;
                teleoperation.tool_command_.angular_desired_pos[1] = new_quat.x;
                teleoperation.tool_command_.angular_desired_pos[2] = new_quat.y;
                teleoperation.tool_command_.angular_desired_pos[3] = new_quat.z;
            }

            // publish message and handle callbacks
            teleoperation.sendToolCommand();
            ros::spinOnce();
        }

        // set haptic device force
        const chai3d::cVector3d force(  force_sensor_.force[0],
                                        force_sensor_.force[1],
                                        force_sensor_.force[2]);
        const chai3d::cVector3d moment( force_sensor_.moment[0],
                                        force_sensor_.moment[1],
                                        force_sensor_.moment[2]);

        if (loop_counter%1000==0){
            std::cout << "f/m : " << force << "\t/\t" << moment << "\n";
            bool button_state;
            device->getUserSwitch(0, button_state);
            std::cout << button_state << std::endl;
        }

        device->setForceAndTorqueAndGripperForce(-force*force_scaling/pos_scaling,
                                                 -moment*force_scaling/pos_scaling,0);

        ++loop_counter;

        // calculate next shot. carry over nanoseconds into microseconds.
        t.tv_nsec += interval;
        while (t.tv_nsec >= 1000000000){
            t.tv_nsec -= 1000000000;
            t.tv_sec++;
        }
    }

    // close haptic device
    device->close();
}


