cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(haptic_teleoperation)

############## CMAKE OPTIONS #####################

SET(CMAKE_VERBOSE_MAKEFILE OFF)
SET(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

set(TACO_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../..)

# chai3d search path
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} {CMAKE_CURRENT_SOURCE_DIR}/chai3d/build )
find_package(CHAI3D REQUIRED)
link_directories(${CHAI3D_LIBRARY_DIRS})

############### SOURCE FILES ######################

add_executable(${PROJECT_NAME}
               _11_haptic_teleoperation.cc
              )

############### MODULES ######################

# teleoperation user primitive
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${TACO_DIR}/include/taco/primitive/teleoperation/user)
find_package (TeleUserPrimitive REQUIRED)
include_directories(    ${TACO_DIR}/include/)
include_directories(    ${TeleUserPrimitive_INCLUDE_DIRS})
target_link_libraries(  ${PROJECT_NAME} ${TeleUserPrimitive_LIBRARIES} )

# chai3d for haptics
include_directories(${CHAI3D_INCLUDE_DIRS})
target_link_libraries(  ${PROJECT_NAME} ${CHAI3D_LIBRARIES} )
