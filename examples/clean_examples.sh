#!/bin/bash

TACO_EXAMPLES_PATH=$(dirname $(readlink -f $0))

rm -rf $TACO_EXAMPLES_PATH/_01_operational_space/build
rm -rf $TACO_EXAMPLES_PATH/_02_fixed_controller/build
rm -rf $TACO_EXAMPLES_PATH/_03_gazebo_cpp_sim/build
rm -rf $TACO_EXAMPLES_PATH/_04_op_vs_qp_controller/build
rm -rf $TACO_EXAMPLES_PATH/_05_arm_plugin_sim/build
rm -rf $TACO_EXAMPLES_PATH/_06_arm_op_sim/build
rm -rf $TACO_EXAMPLES_PATH/_07_arm_qp_sim/build
rm -rf $TACO_EXAMPLES_PATH/_08_rcm_sim/build
rm -rf $TACO_EXAMPLES_PATH/_09_quadruped_sim/build
rm -rf $TACO_EXAMPLES_PATH/_10_keyboard_teleoperation/build
rm -rf $TACO_EXAMPLES_PATH/_11_haptic_teleoperation/build
rm -rf $TACO_EXAMPLES_PATH/_12_shm_ros_publisher/build
rm -rf $TACO_EXAMPLES_PATH/_13_contact_stability/build
rm -rf $TACO_EXAMPLES_PATH/_14_robot_driver/build





