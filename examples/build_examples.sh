#!/bin/bash

# USER PARAMETERS
export TACO_USE_ROS=true

# ===============================================
TACO_EXAMPLES_PATH=$(dirname $(readlink -f $0))

# ===============================================
# TACO STANDALONE EXAMPLES

cd $TACO_EXAMPLES_PATH/_01_operational_space
mkdir -p build
cd build
cmake ..
make -j3

cd $TACO_EXAMPLES_PATH/_02_fixed_controller
mkdir -p build
cd build
cmake ..
make -j3

cd $TACO_EXAMPLES_PATH/_03_gazebo_cpp_sim
mkdir -p build
cd build
cmake ..
make -j3

cd $TACO_EXAMPLES_PATH/_04_op_vs_qp_controller
mkdir -p build
cd build
cmake ..
make -j3

# ===============================================
# ROS DEPENDENT EXAMPLES 

if [ "$TACO_USE_ROS" = true ] ; then

cd $TACO_EXAMPLES_PATH/_05_arm_plugin_sim
mkdir -p build
cd build
cmake ..
make -j3

cd $TACO_EXAMPLES_PATH/_06_arm_opspace_sim
mkdir -p build
cd build
cmake ..
make -j3

cd $TACO_EXAMPLES_PATH/_07_arm_qp_sim
mkdir -p build
cd build
cmake ..
make -j3

cd $TACO_EXAMPLES_PATH/_08_rcm_sim
mkdir -p build
cd build
cmake ..
make -j3

cd $TACO_EXAMPLES_PATH/_09_quadruped_sim
mkdir -p build
cd build
cmake ..
make -j3

cd $TACO_EXAMPLES_PATH/_10_keyboard_teleoperation
mkdir -p build
cd build
cmake ..
make -j3

if [ -d $TACO_EXAMPLES_PATH/_11_haptic_teleoperation/chai3d ]; then
    cd $TACO_EXAMPLES_PATH/_11_haptic_teleoperation
    mkdir -p build
    cd build
    cmake ..
    make -j3
else
    echo "WARNING. Skipping _11_haptic_teleoperation example. Chai3d not found."
fi

cd $TACO_EXAMPLES_PATH/_12_shm_ros_publisher
mkdir -p build
cd build
cmake ..
make -j3

fi # TACO_USE_ROS

cd $TACO_EXAMPLES_PATH/_13_contact_stability
mkdir -p build
cd build
cmake ..
make -j3

if [ -d $TACO_EXAMPLES_PATH/_14_robot_driver/KukaLBRDynamics ]; then
    cd $TACO_EXAMPLES_PATH/_14_robot_driver
    mkdir -p build
    cd build
    cmake ..
    make -j3
else
    echo "WARNING. Skipping _14_robot_driver example. KukaLBRDynamics not found"
fi













