Robot Drivers
=============

Here you can find a list of all robot drivers for the Task-space Control Library. These use the @ref IPC "Inter-process Communication". Drivers should use "ShmDriver.h", which creates the shared memory interface and checks for command timeout. Additional safety checks and all error handling is driver specific. Note: [robot models] are submoduled by the task-space control library, and can be found in the directory taco/models. 

| Driver                                         | Dependency   | Description                                 |
| ---------------------------------------------- | ------------ | ------------------------------------------- |
| @ref md_drivers_kuka_iiwa_KUKA_IIWA_README     | KUKA FRI SDK | KUKA LBR iiwa position and torque interface |
| @ref md_drivers_kinova_jaco_KINOVA_JACO_README | jaco driver  | Kinova position and torque interface        |

[robot models]:https://bitbucket.org/libtaco/models
