/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#include "LBRShmClient.h"
#include <taco/comm/copy.h>
#include <taco/comm/topics.h>
#include <sstream>
#include <tinyxml.h>

namespace KUKA {
namespace FRI {

LBRShmClient::LBRShmClient()
#ifdef USE_KUKA_LBR_DYNAMICS
    : dynamics_(kuka::Robot::LBRiiwa)
#endif
{
    filter.setDimension(LBRState::NUMBER_OF_JOINTS);
    filter.setCutoffFrequency(cutoff_freq_);

    vel_raw_.setZero(KUKA::FRI::LBRState::NUMBER_OF_JOINTS);
    vel_filtered_.setZero(KUKA::FRI::LBRState::NUMBER_OF_JOINTS);

    shm_driver_.initializeSharedMemory(models_name_, joint_names_);

    /*
    shm_driver_.timeout_Kp_[0] = 50;
    shm_driver_.timeout_Kp_[1] = 150;
    shm_driver_.timeout_Kp_[2] = 50;
    shm_driver_.timeout_Kp_[3] = 50;
    shm_driver_.timeout_Kp_[4] = 10;
    shm_driver_.timeout_Kp_[5] = 10;
    shm_driver_.timeout_Kp_[6] = 5;

    for (unsigned int i=0; i<LBRState::NUMBER_OF_JOINTS; ++i){
        shm_driver_.timeout_Kd_[i] = 2*sqrt(shm_driver_.timeout_Kp_[i]);
    }
    */

    shm_driver_.timeout_Kp_[0] = 0;
    shm_driver_.timeout_Kp_[1] = 0;
    shm_driver_.timeout_Kp_[2] = 0;
    shm_driver_.timeout_Kp_[3] = 0;
    shm_driver_.timeout_Kp_[4] = 0;
    shm_driver_.timeout_Kp_[5] = 0;
    shm_driver_.timeout_Kp_[6] = 0;

    shm_driver_.timeout_Kd_[0] = 10;
    shm_driver_.timeout_Kd_[1] = 10;
    shm_driver_.timeout_Kd_[2] = 5;
    shm_driver_.timeout_Kd_[3] = 5;
    shm_driver_.timeout_Kd_[4] = 3;
    shm_driver_.timeout_Kd_[5] = 3;
    shm_driver_.timeout_Kd_[6] = 1;


#ifdef USE_KUKA_LBR_DYNAMICS
    parseTool();
#endif
}


LBRShmClient::~LBRShmClient()
{   
}
      

void LBRShmClient::onStateChange(KUKA::FRI::ESessionState oldState,
                                 KUKA::FRI::ESessionState newState)
{
   LBRClient::onStateChange(oldState, newState);
   // react on state change events
   switch (newState)
   {
      case KUKA::FRI::MONITORING_WAIT:
      {
         shm_driver_.printMessage("FRI switched to state [MONITORING_WAIT]\n");
         break;
      }       
      case KUKA::FRI::MONITORING_READY:
      {
         sampleTime_ = robotState().getSampleTime();
         shm_driver_.printMessage("FRI switched to state [MONITORING_READY]\n");
         break;
      }
      case KUKA::FRI::COMMANDING_WAIT:
      {
         shm_driver_.printMessage("FRI switched to state [COMMANDING_WAIT]\n");
         break;
      }   
      case KUKA::FRI::COMMANDING_ACTIVE:
      {
         shm_driver_.printMessage("FRI switched to state [COMMANDING_ACTIVE]\n");
         break;
      }   
      default:
      {
         shm_driver_.printWarning("FRI switched to state [UNKNOWN]\n");
         break;
      }
   }
}


void LBRShmClient::waitForCommand()
{
   // In waitForCommand(), the joint values have to be mirrored. Which is done, 
   // by calling the base method.
   LBRClient::waitForCommand();

   // If we want to command torques, we have to command them all the time; even in
   // waitForCommand(). This has to be done due to consistency checks. In this state it is
   // only necessary, that some torque values are sent. The LBR does not take the
   // specific value into account.
   if (robotState().getClientCommandMode() == KUKA::FRI::TORQUE) {
      robotCommand().setJointPosition(robotState().getIpoJointPosition());
      robotCommand().setTorque(desired_torque_);
   }
}


void LBRShmClient::command()
{
    bool command_mode_changed = false;

    // In command(), the joint values have to be sent. Which is done by calling
    // the base method.
    LBRClient::command();

   // check for control mode change
   KUKA::FRI::EClientCommandMode fri_command_mode = robotState().getClientCommandMode();
   if (fri_command_mode != prev_fri_command_mode_){
       printCommandMode(fri_command_mode);
       command_mode_changed = true;
       prev_fri_command_mode_ = fri_command_mode;
   }

   // get the position and measure torque
   memcpy(position_, robotState().getMeasuredJointPosition(), LBRState::NUMBER_OF_JOINTS * sizeof(double));

   // get the time
   timespec time;
   time.tv_sec = robotState().getTimestampSec();
   time.tv_nsec = robotState().getTimestampNanoSec();

   // get the velocity
   if (prev_time_.tv_sec==0 && prev_time_.tv_nsec==0){
       // if not initialized, set to zero
       vel_raw_.setZero();
       vel_filtered_.setZero();
   } else {
       // velocity = (position - last position)/(time - last time)
       double dt = shm_driver_.elapsedTime(prev_time_, time);
       for (int i=0; i<LBRState::NUMBER_OF_JOINTS; ++i){
           vel_raw_(i) = (position_[i] - prev_position_[i])/dt;
       }
       vel_filtered_ = filter.update(vel_raw_);
   }

   // copy joint state from FRI client to shared memory
   {
       // update header info
       TACO_SHM_SCOPED_LOCK(shm_driver_.joint_state_->sync.mutex);
       shm_driver_.joint_state_->header.stamp.sec = time.tv_sec;
       shm_driver_.joint_state_->header.stamp.nsec = time.tv_nsec;
       shm_driver_.joint_state_->header.seq++;

       // copy joint position etc
       memcpy(shm_driver_.joint_state_->position, robotState().getMeasuredJointPosition(), KUKA::FRI::LBRState::NUMBER_OF_JOINTS * sizeof(double));
       for (int i=0; i<KUKA::FRI::LBRState::NUMBER_OF_JOINTS; ++i){
            shm_driver_.joint_state_->velocity[i] = vel_filtered_(i);
       } 
       memcpy(shm_driver_.joint_state_->force_commanded, robotState().getCommandedTorque(), KUKA::FRI::LBRState::NUMBER_OF_JOINTS * sizeof(double));
       memcpy(shm_driver_.joint_state_->force_measured, robotState().getMeasuredTorque(), KUKA::FRI::LBRState::NUMBER_OF_JOINTS * sizeof(double));

       // set as as initialized
       shm_driver_.joint_state_->sync.initialized = true;
   }

   // GET COMMAND
   {
       TACO_SHM_SCOPED_LOCK(shm_driver_.joint_command_->sync.mutex);

       // print out if command mode has changed
       taco::shm::JointCommand<LBRState::NUMBER_OF_JOINTS>::ControlType shm_control_type = shm_driver_.joint_command_->control_type;
       if (shm_control_type != prev_shm_control_type_){
           printCommandMode(shm_control_type);
           command_mode_changed = true;
           prev_shm_control_type_ = shm_control_type;
       }

       //check if state is mismatched
       bool command_mode_ok = checkCommandMode(fri_command_mode, shm_control_type);
       if (command_mode_changed && !command_mode_ok){
           shm_driver_.setTimeoutPosition();
           shm_driver_.printWarning("FRI and shared memory control modes do not match. Ignoring command.");
       }

       if (shm_driver_.commandTimeout())
       {
           // command timeout, hold timeout position
            switch (fri_command_mode){
                case KUKA::FRI::NO_COMMAND_MODE:
                    break;
                case KUKA::FRI::POSITION:
                    for (int i=0; i<KUKA::FRI::LBRState::NUMBER_OF_JOINTS; ++i){
                        desired_position_[i] = shm_driver_.timeoutPosition(i);
                    }
                    break;
                case KUKA::FRI::WRENCH:
                    break;
                case KUKA::FRI::TORQUE:
                    shm_driver_.updateTimeoutTorque();
                    for (int i=0; i<KUKA::FRI::LBRState::NUMBER_OF_JOINTS; ++i){
                       desired_torque_[i] = shm_driver_.timeoutTorque(i);
                    }
                    break;
                default:
                    break;
            }
       } else {
           // command ok, copy joint command from shared memory to FRI client
           switch (fri_command_mode){
               case KUKA::FRI::NO_COMMAND_MODE:
                   break;
               case KUKA::FRI::POSITION:
                   for (int i=0; i<KUKA::FRI::LBRState::NUMBER_OF_JOINTS; ++i){
                       desired_position_[i] = shm_driver_.joint_command_->desired_pos[i];
                   }
                   break;
               case KUKA::FRI::WRENCH:
                   break;
               case KUKA::FRI::TORQUE:
                   shm_driver_.updateTimeoutTorque();
                   for (int i=0; i<KUKA::FRI::LBRState::NUMBER_OF_JOINTS; ++i){
                      desired_torque_[i] = shm_driver_.joint_command_->desired_force[i];
                   }
                   break;
               default:
                   break;
           }
       }
   }

   // ADD GRAVITY COMPENSATION FOR TOOL
#ifdef USE_KUKA_LBR_DYNAMICS
    if (fri_command_mode==KUKA::FRI::TORQUE){
        Eigen::VectorXd q(LBRState::NUMBER_OF_JOINTS);
        for (int i=0; i<LBRState::NUMBER_OF_JOINTS; ++i){
            q(i) = position_[i];
        }
        Eigen::MatrixXd J(6, LBRState::NUMBER_OF_JOINTS); J.setZero();
        dynamics_.getJacobian(J, q, tool_com_, 6);
        Eigen::VectorXd tool_g_force(6); tool_g_force << 0,0,-9.81*tool_mass_,0,0,0;
        Eigen::VectorXd tool_g_torque = J.transpose()*tool_g_force;
        for (int i=0; i<LBRState::NUMBER_OF_JOINTS; ++i){
            desired_torque_[i] -= tool_g_torque(i);
        }
    }
#endif

   // CHECK COMMAND
   switch(fri_command_mode){
       case KUKA::FRI::NO_COMMAND_MODE:
           break;
       case KUKA::FRI::POSITION:
            for (int i=0; i<LBRState::NUMBER_OF_JOINTS; ++i){
                if (desired_position_[i] < -joint_limit_[i]){
                    desired_position_[i] = -joint_limit_[i];
                    shm_driver_.printWarning("desired position of joint ["+std::to_string(i)+"] exceeds limit");
                }
                else if (desired_position_[i] > joint_limit_[i]){
                         desired_position_[i] = joint_limit_[i];
                    shm_driver_.printWarning("desired position of joint ["+std::to_string(i)+"] exceeds limit");
                }
            }
            break;
       case KUKA::FRI::WRENCH:
            break;
       case KUKA::FRI::TORQUE:
           for (int i=0; i<LBRState::NUMBER_OF_JOINTS; ++i){
               if (desired_torque_[i] < -torque_limit_[i]){
                   desired_torque_[i] = -torque_limit_[i];
                   shm_driver_.printWarning("desired torque of joint ["+std::to_string(i)+"] exceeds limit");
               }
               else if (desired_torque_[i] > torque_limit_[i]){
                        desired_torque_[i] = torque_limit_[i];
                   shm_driver_.printWarning("desired torque of joint ["+std::to_string(i)+"] exceeds limit");
               }
           }
           break;
   }

   // SEND COMMAND
   switch (fri_command_mode){
       case KUKA::FRI::NO_COMMAND_MODE:
           break;
       case KUKA::FRI::POSITION:
            robotCommand().setJointPosition(desired_position_);
            break;
       case KUKA::FRI::WRENCH:
            shm_driver_.printWarning("Kuka FRI Wrench control mode has not been implemented in driver");
            break;
       case KUKA::FRI::TORQUE:

            memcpy(dither_, robotState().getMeasuredJointPosition(), LBRState::NUMBER_OF_JOINTS*sizeof(double)); 

            for (int i=0; i<7; i++){
              dither_[i] += 0.5/180.0* M_PI*sin(2*M_PI*20*currentTime_);
            }

            // robotCommand().setJointPosition(robotState().getMeasuredJointPosition());
            robotCommand().setJointPosition(  dither_ );
            robotCommand().setTorque(desired_torque_);
           break;
       default:
           shm_driver_.printWarning("Kuka FRI control mode unrecognized");
           break;
   }

   // update the previous values
   currentTime_ += sampleTime_;

   prev_time_ = time;
   memcpy(prev_position_, position_, LBRState::NUMBER_OF_JOINTS * sizeof(double));

   /*
   if (shm_driver_.joint_state_->header.seq%1000==0){
       shm_driver_.joint_state_->print();
   }
   */
}


bool LBRShmClient::checkCommandMode(KUKA::FRI::EClientCommandMode fri_command_mode,
                      taco::shm::JointCommand<LBRState::NUMBER_OF_JOINTS>::ControlType shm_command_mode)
{
    bool match = false;
    switch (fri_command_mode)
    {
        case KUKA::FRI::NO_COMMAND_MODE:
            if (shm_command_mode==shm_driver_.joint_command_->CONTROL_NONE){ match = true; }
            break;
        case KUKA::FRI::POSITION:
            if (shm_command_mode==shm_driver_.joint_command_->CONTROL_POSITION){ match = true; }
            break;
        case KUKA::FRI::WRENCH:
            break;
        case KUKA::FRI::TORQUE:
            if (shm_command_mode==shm_driver_.joint_command_->CONTROL_FORCE){ match = true; }
            break;
    }
    return match;
}


void LBRShmClient::printCommandMode(KUKA::FRI::EClientCommandMode command_mode)
{
    switch (command_mode)
    {
        case KUKA::FRI::NO_COMMAND_MODE:
            shm_driver_.printMessage("FRI command mode is [NO_COMMAND_MODE]\n");
            break;
        case KUKA::FRI::POSITION:
            shm_driver_.printMessage("FRI command mode is [POSITION]\n");
            break;
        case KUKA::FRI::WRENCH:
            shm_driver_.printMessage("FRI command mode is [WRENCH]\n");
            break;
        case KUKA::FRI::TORQUE:
            shm_driver_.printMessage("FRI command mode is [TORQUE]\n");
            break;
    }
}

void LBRShmClient::printCommandMode(taco::shm::JointCommand<LBRState::NUMBER_OF_JOINTS>::ControlType command_mode)
{
    switch (command_mode)
    {
        case shm_driver_.joint_command_->CONTROL_NONE:
            shm_driver_.printMessage("Shm command mode is [NONE]\n");
            break;
        case shm_driver_.joint_command_->CONTROL_POSITION:
            shm_driver_.printMessage("Shm command mode is [POSITION]\n");
            break;
        case shm_driver_.joint_command_->CONTROL_VELOCITY:
            shm_driver_.printMessage("Shm command mode is [VELOCITY]\n");
            break;
        case shm_driver_.joint_command_->CONTROL_ACCELERATION:
            shm_driver_.printMessage("Shm command mode is [ACCELERATION]\n");
            break;
        case shm_driver_.joint_command_->CONTROL_FORCE:
            shm_driver_.printMessage("Shm command mode is [FORCE]\n");
            break;
    }
}


#ifdef USE_KUKA_LBR_DYNAMICS

void LBRShmClient::parseTool()
{
    TiXmlDocument doc( tool_file_name_ );
    if (doc.LoadFile()){
        shm_driver_.printMessage("Loading tool file ["+tool_file_name_+"].");
        try {
            std::string mass = doc.FirstChildElement("tool")->
                                   FirstChildElement("inertial")->
                                   FirstChildElement("mass")->
                                   Attribute("value");
            tool_mass_ = std::stod(mass);
            shm_driver_.printMessage("Tool mass: "+mass);

            std::stringstream com( doc.FirstChildElement("tool")->
                                   FirstChildElement("inertial")->
                                   FirstChildElement("origin")->
                                   Attribute("xyz"));
            com >> tool_com_(0);
            com >> tool_com_(1);
            com >> tool_com_(2);
            std::stringstream ss; ss << tool_com_.transpose();
            shm_driver_.printMessage("Tool CoM : "+ss.str());
        }
        catch( const std::exception& e ) { // reference to the base of a polymorphic object
            std::cout << e.what(); // information from length_error printed
            shm_driver_.printWarning("Failed to parse tool file.");
        }
    } else {
        shm_driver_.printWarning("Could no load tool file ["+tool_file_name_);
    }
}

#endif


} //namespace FRI
} //namespace KUKA
