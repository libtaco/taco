&nbsp;- KUKA LBR iiwa Driver
=========================

This driver creates a shared memory interface between the [KUKA] LBR iiwa and [Task-space Control Library], for torque control or position control. It will estimate joint velocities, using differentation and a 2nd order Butterworth filter (default cutoff frequency is 100 Hz). It uses the KUKA Fast Robot Interface C++ SDK (version 1.7). This a proprietary SDK that should be obtained directly from KUKA. It also uses the KukaLBRDynamics library, to compensate for tool weight. This is proprietary and optional library.

General driver behavior is described in @ref md_drivers_ROBOT_DRIVERS

Install
-------

1. Obtain the FRI-Client-SDK from KUKA Roboter GmbH, and download to "taco/drivers/kuka_iiwa/FRI-Client-SDK". Follow the build instructions. Graduate students in the Stanford Robotics Lab can clone from the private repository.

        cd taco/drivers/kuka_iiwa
        git clone https://bitbucket.org/libtaco/fri-client-sdk.git FRI-Client-SDK
        cd FRI-Client-SDK/build/GNUMake/
        make

2. Obtain KukaLBRDynamics from KUKA Roboter GmbH, and download to "taco/driver/kuka_iiwa/KukaLBRDynamics". This is optional, and used to offset tool weight. Follow the build instructions. Graduate students in the Stanford Robotics Lab can clone from the private repository.

        cd taco/drivers/kuka_iiwa
        git clone https://bitbucket.org/libtaco/kukalbrdynamics.git KukaLBRDynamics

   If you do not want to use this add-on, then change the following the option in the CMakeLists.txt

        option(USE_KUKA_LBR_DYNAMICS "USE_KUKA_LBR_DYNAMICS" OFF)

3. Build the driver.

        cd taco/drivers/kuka_iiwa
        mkdir build && cd build
        cmake .. && make

4. Change the IP Address. The easiest method is adding a new connection in "Network Connections".

        Address: 192.170.10.1
        Netmask: 255.255.255.0

5. Turn on the robot, and select the TorqueOverlay application. Hit play.

6. Open a new terminal and run the kuka_iiwa_driver. The robot should now be floating.

        cd build
        ./kuka_iiwa_driver


[KUKA]:https://www.kuka.com/
[Task-space Control Library]:http://libtaco.bitbucket.org/
[robot model]:https://bitbucket.org/libtaco/models

