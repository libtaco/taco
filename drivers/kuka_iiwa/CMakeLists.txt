cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(kuka_iiwa_driver)


option(USE_KUKA_LBR_DYNAMICS    "USE_KUKA_LBR_DYNAMICS"     ON)


# CMAKE OPTIONS
set(CMAKE_BUILD_TYPE Debug)
SET(CMAKE_VERBOSE_MAKEFILE ON)

# BUILD SOURCES AND LIBRARY
set(SOURCES LBRShmDriver.cpp
            LBRShmClient.cpp
)

# Create a library
add_executable(${PROJECT_NAME} ${SOURCES})

# KUKA IIWA FRI SDK
set(FRI_BASE_DIR ${CMAKE_CURRENT_LIST_DIR}/FRI-Client-SDK)
set(FRI_INCLUDE_DIRS ${FRI_BASE_DIR}/include)
find_library(FRI_LIBRARIES FRIClient PATHS ${FRI_BASE_DIR}/lib)
set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} "-O2 -Wall -Wextra -pedantic -std=c++11 -fPIC -fopenmp")
add_definitions(-DHAVE_SOCKLEN_T -DPB_SYSTEM_HEADER=\"pb_syshdr.h\" -DHAVE_STDINT_H -DHAVE_STDDEF_H -DHAVE_STDBOOL_H -DHAVE_STDLIB_H -DHAVE_STRING_H)

# KUKA LBR Dynamics
if (USE_KUKA_LBR_DYNAMICS)
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/KukaLBRDynamics)
    find_package (KukaLBRDynamics REQUIRED)
    include_directories(${KukaLBRDynamics_INCLUDE_DIRS})
    target_link_libraries(${PROJECT_NAME} ${KukaLBRDynamics_LIBRARIES})
    add_definitions(-DUSE_KUKA_LBR_DYNAMICS)

    find_path( TINYXML_INCLUDE_DIRS "tinyxml.h" PATH_SUFFIXES "tinyxml" )
    find_library( TINYXML_LIBRARY_DIRS NAMES "tinyxml" PATH_SUFFIXES "tinyxml" )
    include_directories(${TINYXML_INCLUDE_DIRS})
    target_link_libraries(${PROJECT_NAME} ${TINYXML_LIBRARY_DIRS})
endif(USE_KUKA_LBR_DYNAMICS)

# TACO
set(TACO_DIR ${CMAKE_CURRENT_LIST_DIR}/../..)

# BOOST
FIND_PACKAGE( Boost 1.40 REQUIRED )

# create headers list
set(kuka_iiwa_driver_INCLUDE_DIRS   ${TACO_DIR}/include
                                    ${Boost_INCLUDE_DIR}
                                    ${FRI_INCLUDE_DIRS}
                                    ${KukaLBRDynamics_INCLUDE_DIRS}
)

# create libraries list
set(kuka_iiwa_driver_LIBRARIES      ${Boost_LIBRARIES}
                                    ${FRI_LIBRARIES}
                                    ${KukaLBRDynamics_LIBRARIES}
                                    -lrt
)

include_directories( ${kuka_iiwa_driver_INCLUDE_DIRS} )
target_link_libraries(${PROJECT_NAME} ${kuka_iiwa_driver_LIBRARIES})



