/**
* \copyright
* TaCo - Task-Space Control Library.<br>
* Copyright (c) 2015-2016
*<br>
* TaCo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*<br>
* TaCo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*<br>
* You should have received a copy of the Lesser GNU Lesser General Public License
* along with TaCo.  If not, see <http://www.gnu.org/licenses/>.
<br>
Author: Brian Soe <bsoe@stanford.edu>
*/


#ifndef TACO_DRIVERS_KUKA_IIWA_LBRSHMCLIENT_H_
#define TACO_DRIVERS_KUKA_IIWA_LBRSHMCLIENT_H_

#include "friLBRClient.h"
#include <taco/driver/ShmDriver.h>
#include <taco/model/models.h>
#include <taco/filter/ButterworthFilter.h>
#include <fstream>
#include <string>

#ifdef USE_KUKA_LBR_DYNAMICS
    #include <KukaLBRDynamics/Robot.h>
    #include <eigen3/Eigen/Dense>
#endif

namespace KUKA {
namespace FRI {


/** \brief Client for Kuka LBR IIWA that reads and writes to shared memory. */
class LBRShmClient : public KUKA::FRI::LBRClient
{
   
public:
      
   LBRShmClient();
   
   ~LBRShmClient();
   
   /** \brief Callback for FRI state changes.
    * @param oldState
    * @param newState
    */
   virtual void onStateChange(KUKA::FRI::ESessionState oldState, KUKA::FRI::ESessionState newState);
    
   /** \brief Callback for the FRI session state 'Commanding Wait'. */
   virtual void waitForCommand();
   
   /** \brief Callback for the FRI state 'Commanding Active'. */
   virtual void command();

   /** \brief robot name used for the shared memory segment name. If there is more than one kuka iiwa, this will cause problems. */
   const std::string models_name_ = taco::model::KUKA_IIWA;

   /** \brief the joint names, this should match the robot model urdf and sdf */
   const std::vector<std::string> joint_names_ = {"j0","j1","j2","j3","j4","j5","j6"};

   /** \brief the joint maximum torques */
   const double torque_limit_[7] = {176, 176, 110, 110, 110, 40, 40}; // LBR iiwa 7 R800
   //const double max_torque_[7] = {320, 320, 176,176, 110, 40, 40}; // LBR iiwa 14 R820

   /** \brief the joint limits. LBR iiwa 7 R800 or LBR iiwa 1r R820 */
   const double joint_limit_[7] = {2.9670, 2.0944, 2.9670, 2.0944, 2.9670, 2.0944, 3.0543};

   /** \brief the cutoff frequency of the filter, in the range of (0 0.5) of sampling freq */
   const double cutoff_freq_ = 0.1;

protected:

   /** \brief Checks if FRI and shared memory command modes match.
    * \return True if command modes match. */
   bool checkCommandMode(KUKA::FRI::EClientCommandMode fri_command_mode,
                         taco::shm::JointCommand<LBRState::NUMBER_OF_JOINTS>::ControlType shm_command_mode);

   /** \brief print out command mode */
   void printCommandMode(KUKA::FRI::EClientCommandMode command_mode);

   /** \brief print out command mode */
   void printCommandMode(taco::shm::JointCommand<LBRState::NUMBER_OF_JOINTS>::ControlType command_mode);

   /** \brief print robot state */
   void printRobotState();

   /** \brief shared memory driver */
   taco::ShmDriver<LBRState::NUMBER_OF_JOINTS> shm_driver_;

   /** \brief time of previous measurement, for calculating dt and velocity */
   timespec prev_time_ = {0, 0};

   /** \brief last fri command mode, for checking for mode change */
   KUKA::FRI::EClientCommandMode prev_fri_command_mode_ = KUKA::FRI::NO_COMMAND_MODE;

   /** \brief last shm command mode, for checking for mode change */
   taco::shm::JointCommand<LBRState::NUMBER_OF_JOINTS>::ControlType prev_shm_control_type_ =
           taco::shm::JointCommand<LBRState::NUMBER_OF_JOINTS>::CONTROL_NONE;

   /** \brief velocity filter */
   taco::ButterworthFilter filter;

   /** \brief raw velocity */
   Eigen::VectorXd vel_raw_;

   /** \brief filtered velocity */
   Eigen::VectorXd vel_filtered_;

   /** \brief desired torque */
   double desired_torque_[LBRState::NUMBER_OF_JOINTS] = {0};

   /** \brief desired position */
   double desired_position_[LBRState::NUMBER_OF_JOINTS] = {0};

   /** \brief position */
   double position_[KUKA::FRI::LBRState::NUMBER_OF_JOINTS] = {0};

   /** \brief previous position */
   double prev_position_[KUKA::FRI::LBRState::NUMBER_OF_JOINTS] = {0};

   double sampleTime_ = 0.0;
   double currentTime_ = 0.0;

   double dither_[LBRState::NUMBER_OF_JOINTS] = {0};

#ifdef USE_KUKA_LBR_DYNAMICS

   void parseTool();

   /** \brief name of the tool */
   const std::string tool_file_name_ = "../tool.xml";

   /** \brief tool mass */
   double tool_mass_ = 0;

   /** \brief tool mass */
   Eigen::Vector3d tool_com_ = Eigen::Vector3d::Zero();

   /** \brief kuka dynamics */
   kuka::Robot dynamics_;

#endif

};

} //namespace FRI
} //namespace KUKA

#endif // _KUKA_FRI_MY_LBR_CLIENT_H
