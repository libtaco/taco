&nbsp;- Kinova Jaco Driver
==========================

This driver creates a shared memory interface between the [Kinova Jaco] and [Task-space Control Library], for torque control. It uses the [Kinova SDK].

STOP! This has not been tested yet.

General driver behavior is described in @ref md_drivers_ROBOT_DRIVERS

Install
-------

1. Download and installed the [Kinova SDK].
2. Create a symbolic link to the libraries so the linker can find them

        cd /usr/lib
        sudo ln -s Kinova.API.CommLayerUbuntu.so libKinova.API.CommLayerUbuntu.so
        sudo ln -s Kinova.API.USBCommandLayerUbuntu.so libKinova.API.USBCommandLayerUbuntu.so 


3. Add usb permission to your udev rules (permanent fix), then plug in the arm. It is also possible to set [temporary usb permissions], or run the program with sudo.

        sudo cp udev/99-jaco-arm.rules /etc/udev/rules.d/
        sudo udevadm control --reload-rules

4. Build the library

        cd taco/drivers/kinova_jaco
        mkdir build && cd build
        cmake .. && make

5. Run the library

        ./kinova_jaco_driver





[Kinova Jaco]:http://www.kinovarobotics.com/service-robotics/products/robot-arms/
[Kinova SDK]:http://www.kinovarobotics.com/service-robotics/products/software/
[Task-space Control Library]:http://libtaco.bitbucket.org/
[robot model]:https://bitbucket.org/libtaco/models
[temporary usb permissions]:https://github.com/radarsat1/dimple/blob/master/doc/novint_falcon_howto.md

