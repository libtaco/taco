#include <taco/driver/ShmDriver.h>
#include <taco/controller/ControlLoopTimer.h>

#include <iostream>
#include <string>
#include <vector>

#include <dlfcn.h> //Ubuntu
#include <KinovaTypes.h>
#include <Kinova.API.CommLayerUbuntu.h>
#include <Kinova.API.UsbCommandLayerUbuntu.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>


const unsigned int DOF = 6;

static volatile bool running = false;
void CtrlCHandler(int sig) {
    running = false;
}

void copy(const AngularPosition& in, double (&out)[DOF]){
   out[0] = in.Actuators.Actuator1;
   out[1] = in.Actuators.Actuator2;
   out[2] = in.Actuators.Actuator3;
   out[3] = in.Actuators.Actuator4;
   out[4] = in.Actuators.Actuator5;
   out[5] = in.Actuators.Actuator6;
}

//Note that under windows, you may/will have to perform other #include
int main()
{

    taco::ControlLoopTimer timer;
    timer.setCtrlCHandler(CtrlCHandler);
    timer.setLoopFrequency(500);
    timer.setThreadHighPriority();

    taco::ShmDriver<DOF> shm_driver;
    std::vector<std::string> joint_names = {"j0","j1","j2","j3","j4","j5"};
    shm_driver.initializeSharedMemory(taco::model::KINOVA_JACO, joint_names);

    // send 0 torque when no commands are received, since gravity compensation done internally
    // and joint damping from friction is already high
    for (int i=0; i<DOF; ++i){
        shm_driver.timeout_Kp_[0];
        shm_driver.timeout_Kd_[0];
    }

    int result;
    int programResult = 0;
    AngularPosition position;
    AngularPosition velocity;
    AngularPosition force_measured;
    AngularPosition force_commanded;

    //Verify that all functions has been loaded correctly
    if ((InitAPI == NULL) || (CloseAPI == NULL) || (GetAngularCommand == NULL) ||
            (SwitchTrajectoryTorque == NULL))
    {
        std::cout << "* * *  E R R O R   D U R I N G   I N I T I A L I Z A T I O N  * * *" << std::endl;
        programResult = 0;
    }
    else
    {
        std::cout << "I N I T I A L I Z A T I O N   C O M P L E T E D" << std::endl << std::endl;
        result = (*InitAPI)();
        int resultComm;
        AngularPosition DataCommand;
        // Get the angular command to test the communication with the robot
        resultComm = GetAngularCommand(DataCommand);
        std::cout << "Initialization's result :" << result << std::endl;
        std::cout << "Communication result :" << resultComm << std::endl;
        // If the API is initialized and the communication with the robot is working
        if (result == 1 && resultComm == 1)
        {
            std::cout << "API initialization worked" << std::flush << std::endl;
            // Set to position mode
            SwitchTrajectoryTorque(POSITION);
            std::cout << "Set to Position Mode" << std::flush << std::endl;

            // Move to home position
            MoveHome();
            std::cout << "Moved to Home Position" << std::flush << std::endl;

            // Set the Optimal parameters obtained from the identification sequence
            float OptimalParam[OPTIMAL_Z_PARAM_SIZE] = {1.1993, 0.0175869, -0.00114071, -1.14021, 0.00718503, 0.570303,
                    0.00267999, 0.174534, -0.00615744, -0.00341871,0.505526, 0.132563, 0.0872487, 0.116211, 1.30409, 0.763368};
            // Set the gravity mode to Manual input
            SetGravityOptimalZParam(OptimalParam);
            // Set gravity type to optimal
            SetGravityType(OPTIMAL);

            std::cout << "Gravity Optimal Parameters Set" << std::flush << std::endl;

            // Set the torque control type to Direct Torque Control
            SwitchTrajectoryTorque(TORQUE);
            SetTorqueControlType(DIRECTTORQUE);

            std::cout << "Switch to Direct Torque Control" << std::flush << std::endl;

            // Set the safety factor off
            SetTorqueSafetyFactor(1);
            // Set the vibration controller off
            SetTorqueVibrationController(0);

            // Switch to torque control
            // Initialize the torque commands
            float TorqueCommand[COMMAND_SIZE] = {0};
            float Result[COMMAND_SIZE];

            for (int i = 0; i < COMMAND_SIZE; i++)
            {
                TorqueCommand[i] = 0;
            }
            TorqueCommand[1] = 1;

            running = true;
            timer.initializeTimer(1000000);
            while(running)
            {
                timer.waitForNextLoop();

                GetAngularPosition(position);
                GetAngularVelocity(velocity);
                //GetActuatorAcceleration(acceleration);
                GetAngularForce(force_measured);
                GetAngularCommand(force_commanded);

                // copy joint state from FRI client to shared memory
                {
                    // update header info
                    TACO_SHM_SCOPED_LOCK(shm_driver.joint_state_->sync.mutex);
                    struct timespec t;
                    timer.loopTime(t);
                    shm_driver.joint_state_->header.stamp.sec = t.tv_sec;
                    shm_driver.joint_state_->header.stamp.nsec = t.tv_nsec;
                    shm_driver.joint_state_->header.seq++;

                    // copy joint position etc
                    copy(position, shm_driver.joint_state_->position);
                    copy(velocity, shm_driver.joint_state_->velocity);
                    //copy(acceleration, shm_driver.joint_state_->acceleration);
                    copy(force_commanded, shm_driver.joint_state_->force_commanded);
                    copy(force_measured, shm_driver.joint_state_->force_measured);

                    // set as as initialized
                    shm_driver.joint_state_->sync.initialized = true;
                }

                {
                    TACO_SHM_SCOPED_LOCK(shm_driver.joint_command_->sync.mutex);
                    if (shm_driver.commandTimeout()){
                        shm_driver.updateTimeoutTorque();
                        for (unsigned int i=0; i<DOF; ++i){
                            TorqueCommand[i] = shm_driver.timeoutTorque(i);
                        }
                    } else {
                        switch (shm_driver.joint_command_->control_type){
                            case shm_driver.joint_command_->CONTROL_NONE:
                                shm_driver.setTimeoutPosition();
                                shm_driver.updateTimeoutTorque();
                                for (unsigned int i=0; i<DOF; ++i){
                                    TorqueCommand[i] = shm_driver.timeoutTorque(i);
                                }
                                break;
                            case shm_driver.joint_command_->CONTROL_FORCE:
                                for (unsigned int i=0; i<DOF; ++i){
                                    TorqueCommand[i] = shm_driver.joint_command_->desired_force[i];
                                }
                                break;
                            default:
                                shm_driver.printWarning("shared memory command mode is not torque");
                        }
                    }
                }
                SendAngularTorqueCommand(TorqueCommand);

                if(timer.elapsedCycles() % 1000 == 0){
                    GetAngularTorqueCommand(Result);
                    std::cout << "Torques: (" << Result[0] << ", " << Result[1] << ", " << Result[2] << ", ";
                    std::cout << Result[3] << ", " << Result[4] << ", " << Result[5] << "]" << std::endl;
                    shm_driver.joint_state_->print();
                }
            }

            // Set to position mode
            SwitchTrajectoryTorque(POSITION);
            // Move to home position
            MoveHome();
        }
        else
        {
            std::cout << "API initialization failed" << std::endl;
        }
        std::cout << std::endl << "C L O S I N G   A P I" << std::endl;
        result = (*CloseAPI)();
        programResult = 1;
    }
    return programResult;
}
