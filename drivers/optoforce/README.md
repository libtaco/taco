Optoforce driver
===============

This driver allows you to read the data from the optoforce force sensor and publish it to shared memory.
it has been tested on the 6DOF force sensor only.
The data is processed to send to the shared memory data in a direct right hand basis and with units (N for the forces, Nm for the moments)

Installation
------------
First, download the C++ API for linux in optoforce website
http://optoforce.com/support/

put it in the drivers folder and unzip it here.
Then, you can create a build directory and compile using cmake

```sh
mkdir build
cd build
cmake ..
make
```

Plug the force sensor to a usb port in your computer and run the program
```sh
./optoforce_driver
```
