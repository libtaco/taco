/*
 * Driver to use the force sensor optoforce (tested on 6D force sensor only for now)
 * it will create a connection, read the data, transfer the data from count value to real units
 * and publish it in a force sensor shared memory named "optoforce:force_sensor:6Dsensor"
 */


#include <iostream>
#include <string>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include "omd/opto.h"
#include "omd/sensorconfig.h"
#include "omd/optopackage.h"

// For shared memory publication
#include <taco/comm/shm/SharedMemory.h>
#include <taco/comm/shm/ForceSensor.h>

typedef unsigned long long mytime_t;

mytime_t Now()
{
    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    mytime_t millisecs = t.tv_sec * 1000;
    millisecs += t.tv_nsec / (1000 * 1000);
    return millisecs;
}


mytime_t NowMicro()
{
    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    mytime_t microsecs = t.tv_sec * 1000 * 1000;
    microsecs += t.tv_nsec / (1000);
    return microsecs;
}


mytime_t ElapsedTime(mytime_t p_Time)
{
	return Now() - p_Time;
}


mytime_t ElapsedTimeMicro(mytime_t p_Time)
{
	return NowMicro() - p_Time;
}


void MySleep(unsigned long p_uMillisecs)
{
	usleep(p_uMillisecs * 1000);
}

/*
 * Set the config to the DAQ
 * it is a blocking function; returns true, if the sending of
 * configuration is succeeded otherwise it returns false
 */
bool SetConfig(OptoDAQ & p_optoDAQ, int p_iSpeed, int p_iFilter)
{
	SensorConfig sensorConfig;
	sensorConfig.setSpeed(p_iSpeed);
	sensorConfig.setFilter(p_iFilter);
	mytime_t tNow = Now();

	bool bSuccess = false;
	do {
		bSuccess = p_optoDAQ.sendConfig(sensorConfig);
		if (bSuccess) {
			return true;
		}
		if (ElapsedTime(tNow) > 1000) {
			// 1 sec timeout
			return false;
		}
		MySleep(1);
	} while (bSuccess == false);
	return false;
}


void ShowInformation(OptoDAQ & p_optoDAQ, OPort & p_Port)
{
	std::string deviceName = std::string(p_Port.deviceName);
	std::string name = std::string(p_Port.name);
	std::string serialNumber = std::string (p_Port.serialNumber);
	int version = p_optoDAQ.getVersion();
	std::cout<<"Device Name: "<<deviceName<<std::endl;
	std::cout<<"Name: "<<name<<std::endl;
	std::cout<<"Serial Number: "<<serialNumber<<std::endl;
	std::cout<<"Version: "<<version<<std::endl;
}



/*
 * Opens the desired port
 * it returns true if port could be opened otherwise returns false
 */
bool OpenPort(OptoDAQ & p_optoDAQ, OptoPorts & p_Ports, int p_iIndex)
{
	MySleep(500); // We wait some ms to be sure about OptoPorts enumerated PortList
	OPort * portList = p_Ports.listPorts(true);
	int iLastSize = p_Ports.getLastSize();
	if (p_iIndex >= iLastSize) {
		// index overflow
		return false;
	}
	bool bSuccess = p_optoDAQ.open(portList[p_iIndex]);
	if (bSuccess) {
		ShowInformation(p_optoDAQ, portList[p_iIndex]);
	}
	return bSuccess;
}


/*
 * Blocking call to read one 3D package (with one second timeout)
 * it return a non-negative number if it succeeded (p_Package will be the read package)
 * otherwise returns -1
 */
int ReadPackage3D(OptoDAQ & p_optoDAQ, OptoPackage & p_Package)
{
	int iSize = -1;
	mytime_t tNow = Now();
	for (;;) {
		iSize = p_optoDAQ.read(p_Package, 0, false);
		if (iSize < 0 || iSize > 0) {
			break;
		}
		// No packages in the queue so we check the timeout
		if (ElapsedTime(tNow) >= 1000) {
			break;
		}
		usleep(100);
//		MySleep(1);
	}
	return iSize;
}


/*
 * Blocking call to read one 6D package (with one second timeout)
 * it return a non-negative number if it succeeded (p_Package will be the read package)
 * otherwise returns -1
 */
int ReadPackage6D(OptoDAQ & p_optoDAQ, OptoPackage6D & p_Package)
{
	int iSize = -1;
	mytime_t tNow = Now();
	for (;;) {
		iSize = p_optoDAQ.read6D(p_Package, false);
		if (iSize < 0 || iSize > 0) {
			break;
		}
		// No packages in the queue so we check the timeout
		if (ElapsedTime(tNow) >= 1000) {
			break;
		}
		usleep(100);
//		MySleep(1);
	}
	return iSize;
}


/*
 * The function determines if the sensor is a 3D sensor
 */
bool Is3DSensor(OptoDAQ & p_optoDAQ)
{
	opto_version optoVersion = p_optoDAQ.getVersion();
	if (optoVersion != _95 && optoVersion != _64) {
		return true;
	}
	return false;
}



void Run3DSensorExample(OptoDAQ & p_optoDAQ)
{
    // shared memory to share the force sensor data
    taco::shm::SharedMemory<taco::shm::ForceSensor > shm_force_sensor;
    taco::shm::ForceSensor* force_sensor = NULL;

    std::string shm_name = "optoforce:force_sensor:3Dsensor";
    std::cout << "\nOpening force sensor shared memory using name [" << shm_name << "]\n";

    while (force_sensor==NULL){
	force_sensor = shm_force_sensor.open(shm_name);
	if (force_sensor==NULL){ continue; }
	std::cout << "Opened force sensor shared memory\n";
    }

	mytime_t tNow = Now();
	unsigned int uTotalReadPackages = 0;
	while(true)
	{
		mytime_t tLoopNow = NowMicro();
		OptoPackage optoPackage;
		int iReadSize = ReadPackage3D(p_optoDAQ, optoPackage);
		if (iReadSize < 0) {
			std::cout<<"Something went wrong, DAQ closed!"<<std::endl;
			return;
		}
		uTotalReadPackages += (unsigned int)iReadSize;

		// Formatting output in C style
		double dLoopTime = ElapsedTimeMicro(tLoopNow) / 1000.0;
		mytime_t TotalElapsedTime = ElapsedTime(tNow);
		double dTotalTime = (double)TotalElapsedTime / 1000.0; // Elapsed time in sec
		double dFrequency = 0.0;
		if (dTotalTime > 0.0) {
			dFrequency = (uTotalReadPackages / dTotalTime);
		}
//		fprintf(stdout, "Elapsed: %.1f s Loop time: %.2f ms Samples: %u Sample rate: %.2f Hz\r\n", dTotalTime, dLoopTime, uTotalReadPackages, dFrequency);
//		fflush(stdout);

		// publish data to shared memory
		TACO_SHM_SCOPED_LOCK(force_sensor->sync.mutex);
		force_sensor->force[0] = optoPackage.x;
		force_sensor->force[1] = optoPackage.y;
		force_sensor->force[2] = optoPackage.z;

		force_sensor->moment[0] = 0;
		force_sensor->moment[1] = 0;
		force_sensor->moment[2] = 0;
	}

}

/*
 * Remaps the measurements to a right hand basis and uses the sensivity report (in docs)
 * to transform the measurements in N and Nm
 */
void processRaw6DSensorData(const OptoPackage6D& optoPackage, double (&data)[6])
{
    double Fzp_coeff = 1200./16078.;
    double Fzm_coeff = 150./1925.;
    double Fz_coeff = (Fzp_coeff + Fzm_coeff)/2;

    double Fx_coeff = 150./7098.;
    double Fy_coeff = 150./8006.;

    double Tx_coeff = 5./10284.;
    double Ty_coeff = 5./10134.;
    double Tz_coeff = 5./14977.;

    data[0] = optoPackage.Fx * Fx_coeff;
    data[1] = optoPackage.Fy * Fy_coeff;
    data[2] = -optoPackage.Fz * Fz_coeff;
    data[3] = -optoPackage.Tx * Tx_coeff;
    data[4] = optoPackage.Ty * Ty_coeff;
    data[5] = optoPackage.Tz * Tz_coeff;

}


void Run6DSensorExample(OptoDAQ & p_optoDAQ)
{
    // shared memory to share the force sensor data
    taco::shm::SharedMemory<taco::shm::ForceSensor > shm_force_sensor;
    taco::shm::ForceSensor* force_sensor = NULL;

    std::string shm_name = "optoforce:force_sensor:6Dsensor";
    std::cout << "\nOpening force sensor shared memory using name [" << shm_name << "]\n";

    while (force_sensor==NULL){
	force_sensor = shm_force_sensor.create(shm_name);
	if (force_sensor==NULL){continue;}
	std::cout << "Opened force sensor shared memory\n";
    }

    double measurements_with_units[6];

	mytime_t tNow = Now();
	unsigned int uTotalReadPackages = 0;
	int ctr = 0;
//	do {
	while(true)
	{
		mytime_t tLoopNow = NowMicro();
		OptoPackage6D optoPackage;
		int iReadSize = ReadPackage6D(p_optoDAQ, optoPackage);
		if (iReadSize < 0) {
			std::cout<<"Something went wrong, DAQ closed!"<<std::endl;
			return;
		}
		uTotalReadPackages += (unsigned int)iReadSize;

		// Formatting output in C style
		double dLoopTime = ElapsedTimeMicro(tLoopNow) / 1000.0;
		mytime_t TotalElapsedTime = ElapsedTime(tNow);
		double dTotalTime = (double)TotalElapsedTime / 1000.0; // Elapsed time in sec
		double dFrequency = 0.0;
		if (dTotalTime > 0.0) {
			dFrequency = (uTotalReadPackages / dTotalTime);
		}
		//fprintf(stdout, "Elapsed: %.1f s Loop time: %.2f ms Samples: %u Sample rate: %.2f Hz\r\n", dTotalTime, dLoopTime, uTotalReadPackages, dFrequency);
		//fflush(stdout);

		processRaw6DSensorData(optoPackage, measurements_with_units);

		// publish data to shared memory remaping it to a right hand base
		TACO_SHM_SCOPED_LOCK(force_sensor->sync.mutex);
		force_sensor->force[0] = measurements_with_units[0];
		force_sensor->force[1] = measurements_with_units[1];
		force_sensor->force[2] = measurements_with_units[2];
		force_sensor->moment[0] = measurements_with_units[3];
		force_sensor->moment[1] = measurements_with_units[4];
		force_sensor->moment[2] = measurements_with_units[5];

	}

}



int main()
{
	OptoDAQ optoDaq;
	OptoPorts optoPorts;
	// Changeable values, feel free to play with them
	int iPortIndex = 0;  // The index of the port which will be opened

	int iSpeed = 1000; // Speed in Hz (30, 100, 333 or 1000)
	int iFilter = 15;  // Filter cutoff frequency in Hz
	///////////////////
	if (OpenPort(optoDaq, optoPorts, iPortIndex) == false) {
		std::cout<<"Could not open port"<<std::endl;
		return 0;
	}
	bool bConfig = SetConfig(optoDaq, iSpeed, iFilter);
	if (bConfig == false) {
		std::cout<<"Could not set config"<<std::endl;
		optoDaq.close();
		return 0;
	}


	if (Is3DSensor(optoDaq)) {
		Run3DSensorExample(optoDaq);
	}
	else {
		Run6DSensorExample(optoDaq);
	}


	optoDaq.close();
	return 0;
}

