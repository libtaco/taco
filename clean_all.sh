#!/bin/bash

TACO_PATH=$(dirname $(readlink -f $0))

rm -rf $TACO_PATH/build
rm -rf $TACO_PATH/lib
rm -rf $TACO_PATH/include/taco/comm/ros
rm -rf $TACO_PATH/include/taco/model/models.h
sh $TACO_PATH/gazebo/clean_plugins.sh
sh $TACO_PATH/v-rep/clean_plugins.sh
sh $TACO_PATH/include/taco/primitive/clean_primitives.sh
sh $TACO_PATH/tests/clean_tests.sh
